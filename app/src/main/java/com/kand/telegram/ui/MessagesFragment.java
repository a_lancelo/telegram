package com.kand.telegram.ui;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.kand.telegram.logic.Util;
import com.kand.telegram.logic.Emoji;
import com.kand.telegram.logic.SidebarMenuFragment;
import com.kand.telegram.logic.TGService;
import com.kand.telegram.ui.adapters.MessagesAdapter;
import com.kand.telegram.R;
import com.kand.telegram.logic.TGFunc;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class MessagesFragment extends SidebarMenuFragment {

    private ArrayList<TdApi.Chat> chats = new ArrayList<>();
    private ListView listView;
    private Client client;
    private TdApi.User user;
    private boolean isDownloadedAllChats = false, isLoading = false, isOnline = true;
    private int userId;
    private Menu menu;

    AdapterView.OnItemClickListener menuListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            switch (position) {
                case 0:
                    hideSidebarMenu();
                    //Log.d("setUser0", user.firstName+" "+user.id+" "+user.lastName);
                    if (user != null && user.id != 0) {
                        //Log.d("setUser", user.toString());
                        SettingsFragment fragment = new SettingsFragment();
                        fragment.user = user;

                        if (Util.isTablet) {
                            TGFunc.showContainer(MessagesFragment.this);
                            getActivity().getSupportFragmentManager().beginTransaction()
                                    .add(R.id.container, fragment, "settings").addToBackStack(null).commit();
                        } else {
                            getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(0, 0, 0, R.anim.exit_to_right)
                                    .add(R.id.container, fragment, "settings").addToBackStack(null).commit();
                        }
                    }
                    break;
                case 1:
                    new AlertDialog.Builder(getActivity())
                            .setTitle(R.string.app_name)
                            .setPositiveButton(R.string.ok, okLogOutDialog)
                            .setNegativeButton(R.string.cancel, null)
                            .setMessage(R.string.log_out_text)
                            .show();
                    break;
            }
        }
    };

    BroadcastReceiver br = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, final Intent intent) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    if (getActivity() == null) return;
                    TGService service = ((MainActivity) getActivity()).getService();
                    if (service == null) return;
                    ChatFragment fragment = (ChatFragment) getFragmentManager().findFragmentByTag("chat");

                    switch (intent.getIntExtra("type", 0)) {
                        case TdApi.GetMe.CONSTRUCTOR:
                            //updateSidebarMenu(getView());
                            user = service.user;
                            updateSidebarHeader(user);
                            break;
                        case TdApi.GetChats.CONSTRUCTOR:
                            //userId = service.userId;
                            chats = service.chats;
                            //userId = user.id;
                            Log.d("aaa 0", "" + userId);
                            if (listView.getAdapter() == null) {
                                MessagesAdapter chatsAdapter = new MessagesAdapter(getActivity(), chats);
                                //listView.setOnTouchListener(scrollListView);
                                listView.setOnScrollListener(scrollListener);
                                listView.setAdapter(chatsAdapter);
                                listView.setOnItemClickListener(listener);
                                //listView.setOnItemLongClickListener(longClickListener);
                                //client.send(new TdApi.GetMe(), getMe);
                            } else {
                                listView.invalidateViews();
                            }
                            isDownloadedAllChats = service.isDownloadedAllChats;
                            isLoading = service.isLoading;

                            if (client == null) client = TG.getClientInstance();
                            client.send(new TdApi.GetStickers(), new Client.ResultHandler() {
                                @Override
                                public void onResult(TdApi.TLObject object) {
                                    Log.d("stickers", object.toString());
                                    for (int i = 0; i < ((TdApi.Stickers) object).stickers.length; i++) {
                                        downloadFile(((TdApi.Stickers) object).stickers[i].thumb.photo.id);//sticker.id);
                                    }
                                }
                            });

                            if (getActivity().getIntent().getIntExtra("type", -1) == TdApi.UpdateNewMessage.CONSTRUCTOR) {
                                showChat(getActivity().getIntent().getIntExtra("pos", 0));
                            }

                            break;

                        case TdApi.UpdateNewMessage.CONSTRUCTOR:
                            listView.invalidateViews();
                            break;
                        case TdApi.UpdateChatReadInbox.CONSTRUCTOR:
                        case TdApi.UpdateFile.CONSTRUCTOR:
                        case TdApi.UpdateUserStatus.CONSTRUCTOR:
                        case TdApi.UpdateNotificationSettings.CONSTRUCTOR:
                            int pos = intent.getIntExtra("pos", -1);
                            if (pos != -1) updateListItem(pos);
                            break;

                        case Util.UpdateLineStatus:
                            setLineStatus(TGFunc.isOnline(getActivity()));
                            break;

                        case Util.UpdateCurrentUser:
                            updateSidebarHeader(user);
                            break;

                        case Util.UpdateEmoji:
                            Log.d("UpdateEmoji", "");
                            if (fragment != null) {
                                fragment.updateEmoji();
                            }
                            break;
                        case Util.UpdateVoiceProgress:
                            if (fragment != null) {
                                fragment.updateVoiceProgress();
                            }
                            break;
                        case Util.VoiceStopped:
                            if (fragment != null) {
                                fragment.updateListView();
                            }
                            break;
                        case Util.LogOut:
                            logOut();
                            break;
                    }


                }
            });
        }
    };


    AdapterView.OnItemClickListener listener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            showChat(position);
        }
    };

    AbsListView.OnScrollListener scrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (!isDownloadedAllChats && !isLoading && firstVisibleItem + visibleItemCount + 5 >= totalItemCount) {
                isLoading = true;
                //getChats();
                sendRequest(TdApi.GetChats.CONSTRUCTOR);
            }
        }
    };

    private final long mFrequency = 100;
    private final int TICK_WHAT = 2;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message m) {
            TGFunc.updatePlayer(getActivity(), getView());
            sendMessageDelayed(Message.obtain(this, TICK_WHAT), mFrequency);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle(R.string.messages);
        View rootView = inflater.inflate(R.layout.sidebar_fragment, container, false);
        listView = (ListView) rootView.findViewById(R.id.listView);
        listView.setEmptyView(rootView.findViewById(android.R.id.empty));
        userId = getPreferences().getInt("id", 0);
        addSlideMenu(rootView);
        sidebarMenulistView.setOnItemClickListener(menuListener);
        client = TG.getClientInstance();
        IntentFilter filter = new IntentFilter(TGFunc.BROADCAST_ACTION);
        getActivity().registerReceiver(br, filter);
        setHasOptionsMenu(true);
        onFirstStart();
        mHandler.sendMessageDelayed(Message.obtain(mHandler, TICK_WHAT), mFrequency);
        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(br);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (this.menu != null) return;
        inflater.inflate(R.menu.messages, menu);
        this.menu = menu;
        updateMenu();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_lock_open:
                Util.getPreferences().edit().putBoolean("appLocked", true).commit();
                updateMenu();
                break;
            case R.id.action_lock_close:
                Util.getPreferences().edit().putBoolean("appLocked", false).commit();
                updateMenu();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void updateMenu() {
        Log.d("updateMenu", "");
        if (menu == null || menu.size() < 2) return;
        menu.getItem(0).setVisible(false);
        menu.getItem(1).setVisible(false);
        if (Util.getPreferences().getBoolean("passcodeEnabled", false)) {
            Log.d("updateMenuS", ""+Util.getPreferences().getBoolean("appLocked", false));
            menu.getItem(Util.getPreferences().getBoolean("appLocked", false) ? 1 : 0).setVisible(true);
        }
    }

    public void updateListView() {
        Log.d("clPl", "12");
        listView.invalidateViews();
    }

    private void onFirstStart() {
        if (TGFunc.isFirstTime()) {
            TGFunc.setFirstTimeState(getActivity(), false);
            ((MainActivity)getActivity()).getService().getPrimaryData();
            saveRecentEmoji();
        }
    }

    private void saveRecentEmoji() {
        long[] emojis = {Emoji.data[1][0], Emoji.data[1][1], Emoji.data[5][2],
                Emoji.data[1][5], Emoji.data[1][15], Emoji.data[1][26]};
        for (long emoji : emojis) {
            ChatFragment.addRecentEmoji(emoji);
        }
    }

    private void sendRequest(int type, Object... args) {
        ((MainActivity) getActivity()).getService().sendRequest(type, args);
    }

    private void downloadFile(int id) {
        client.send(new TdApi.DownloadFile(id), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {

            }
        });
    }

    private void showChat(int position) {
        TdApi.Chat chat = chats.get(position);
        Bundle bundle = new Bundle();
        Log.d("aaa 2", ""+userId);
        bundle.putLong("chatId", chat.id);
        bundle.putInt("userId", user.id);

        ChatFragment fragment = new ChatFragment();
        fragment.setArguments(bundle);
        fragment.chat = chat;
        fragment.users.add(user);

        if (Util.isTablet) {
            getFragmentManager().beginTransaction().replace(R.id.container_right, fragment, "chat").commit();
        } else {
            getFragmentManager().beginTransaction()//.setCustomAnimations(R.anim.enter_from_right, 0, 0, R.anim.exit_to_right)
                    .add(R.id.container, fragment, "chat").addToBackStack(null).commit();
        }
    }

    private void updateListItem(int index) {
        Log.d("updateListItem i = ", ""+index);
        if (getActivity() == null) return;
        if (index >= chats.size()) return;
        TdApi.Chat chat = chats.get(index);
        LinearLayout view = (LinearLayout) listView.getChildAt(index - listView.getFirstVisiblePosition());
        if (view == null) {
            return;
        }
        view.removeAllViews();
        view.addView(TGFunc.getChatView(getActivity(), chat));
    }


    public void setLineStatus(boolean status) {
        if (isOnline != status) {
            isOnline = status;
            if (getActivity() != null) {
                ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
                Log.d("actionBar != null", ""+(actionBar != null));
                if (actionBar != null) {
                    if (isOnline) {
                        actionBar.setTitle(R.string.messages);
                    } else {
                        actionBar.setTitle(R.string.waiting_connection);
                    }
                }
            }
            if (listView != null) listView.invalidateViews();
        }
    }

    public void deleteTopMessage(long chatId) {
        int pos = TGFunc.getChatPositionById(chats, chatId);
        if (pos == -1) return;
        TdApi.Chat chat = chats.get(pos);
        chat.topMessage.message = new TdApi.MessageText("");
        updateListItem(pos);
    }

    public void deleteChat(long chatId) {
        int pos = TGFunc.getChatPositionById(chats, chatId);
        Log.d("posChat", ""+pos);
        if (pos == -1) return;
    }

    public void decUnreadCount(long chatId) {
        int pos = TGFunc.getChatPositionById(chats, chatId);
        if (pos == -1) return;
        TdApi.Chat chat = chats.get(pos);
        chat.unreadCount--;
        updateListItem(pos);
    }

}
