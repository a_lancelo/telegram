package com.kand.telegram.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.kand.telegram.R;
import com.kand.telegram.logic.SwipeView;
import com.kand.telegram.logic.TGFunc;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

public class ChangeNameFragment extends Fragment {

    private EditText firstName, lastName;
    public TdApi.User user;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.edit_name, container, false);
        TGFunc.setToolbar(getActivity(), rootView, getString(R.string.edit_name), true);
        firstName = (EditText) rootView.findViewById(R.id.firstName);
        lastName = (EditText) rootView.findViewById(R.id.lastName);
        firstName.setText(user.firstName);
        lastName.setText(user.lastName);
        setHasOptionsMenu(true);
        return new SwipeView(rootView);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.auth_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_check:
                if (firstName.getText().length() != 0) {
                    Client client = TG.getClientInstance();
                    if (client != null) {
                        client.send(new TdApi.ChangeName(firstName.getText().toString(), lastName.getText().toString()), new Client.ResultHandler() {
                            @Override
                            public void onResult(TdApi.TLObject object) {
                                Log.d("ChangeName", ""+object.toString());
                                user.firstName = firstName.getText().toString();
                                user.lastName = lastName.getText().toString();
                                TGFunc.hideSoftKeyboard(getActivity());
                                getFragmentManager().popBackStack();
                                getFragmentManager().findFragmentByTag("settings").
                                        onActivityResult(109, Activity.RESULT_OK, null);
                            }
                        });
                    }
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
