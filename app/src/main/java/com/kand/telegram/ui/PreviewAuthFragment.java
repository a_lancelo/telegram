package com.kand.telegram.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kand.telegram.R;
import com.kand.telegram.logic.TGFunc;

import java.io.File;

public class PreviewAuthFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (TGFunc.isFirstTime()) {
            TGFunc.deleteRecursive(new File(TGFunc.getDir()));
        }

        View rootView = inflater.inflate(R.layout.intro_layout, container, false);

        ((TextView)rootView.findViewById(R.id.preview_text)).setText(Html.fromHtml(getString(R.string.preview_text)));

        try {
            ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        } catch (NullPointerException e) {}
        rootView.findViewById(R.id.start_messaging_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction().add(R.id.container, new AuthFragment(), "auth").addToBackStack(null).commit();
            }
        });        
        return rootView;
    }

}
