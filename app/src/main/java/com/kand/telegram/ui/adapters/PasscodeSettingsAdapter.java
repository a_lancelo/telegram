package com.kand.telegram.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.kand.telegram.R;
import com.kand.telegram.logic.Util;

public class PasscodeSettingsAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    int [] icons = {R.drawable.ic_account, R.drawable.ic_phone, 0, R.drawable.ic_lock};
    int [] titles = {R.string.passcode_lock, R.string.change_passcode, R.string.change_passcode_info, R.string.auto_lock, R.string.auto_lock_info};
    private CompoundButton.OnCheckedChangeListener listener;

    public PasscodeSettingsAdapter(Context context, CompoundButton.OnCheckedChangeListener listener) {
        ctx = context;
        this.listener = listener;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if (Util.getPreferences().getBoolean("passcodeEnabled", false)) {
            return titles.length;
        }
        return titles.length-2;
    }

    @Override
    public Object getItem(int position) {
        return titles[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        if (position == 0 || position == 1 || position == 3) {
            view = lInflater.inflate(R.layout.settings_item_4, parent, false);
        }
        if (position == 0) {
            SwitchCompat switchV = (SwitchCompat) view.findViewById(R.id.switchV);
            switchV.setVisibility(View.VISIBLE);
            switchV.setChecked(Util.getPreferences().getBoolean("passcodeEnabled", false));//TGFunc.getNotificationState(context));
            switchV.setOnCheckedChangeListener(listener);//onCheckedChangeListener);
        }
        if (position == 2 || position == 4) {
            view = lInflater.inflate(R.layout.settings_item_3, parent, false);
        }
        if (position == 3) {
            TextView text = (TextView) view.findViewById(R.id.text);
            text.setVisibility(View.VISIBLE);
            text.setText(ctx.getResources().getStringArray(R.array.types_autolock)[Util.getPreferences().getInt("type_auto_lock", 2)]);
        }
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setVisibility(View.VISIBLE);
        title.setText(titles[position]);
        if (position == 1 && !Util.getPreferences().getBoolean("passcodeEnabled", false)) {
            //title.setEnabled(false);
            title.setTextColor(Color.GRAY);
        }
        return view;
    }

    public boolean isEnabled(int position) {
        if (position == 1 && !Util.getPreferences().getBoolean("passcodeEnabled", false)) {
            return false;
        }
        return true;
    }

}