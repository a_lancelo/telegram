package com.kand.telegram.ui;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.kand.telegram.R;
import com.kand.telegram.logic.CircleTransform;
import com.kand.telegram.logic.SwipeView;
import com.kand.telegram.logic.Util;
import com.kand.telegram.logic.BigToolbar;
import com.kand.telegram.logic.TGFunc;
import com.kand.telegram.logic.TGService;
import com.kand.telegram.ui.adapters.SettingsAdapter;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

public class SettingsFragment extends Fragment {

    public TdApi.User user;
    private ListView listView;

    private DialogInterface.OnClickListener okLogOutDialog = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            logOut();
        }
    };

    AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (user.username.length() == 0) position++;
            if (position == 4) {
                if (Util.getPreferences().getBoolean("passcodeEnabled", false)) {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("checkPassSettings", true);
                    Fragment fragment = new PasscodeFragment();
                    fragment.setArguments(bundle);

                    getActivity().getSupportFragmentManager().beginTransaction()
                            .add(R.id.container, fragment).addToBackStack(null).commit();
                } else {
                    getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, 0, 0, R.anim.exit_to_right)
                            .add(R.id.container, new PasscodeSettingsFragment(), "passcode_settings").addToBackStack(null).commit();
                }
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.profile, container, false);
        //new BigToolbar(getActivity()).init(inflater, rootView, new SettingsAdapter(getActivity(), user), itemClickListener, user);
        if (user == null) return new SwipeView(rootView);

        Drawable drawable = new BitmapDrawable(getActivity().getResources(),
                CircleTransform.makeCircleBitmap(TGFunc.getUserPlaceholder(getActivity(), user, Util.dp(65))));
        new BigToolbar(getActivity()).show(rootView, user.firstName + " " + user.lastName, getString(R.string.online), user.profilePhoto, drawable, R.drawable.ic_camera);

        listView = (ListView) rootView.findViewById(R.id.listView);
        listView.setAdapter(new SettingsAdapter(getActivity(), user));
        listView.setOnItemClickListener(itemClickListener);

        setHasOptionsMenu(true);
        return Util.isTablet ? rootView : new SwipeView(rootView);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.settings, menu);
        listView.invalidateViews();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getFragmentManager().getBackStackEntryCount() == 1) {
                    //getActivity().getSupportFragmentManager().popBackStack();
                    getFragmentManager().findFragmentByTag("messages").
                            onActivityResult(108, Activity.RESULT_OK, null);
                }
                break;
            case R.id.action_edit_name:
                ChangeNameFragment fragment = new ChangeNameFragment();
                fragment.user = user;
                getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, 0, 0, R.anim.exit_to_right)
                        .add(R.id.container, fragment, "edit_name").addToBackStack(null).commit();
                break;
            case R.id.action_log_out:
                new AlertDialog.Builder(getActivity())
                        .setTitle(R.string.app_name)
                        .setPositiveButton(R.string.ok, okLogOutDialog)
                        .setNegativeButton(R.string.cancel, null)
                        .setMessage(R.string.log_out_text)
                        .show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 109 && resultCode == Activity.RESULT_OK) {
            //((BaseAdapter)((ListView)getView().findViewById(R.id.listView)).getAdapter()).notifyDataSetChanged();//.header.invalidateViews();
            //Log.d("SetFr", "" + 109);
            ((TextView)getView().findViewById(R.id.title)).setText(user.firstName+" "+user.lastName);
        }
        if (requestCode == 110 && resultCode == Activity.RESULT_OK) {
            Log.d("onActivityResult", "2");
            getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, 0, 0, R.anim.exit_to_right)
                    .add(R.id.container, new PasscodeSettingsFragment(), "passcode_settings").addToBackStack(null).commit();
        }
    }

    private void logOut() {
        Client client = TG.getClientInstance();
        if (client == null) return;
        client.send(new TdApi.ResetAuth(), new Client.ResultHandler() {
            @Override
            public void onResult(final TdApi.TLObject object) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("LogOut", object.toString());
                        TGFunc.setFirstTimeState(getActivity(), true);
                        getPreferences().edit().clear().commit();
                        TGService service = ((MainActivity) getActivity()).getService();
                        if (service != null) {
                            Log.d("LogOut", "appService");
                            service.stopSelf();
                        } else {
                            Log.d("LogOut", "appService null");
                        }

                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        fragmentManager.popBackStack();
                        fragmentManager.popBackStack();
                        Fragment fragment = new PreviewAuthFragment();
                        fragmentManager.beginTransaction().replace(R.id.container, fragment, "preview_auth").commit();
                    }
                });
            }
        });
    }

    public SharedPreferences getPreferences() {
        return getActivity().getSharedPreferences("telegram", Context.MODE_PRIVATE);
    }

}
