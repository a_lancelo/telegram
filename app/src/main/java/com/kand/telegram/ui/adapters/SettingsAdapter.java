package com.kand.telegram.ui.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kand.telegram.R;
import com.kand.telegram.logic.Util;

import org.drinkless.td.libcore.telegram.TdApi;

public class SettingsAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    TdApi.User user;
    int [] icons = {0, R.drawable.ic_account, R.drawable.ic_phone, 0, R.drawable.ic_lock};
    String [] titles = {"", "", "", "", ""};
    int [] texts = {0, R.string.username, R.string.phone, 0, R.string.disabled};

    public SettingsAdapter(Context context, TdApi.User user) {
        ctx = context;
        this.user = user;
        titles[1] = "@"+user.username;
        titles[2] = "+"+user.phoneNumber;
        titles[4] = ctx.getString(R.string.passcode_lock);
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if (user.username.length() == 0) {
            return titles.length-1;
        }
        return titles.length;
    }

    @Override
    public Object getItem(int position) {
        return titles[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (position == 0) {
            LinearLayout linearLayout = new LinearLayout(ctx);
            linearLayout.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, Util.dp(120)));
            return linearLayout;
        }
        if (user.username.length() == 0) position++;
        if (position == 3) {
            return lInflater.inflate(R.layout.settings_item_3, parent, false);
        }
        View view = null;
        if (position == 1 || position == 2) {
            view = lInflater.inflate(R.layout.settings_item_1, parent, false);
        }
        if (position == 4) {
            view = lInflater.inflate(R.layout.settings_item_2, parent, false);
            view.findViewById(R.id.text).setVisibility(View.VISIBLE);
            texts[4] = Util.getPreferences().getBoolean("passcodeEnabled", false) ? R.string.enabled : R.string.disabled;
        }
        if (view == null)
            Log.d("nullposv", "" + position);
        if (view.findViewById(R.id.title) == null)
        Log.d("nullpos", ""+position);
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(titles[position]);
        TextView text = (TextView) view.findViewById(R.id.text);
        text.setText(texts[position]);
        ImageView imageView = (ImageView) view.findViewById(R.id.image);
        imageView.setImageResource(icons[position]);
        return view;
    }

}