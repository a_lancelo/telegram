package com.kand.telegram.ui.adapters;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class EmojiPagerAdapter extends PagerAdapter {

    private ArrayList<View> views = new ArrayList<>();
    public EmojiPagerAdapter(ArrayList<View> views) {
        this.views = views;
    }

    public void destroyItem(ViewGroup paramViewGroup, int paramInt, Object paramObject) {
        View localObject;
        localObject = views.get(paramInt);
        paramViewGroup.removeView(localObject);
    }

    public int getCount() {
        return views.size();
    }

    public Object instantiateItem(ViewGroup paramViewGroup, int paramInt) {
        View localObject;
        localObject = views.get(paramInt);
        paramViewGroup.addView(localObject);
        return localObject;
    }

    public boolean isViewFromObject(View paramView, Object paramObject) {
        return paramView == paramObject;
    }

}
