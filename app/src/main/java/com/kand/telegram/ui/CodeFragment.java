package com.kand.telegram.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.kand.telegram.R;
import com.kand.telegram.logic.SwipeView;
import com.kand.telegram.logic.TGFunc;

public class CodeFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.code, container, false);
        TGFunc.setToolbar(getActivity(), rootView, getString(R.string.country), true);
        ListView listView = (ListView) rootView.findViewById(R.id.listView);
        listView.setAdapter(new CodeAdapter(getActivity(), TGFunc.getCountries(), TGFunc.getCodes()));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String country = TGFunc.getCountries()[position];
                String code = TGFunc.getCodes()[position];
                TGFunc.setCountryCode(country, code);
                ((AuthFragment) getFragmentManager().findFragmentByTag("auth")).updateView();
                getFragmentManager().popBackStack();
            }
        });
        setHasOptionsMenu(true);
        return new SwipeView(rootView);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    public class CodeAdapter extends BaseAdapter {
        LayoutInflater lInflater;
        String [] countries;
        String [] codes;

        CodeAdapter(Context context, String [] countries, String [] codes) {
            this.countries = countries;
            this.codes = codes;
            lInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return codes.length;
        }

        @Override
        public Object getItem(int position) {
            return codes[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = lInflater.inflate(R.layout.code_item, parent, false);
            ((TextView) view.findViewById(R.id.country)).setText(countries[position]);
            ((TextView) view.findViewById(R.id.code)).setText("+"+codes[position]);
            return view;
        }
    }
}
