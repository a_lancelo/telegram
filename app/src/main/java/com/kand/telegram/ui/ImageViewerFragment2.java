package com.kand.telegram.ui;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.kand.telegram.R;
import com.kand.telegram.logic.CircleProgressBar;
import com.kand.telegram.logic.TGFunc;

import org.drinkless.td.libcore.telegram.TdApi;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;

public class ImageViewerFragment2 extends Fragment {

    private TdApi.File small, big;
    private long id;
    private ImageViewTouch imageView;
    Bitmap bitmap;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.image_viewer, container, false);
        TGFunc.setToolbar(getActivity(), rootView, "", true);
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(Color.parseColor("#00000000"));
        imageView = (ImageViewTouch) rootView.findViewById(R.id.imageView);
        setHasOptionsMenu(true);
        Log.d("bigSmall0", big.toString()+" "+small.toString());
        String path = big.path.length() != 0 ? big.path : small.path;
        if (path != null) loadImage(path);
        if (big.path.length() == 0) {
            TGFunc.downloadFile(big.id);
            rootView.findViewById(R.id.linearLayout).setVisibility(View.VISIBLE);
        }
        //((MainActivity)getActivity()).getService().imageViewerFragment = this;
        setHasOptionsMenu(true);
        return rootView;
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.image_viewer, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save_to_gallery:
                new SavingImage().execute();
                break;
            case R.id.action_delete:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (bitmap != null) bitmap.recycle();
        bitmap = null;
    }

    public void putPhoto(int id, TdApi.Photo photo) {
        this.id = id;
        small = TGFunc.getSmallestPhoto(photo.photos).photo;
        big = TGFunc.getBetterPhoto(photo.photos).photo;
        Log.d("bigSmall1", " " + id + " " + big.toString() + " " + small.toString());
    }

    public void putPhoto(TdApi.ProfilePhoto photo) {
        id = photo.id;
        small = photo.small;
        big = photo.big;
    }

    private void loadImage(String path) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        bitmap = BitmapFactory.decodeFile(path, options);
        imageView.setImageBitmap(bitmap);

        //imageView.setTranslationX(160);
        //imageView.setTranslationY(160);
        //imageView.getLayoutParams().width = 160;
        //imageView.getLayoutParams().height = 100;
        //imageView.animate().scaleX(2).scaleY(2).setDuration(2000);//.translationXBy(0).translationYBy(0).setDuration(2000);

        if (bitmap != null)
            Log.d("log", String.format("Required size = %s, bitmap size = %sx%s, byteCount = %s",
                    0, bitmap.getWidth(), bitmap.getHeight(), bitmap.getByteCount()));
    }

    public void updateHandler(TdApi.TLObject object) {
        switch (object.getConstructor()) {
            case TdApi.UpdateFile.CONSTRUCTOR:
                Log.d("ShMe Upd file", object.toString());
                updateFile(((TdApi.UpdateFile) object));
                break;
            case TdApi.UpdateFileProgress.CONSTRUCTOR:
                updateFileProgress((TdApi.UpdateFileProgress) object);
                break;
        }
    }

    private void updateFile(TdApi.UpdateFile file) {
        if (big.id == file.file.id) {
            big = file.file;
            loadImage(big.path);
            getView().findViewById(R.id.linearLayout).setVisibility(View.GONE);
        }
    }

    public void updateFileProgress(TdApi.UpdateFileProgress file) {
        if (big.id == file.fileId) {
            float progress = file.ready * 100 / file.size;
            CircleProgressBar progressBar = (CircleProgressBar) getView().findViewById(R.id.progressBar);
            if (progressBar == null) return;
            progressBar.setProgress(progress);
        }
    }

    private class SavingImage extends AsyncTask<Void, Void, Void> {
        private ProgressDialog progress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(getActivity(), getString(R.string.app_name),
                    getString(R.string.save_to_gallery), true);

        }

        @Override
        protected Void doInBackground(Void... params) {
            MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), bitmap, ""+id, "");
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            progress.dismiss();
        }
    }

}
