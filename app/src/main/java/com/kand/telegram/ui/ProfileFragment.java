package com.kand.telegram.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.kand.telegram.R;
import com.kand.telegram.logic.BigToolbar;
import com.kand.telegram.logic.CircleTransform;
import com.kand.telegram.logic.SwipeView;
import com.kand.telegram.logic.TGFunc;
import com.kand.telegram.logic.TGService;
import com.kand.telegram.logic.Util;
import com.kand.telegram.ui.adapters.ProfileAdapter;
import com.kand.telegram.ui.adapters.ProfileBotAdapter;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;
import java.util.Arrays;

public class ProfileFragment extends Fragment {

    public TdApi.User user;
    public TdApi.BotInfo botInfo;
    public TdApi.NotificationSettings notificationSettings;
    private ArrayList<TdApi.Message> messagesMedia = new ArrayList<>();
    private ArrayList<TdApi.Message> messagesAudio = new ArrayList<>();
    private ListView listView;

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (messagesMedia.size() == 0 && messagesAudio.size() == 0) return;
            SharedMediaFragment fragment = new SharedMediaFragment();
            fragment.putMessages(user.id, messagesMedia, messagesAudio);
            getFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, 0, 0, R.anim.exit_to_right)
                    .add(R.id.container, fragment, "shared_media").addToBackStack(null).commit();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.profile, container, false);

        Drawable drawable = new BitmapDrawable(getActivity().getResources(),
                CircleTransform.makeCircleBitmap(TGFunc.getUserPlaceholder(getActivity(), user, Util.dp(65))));
        new BigToolbar(getActivity()).show(rootView, user.firstName+" "+user.lastName, user.id == TGFunc.getUserId(getActivity()) ? getString(R.string.online) : (user.type instanceof TdApi.UserTypeBot ?
                getString(R.string.bot) : TGFunc.getUserStatus(user.status)), user.profilePhoto, drawable, R.drawable.ic_message).
                getFloatingButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                long chatId = ((ChatFragment)getFragmentManager().findFragmentByTag("chat")).chatId;
                if (getFragmentManager().findFragmentByTag("group") == null && user.id == chatId) {
                    getFragmentManager().popBackStack();
                } else {
                    Client client = TG.getClientInstance();
                    if (client == null) return;
                    client.send(new TdApi.GetChat(user.id), new Client.ResultHandler() {
                        @Override
                        public void onResult(final TdApi.TLObject object) {
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    Log.d("pr fr", object.toString());
                                    Bundle bundle = new Bundle();
                                    bundle.putLong("chatId", user.id);
                                    bundle.putInt("userId", user.id);
                                    ChatFragment fragment = new ChatFragment();
                                    fragment.setArguments(bundle);
                                    if (object instanceof TdApi.Chat) {
                                        fragment.chat = (TdApi.Chat) object;
                                    } else {
                                        fragment.isFirstTime = true;
                                    }
                                    fragment.users.add(((MainActivity) getActivity()).getService().user);
                                    fragment.users.add(user);
                                    if (getFragmentManager().findFragmentByTag("group") != null) {
                                        getFragmentManager().popBackStack();
                                    }
                                    getFragmentManager().popBackStack();
                                    getFragmentManager().popBackStack();

                                    if (Util.isTablet) {
                                        getFragmentManager().beginTransaction().replace(R.id.container_right, fragment, "chat").commit();
                                    } else {
                                        getFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, 0, 0, R.anim.exit_to_right)
                                                .add(R.id.container, fragment, "chat").addToBackStack(null).commit();
                                    }
                                    //getFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, 0, 0, R.anim.exit_to_right).add(R.id.container, fragment, "chat").addToBackStack(null).commit();
                                }
                            });
                        }
                    });
                }
            }
        });

        listView = (ListView) rootView.findViewById(R.id.listView);
        listView.setAdapter(user.type instanceof TdApi.UserTypeBot ? new ProfileBotAdapter(getActivity(), user, botInfo, null, 0, listener) :
                new ProfileAdapter(getActivity(), user, null, 0, listener));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("prof pos", ""+position);
                if (position == 4 && user.type instanceof TdApi.UserTypeBot) {
                    getFragmentManager().beginTransaction().add(R.id.container, new SelectGroupFragment(), "selectGroup").addToBackStack(null).commit();
                } else if (position == 1) {
                    if (messagesMedia.size() == 0 && messagesAudio.size() == 0) return;
                    SharedMediaFragment fragment = new SharedMediaFragment();
                    fragment.putMessages(user.id, messagesMedia, messagesAudio);
                    Log.d("messagesAudio1", "" + messagesAudio.size());
                    getFragmentManager().beginTransaction()
                            .add(R.id.container, fragment, "shared_media").addToBackStack(null).commit();
                }
            }
        });

        Client client = TG.getClientInstance();
        if (client != null) {
            client.send(new TdApi.SearchMessages(user.id, "", 0, 100, new TdApi.SearchMessagesFilterPhoto()), new Client.ResultHandler() {
                @Override
                public void onResult(final TdApi.TLObject object) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("SearchMessages", object.toString());
                            if (object instanceof TdApi.Messages) {
                                messagesMedia = new ArrayList<>(Arrays.asList(((TdApi.Messages) object).messages));
                                TGFunc.downloadPreviewPhoto(messagesMedia);
                                if (getActivity() != null)
                                listView.setAdapter(user.type instanceof TdApi.UserTypeBot ? new ProfileBotAdapter(getActivity(), user, botInfo, messagesMedia, ((TdApi.Messages) object).totalCount, listener) :
                                        new ProfileAdapter(getActivity(), user, messagesMedia, ((TdApi.Messages) object).totalCount, listener));
                            }
                        }
                    });
                }
            });
            client.send(new TdApi.SearchMessages(user.id, "", 0, 100, new TdApi.SearchMessagesFilterAudio()), new Client.ResultHandler() {
                @Override
                public void onResult(final TdApi.TLObject object) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("SearchMessages", object.toString());
                            if (object instanceof TdApi.Messages) {
                                messagesAudio = new ArrayList<>(Arrays.asList(((TdApi.Messages) object).messages));
                                Log.d("messagesAudio0", ""+messagesAudio.size());
                            }
                        }
                    });
                }
            });
        }

        setHasOptionsMenu(true);
        return Util.isTablet ? rootView : new SwipeView(rootView);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.profile, menu);
        if (notificationSettings != null) {
            menu.getItem(notificationSettings.muteFor == 0 ? 0 : 1).setVisible(true);
        }
        listView.invalidateViews();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.notifications_on:
            case R.id.notifications_off:
                TGFunc.showNotificationPopupMenu(getView().findViewById(item.getItemId()), user.id, notificationSettings);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 111 && resultCode == Activity.RESULT_OK) {
            TGService service = ((MainActivity) getActivity()).getService();
            if (service == null) return;
            getFragmentManager().popBackStack();
            getFragmentManager().popBackStack();
            getFragmentManager().popBackStack();

            TdApi.Chat chat = TGFunc.getChatById(service.chats, data.getLongExtra("selectedGroupId", 0));
            Bundle bundle = new Bundle();
            bundle.putLong("chatId", chat.id);
            bundle.putInt("userId", service.user.id);
            bundle.putInt("addBot", user.id);
            ChatFragment fragment = new ChatFragment();
            fragment.setArguments(bundle);
            fragment.chat = chat;
            fragment.users.add(service.user);
            getFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, 0, 0, R.anim.exit_to_right).add(R.id.container, fragment, "chat").addToBackStack(null).commit();
        }
    }

    public void updateFile(TdApi.UpdateFile file) {
        if (messagesMedia.size() == 0) return;
        if (TGFunc.updateMessagesByFile(messagesMedia, file)) listView.invalidateViews();
    }

    public void updateDeleteMessages(TdApi.UpdateDeleteMessages object) {
        if (messagesMedia.size() == 0) return;
        if (messagesMedia.get(0).chatId != object.chatId) return;
        for (int id : object.messages) {
            for (int i = messagesMedia.size()-1; i >= 0; i--) {
                if (messagesMedia.get(i).id == id) {
                    messagesMedia.remove(i);
                }
            }
        }
        listView.invalidateViews();
    }

}
