package com.kand.telegram.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.kand.telegram.R;
import com.kand.telegram.logic.Util;

public class PinKeyboardAdapter extends BaseAdapter {

    private Context context;
    LayoutInflater inflater;
    String texts[] = {"", "ABC", "DEF", "GHI", "JKL",
            "MNO", "PQRS", "TUV", "WXYZ", "", "+"};

    public PinKeyboardAdapter(Context context) {
        this.context = context;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return 12;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (position == 11) {
            ImageView imageView = new ImageView(context);
            imageView.setImageResource(R.drawable.ic_passcode_delete);
            imageView.setBackgroundResource(R.drawable.pin_key);
            imageView.setLayoutParams(new GridView.LayoutParams(Util.dp(75), Util.dp(75)));
            imageView.setScaleType(ImageView.ScaleType.CENTER);
            return imageView;
        }
        View view = inflater.inflate(R.layout.pin_keyboard_item, parent, false);
        ((TextView)view.findViewById(R.id.title)).setText(position < 9 ? ""+(position+1) : (position == 10 ? "0" : ""));
        ((TextView)view.findViewById(R.id.text)).setText(texts[position]);
        return view;
    }

}