package com.kand.telegram.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.kand.telegram.R;
import com.kand.telegram.logic.SwipeView;
import com.kand.telegram.logic.TGFunc;
import com.kand.telegram.logic.TGService;
import com.kand.telegram.ui.adapters.MessagesAdapter;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;

public class SelectGroupFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.messages, container, false);
        TGFunc.setToolbar(getActivity(), rootView, getString(R.string.select_chat), true);
        TGService service = ((MainActivity) getActivity()).getService();
        if (service != null) {
            final ArrayList<TdApi.Chat> groups = getGroupChats(service.chats);
            ListView listView = (ListView) rootView.findViewById(R.id.listView);
            listView.setAdapter(new MessagesAdapter(getActivity(), groups));
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    getFragmentManager().popBackStack();
                    Intent data = new Intent();
                    data.putExtra("selectedGroupId", groups.get(position).id);
                    Fragment fragment = getFragmentManager().findFragmentByTag("profile");
                    fragment.onActivityResult(111, Activity.RESULT_OK, data);
                }
            });
        }
        setHasOptionsMenu(true);
        return new SwipeView(rootView);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    private ArrayList<TdApi.Chat> getGroupChats(ArrayList<TdApi.Chat> chats) {
        ArrayList<TdApi.Chat> groups = new ArrayList<>();
        for (TdApi.Chat chat: chats) {
            if (chat.type instanceof TdApi.GroupChatInfo) {
                groups.add(chat);
            }
        }
        return groups;
    }

}
