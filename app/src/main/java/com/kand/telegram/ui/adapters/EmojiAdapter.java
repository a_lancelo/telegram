package com.kand.telegram.ui.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.kand.telegram.logic.Emoji;
import com.kand.telegram.logic.Util;

public class EmojiAdapter extends BaseAdapter {

    private Context context;
    private long data[];


    public EmojiAdapter(Context context, long data[]) {
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(context);

        int s = Util.dp(45);
        imageView.setLayoutParams(new GridView.LayoutParams(s, s));
        s = Util.dp(6);
        imageView.setPadding(s, s, s, s);

        imageView.setImageDrawable(Emoji.getEmojiBigDrawable(data[position]));
        return imageView;
    }

}