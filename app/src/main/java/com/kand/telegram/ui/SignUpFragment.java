package com.kand.telegram.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.kand.telegram.R;
import com.kand.telegram.logic.TGFunc;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

public class SignUpFragment extends Fragment {

    EditText firstName, lastName;

    View.OnClickListener setName = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String first = firstName.getText().toString();
            String last = lastName.getText().toString();
            if (first.length() > 0 & last.length() > 0) {
                final ProgressDialog dialog = ProgressDialog.show(getActivity(), "", getString(R.string.loading), true);
                Client client = TG.getClientInstance();
                if (client == null) return;
                client.send(new TdApi.SetAuthName(first, last), new Client.ResultHandler() {
                    @Override
                    public void onResult(final TdApi.TLObject object) {
                        dialog.cancel();
                        Log.d("CHECK SIGNUP", object.toString());
                        if (object instanceof TdApi.AuthStateWaitCode) {
                            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                            CheckAuthFragment fragment = new CheckAuthFragment();
                            fragment.isNewUser = true;
                            fragmentManager.beginTransaction().setCustomAnimations(R.anim.enter_from_right, 0, 0, R.anim.exit_to_right).add(R.id.container, fragment).addToBackStack(null).commit();
                        }
                    }
                });
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.sign_up, container, false);
        TGFunc.setToolbar(getActivity(), rootView, getString(R.string.sign_up), true);
        firstName = (EditText) rootView.findViewById(R.id.firstName);
        lastName = (EditText) rootView.findViewById(R.id.lastName);
        lastName.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    rootView.findViewById(R.id.sign_up).callOnClick();
                    return true;
                }
                return false;
            }
        });

        rootView.findViewById(R.id.sign_up).setOnClickListener(setName);
        setHasOptionsMenu(true);
        return rootView;
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

}
