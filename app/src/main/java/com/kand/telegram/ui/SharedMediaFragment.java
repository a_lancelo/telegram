package com.kand.telegram.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.kand.telegram.R;
import com.kand.telegram.logic.CircleProgressBar;
import com.kand.telegram.logic.SwipeView;
import com.kand.telegram.logic.TGFunc;
import com.kand.telegram.logic.Util;
import com.kand.telegram.ui.adapters.SpinnerAdapter;
import com.squareup.picasso.Picasso;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class SharedMediaFragment extends Fragment {

    private ArrayList<Item> messagesMedia = new ArrayList<>();
    private ArrayList<Item> messagesAudio = new ArrayList<>();
    private ArrayList<Integer> selectedMessages = new ArrayList<>();
    private ActionMode actionMode;
    private ListView listView;
    private boolean multiSelect = false;
    private int type = 0;
    private long fromId;

    private final long mFrequency = 100;
    private final int TICK_WHAT = 2;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message m) {
            TGFunc.updatePlayer(getActivity(), getView());
            sendMessageDelayed(Message.obtain(this, TICK_WHAT), mFrequency);
        }
    };

    AdapterView.OnItemLongClickListener longClickListener = new AdapterView.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            if (!multiSelect) {
                actionMode = ((AppCompatActivity) getActivity()).startSupportActionMode(callback);
                multiSelect = true;
                ArrayList<Item> messages = type == 0 ? messagesMedia : messagesAudio;
                int mId = messages.get((int)((View)view.getParent()).getTag()).messages.get(position).id;//);
                //addSelectedMessage(mId, view);
                selectedMessages.add(mId);
                //((GridView)view.getParent()).getChildAt(position).invalidate();
                actionMode.setTitle(String.valueOf(selectedMessages.size()));
                listView.invalidateViews();
                //for (listView.getChildCount().)
            }
            return true;
        }
    };

    AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            ArrayList<Item> messages = type == 0 ? messagesMedia : messagesAudio;
            if (type == 0) {
                if (multiSelect) {
                    int mId = messages.get((int) ((View) view.getParent()).getTag()).messages.get(position).id;//);
                    int last = selectedMessages.size();
                    for (int i = selectedMessages.size() - 1; i >= 0; i--) {
                        if (selectedMessages.get(i).equals(mId)) {
                            selectedMessages.remove(i);

                            view.findViewById(R.id.check).setVisibility(View.GONE);
                            int p = Util.dp(1);
                            view.setPadding(p, p, p, p);
                        }
                    }
                    if (last == selectedMessages.size()) {
                        selectedMessages.add(mId);

                        view.findViewById(R.id.check).setVisibility(View.VISIBLE);
                        int p = Util.dp(10);
                        view.setPadding(p, p, p, p);
                    }
                    actionMode.setTitle(String.valueOf(selectedMessages.size()));
                    if (selectedMessages.size() == 0) actionMode.finish();
                } else {
                    TdApi.Message message = messages.get((int) ((View) view.getParent()).getTag()).messages.get(position);
                    ImageViewerFragment fragment = new ImageViewerFragment();
                    fragment.putPhoto(message.chatId, message.id, ((TdApi.MessagePhoto) message.message).photo);
                    //Toast.makeText(getActivity(), ""+message.id+" "+((TdApi.MessagePhoto) message.message).photo.photos, Toast.LENGTH_LONG).show();
                    TdApi.PhotoSize photo = TGFunc.getSmallestPhoto(((TdApi.MessagePhoto) message.message).photo.photos);
                    Log.d("clickSM", "" + message.id + " " + photo.photo.id + " " + photo.photo.persistentId + " " + photo.photo.path);

                    if (Util.isTablet) {
                        getActivity().findViewById(R.id.container_fullscreen).setVisibility(View.VISIBLE);
                        getFragmentManager().
                                beginTransaction().add(R.id.container_fullscreen, fragment, "imageViewer").addToBackStack(null).commit();
                    } else {
                        getFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, 0, 0, R.anim.exit_to_right).add(R.id.container, fragment, "imageViewer").addToBackStack(null).commit();
                    }
                }
            } else {
                if (multiSelect) {
                    int mId = messages.get((int) ((View) view.getParent()).getTag()).messages.get(position).id;//);
                    int last = selectedMessages.size();
                    for (int i = selectedMessages.size() - 1; i >= 0; i--) {
                        if (selectedMessages.get(i).equals(mId)) {
                            selectedMessages.remove(i);

                            view.findViewById(R.id.check).setVisibility(View.GONE);
                            view.setBackgroundColor(Color.WHITE);
                        }
                    }
                    if (last == selectedMessages.size()) {
                        selectedMessages.add(mId);

                        view.findViewById(R.id.check).setVisibility(View.VISIBLE);
                        view.setBackgroundColor(Color.parseColor("#f5f5f5"));
                        //int p = Util.dp(10);
                        //view.setPadding(p, p, p, p);
                    }
                    actionMode.setTitle(String.valueOf(selectedMessages.size()));
                    if (selectedMessages.size() == 0) actionMode.finish();
                }
            }
        }
    };

    private ActionMode.Callback callback = new ActionMode.Callback() {

        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            multiSelect = true;
            mode.getMenuInflater().inflate(R.menu.shared_media, menu);
            return true;
        }

        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            Log.d("onActionItemClicked", "");
            //doFileOperation(item.getItemId());
            ArrayList<Item> messages = type == 0 ? messagesMedia : messagesAudio;
            switch (item.getItemId()) {
                case R.id.delete:
                    Client client = TG.getClientInstance();
                    if (client != null) {
                        int[] ids = new int[selectedMessages.size()];
                        for (int i = 0; i < selectedMessages.size(); i++) {
                            ids[i] = selectedMessages.get(i);
                            for (int j = 0; j < messages.size(); j++) {
                                ArrayList<TdApi.Message> _messages = messages.get(j).messages;
                                for (int k = 0; k < _messages.size(); k++) {
                                    if (_messages.get(k).id == ids[i]) {
                                        _messages.remove(k);
                                        break;
                                    }
                                }
                            }
                        }
                        listView.setAdapter(new SharedMediaAdapter(getActivity()));//, 0, messages, longClickListener, itemClickListener));
                        long chatId = messages.get(0).messages.get(0).chatId;
                        client.send(new TdApi.DeleteMessages(chatId, ids), new Client.ResultHandler() {
                            @Override
                            public void onResult(TdApi.TLObject object) {
                                Log.d("resultDM", object.toString());
                            }
                        });
                        ((ChatFragment) getFragmentManager().findFragmentByTag("chat")).updateHandler(new TdApi.UpdateDeleteMessages(chatId, ids));
                        selectedMessages.clear();
                    }
                    break;
            }
            mode.finish();
            return false;
        }

        public void onDestroyActionMode(ActionMode mode) {
            Log.d("onDestroyActionMode", "");
            multiSelect = false;
            selectedMessages.clear();
            listView.invalidateViews();
            /*for (int i = 0; i < list.getChildCount(); i++){
                list.getChildAt(i).setBackgroundColor(-1);
            }*/
        }

        public void onItemCheckedStateChanged(ActionMode mode,
                                              int position, long id, boolean checked) {
        }

    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle(R.string.shared_media);
        View rootView = inflater.inflate(R.layout.shared_media, container, false);
        TGFunc.setToolbar(getActivity(), rootView, getString(R.string.shared_media), true);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_back_grey);

        listView = (ListView) rootView.findViewById(R.id.listView);
        ((MainActivity)getActivity()).getService().sharedMediaFragment = this;
        listView.setAdapter(new SharedMediaAdapter(getActivity()));
        try {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
            Spinner spinner = (Spinner) rootView.findViewById(R.id.spinner);
            spinner.setAdapter(new SpinnerAdapter(getActivity(), new String[]{getString(R.string.shared_media), getString(R.string.audio_files)}, true));
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    type = position;
                    Log.d("onItemSelected", "" + position);
                    listView.setAdapter(new SharedMediaAdapter(getActivity()));
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        } catch (Exception e) { Log.d("passcode", e.toString());}

        listView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.d("onKeyView msg", "KEYCODE = " + keyCode + " action = " + event.getAction());
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    Log.d("onKeyView msg", "KEYCODE_BACK");
                    if (multiSelect) {
                        actionMode.finish();
                    } else {
                        getFragmentManager().popBackStack();
                    }
                    return true;
                }
                return false;
            }
        });
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            boolean isDowloading = false;
            boolean isDowloadingAll = false;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (!isDowloadingAll && !isDowloading && firstVisibleItem + visibleItemCount + 2 >= totalItemCount) {
                    isDowloading = true;
                    Client client = TG.getClientInstance();
                    if (client != null) {

                        final int hType = type;
                        int lastId = 0;
                        final ArrayList<Item> messages = hType == 0 ? messagesMedia : messagesAudio;
                        if (messages.size() > 0) {
                            Item item = messages.get(messages.size() - 1);
                            if (item.messages.size() > 0) {
                                lastId = item.messages.get(item.messages.size() - 1).id;
                            }
                        }
                        client.send(new TdApi.SearchMessages(fromId, "", lastId, 100, hType == 0 ? new TdApi.SearchMessagesFilterPhoto() : new TdApi.SearchMessagesFilterAudio()), new Client.ResultHandler() {
                            @Override
                            public void onResult(final TdApi.TLObject object) {
                                new Handler(Looper.getMainLooper()).post(new Runnable() {
                                    @Override
                                    public void run() {
                                        Log.d("SearchMessages", object.toString());
                                        if (object instanceof TdApi.Messages) {
                                            TdApi.Messages newMessages = ((TdApi.Messages) object);
                                            addNewMessages(hType, newMessages);
                                            listView.invalidateViews();
                                            int totalCount = 0;
                                            for (Item item : messages) {
                                                totalCount += item.messages.size();
                                            }
                                            Log.d("totalCount", "" + newMessages.totalCount + " " + totalCount);
                                            if (newMessages.totalCount == totalCount) {
                                                isDowloadingAll = true;
                                            }
                                        }
                                        isDowloading = false;
                                    }
                                });
                            }
                        });
                    }
                }
            }
        });
        listView.requestFocus();
        mHandler.sendMessageDelayed(Message.obtain(mHandler, TICK_WHAT), mFrequency);
        setHasOptionsMenu(true);
        return new SwipeView(rootView);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (actionMode != null) actionMode.finish();
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    public void putMessages(long fromId, ArrayList<TdApi.Message> messagesMedia, ArrayList<TdApi.Message> messagesAudio) {
        this.fromId = fromId;
        this.messagesMedia = sortMessages(messagesMedia);
        this.messagesAudio = sortMessages(messagesAudio);
    }

    private void addNewMessages(int type, TdApi.Messages object) {
        ArrayList<Item> messages = type == 0 ? messagesMedia : messagesAudio;
        ArrayList<TdApi.Message> newMessages = new ArrayList<>(Arrays.asList(object.messages));
        TGFunc.downloadPreviewPhoto(newMessages);
        ArrayList<Item> items = sortMessages(newMessages);
        if (items.size() > 0 && messages.size() > 0 && items.get(0).title.equals(messages.get(messages.size()-1).title)) {
            for (TdApi.Message message : items.get(0).messages) {
                messages.get(messages.size() - 1).messages.add(message);
            }
            for (Float progress : items.get(0).progresses) {
                messages.get(messages.size() - 1).progresses.add(progress);
            }
            items.remove(0);
        }
        for (Item item : items) {
            messages.add(item);
        }
    }

    public void updateHandler(TdApi.TLObject object) {
        switch (object.getConstructor()) {
            case TdApi.UpdateFile.CONSTRUCTOR:
                updateFile(((TdApi.UpdateFile) object));
                break;
            case TdApi.UpdateFileProgress.CONSTRUCTOR:
                updateFileProgress((TdApi.UpdateFileProgress) object);
                break;
        }
    }

    public void updateFile(TdApi.UpdateFile file) {
        for (int i = 0; i < messagesMedia.size(); i++) {
            if (TGFunc.updateMessagesByFile(messagesMedia.get(i).messages, file)) listView.invalidateViews();
        }
        for (int i = 0; i < messagesAudio.size(); i++) {
            if (TGFunc.updateMessagesByFile(messagesAudio.get(i).messages, file)) listView.invalidateViews();
        }
    }

    public void updateFileProgress(TdApi.UpdateFileProgress file) {
        for (int i = 0; i < messagesAudio.size(); i++) {
            Log.d("SM", "0; "+i);
            int index = TGFunc.getMessagePosByFileId(getActivity(), messagesAudio.get(i).messages, file);
            if (index == -1) break;
            if (file.size != 0) {
                Log.d("SM", "1; "+i+" "+index);
                float progress = file.ready * 100 / file.size;
                messagesAudio.get(i).progresses.set(index, progress);
                //index = i;
                Log.d("SM", "2");
                final LinearLayout listItem = (LinearLayout) listView.getChildAt(i - listView.getFirstVisiblePosition());
                if (listItem == null) break;
                Log.d("SM", "3");
                final GridView gridView = (GridView) listItem.findViewById(R.id.gridView);//LinearLayout) listView.getChildAt(i - listView.getFirstVisiblePosition());
                if (gridView == null) break;
                Log.d("SM", "4");
                if (!(gridView.getChildAt(index - listView.getFirstVisiblePosition()) instanceof LinearLayout)) break;
                final LinearLayout gridItem = (LinearLayout) gridView.getChildAt(index - listView.getFirstVisiblePosition());
                Log.d("SM", "5");
                CircleProgressBar progressBar = (CircleProgressBar) gridItem.findViewById(R.id.progressBar);
                if (progressBar == null) break;
                progressBar.setProgress(progress);
            }
        }
    }

    private ArrayList<Item> sortMessages(ArrayList<TdApi.Message> messages) {
        ArrayList<Item> items = new ArrayList<>();
        String prevDate = null;
        for (TdApi.Message message : messages) {
            if (message.message instanceof TdApi.MessagePhoto || message.message instanceof TdApi.MessageAudio) {
                String curDate = new SimpleDateFormat("MMMM yyyy").format(new Date(message.date * 1000L));
                if (curDate.equals(prevDate)) {
                    items.get(items.size() - 1).messages.add(message);
                    items.get(items.size() - 1).progresses.add(0f);
                } else {
                    Item item = new Item();
                    item.title = curDate;
                    item.messages.add(message);
                    item.progresses.add(0f);
                    items.add(item);
                    prevDate = curDate;
                }
            }
        }
        return items;
    }

    public class Item {
        public String title;
        public ArrayList<TdApi.Message> messages = new ArrayList<>();
        public ArrayList<Float> progresses = new ArrayList<>();
    }


    public class SharedMediaAdapter extends BaseAdapter {
        Context context;
        LayoutInflater lInflater;
        ArrayList<SharedMediaFragment.Item> items;
        private int width, side;

        public SharedMediaAdapter(Context context) {
            this.context = context;
            this.items = type == 0 ? messagesMedia : messagesAudio;
            lInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (Util.isTablet) {
                width = Util.dp(480);
            } else {
                Point size = new Point();
                ((Activity)context).getWindowManager().getDefaultDisplay().getSize(size);
                width = size.x;
            }
            side = width/(width / Util.dp(100));
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = lInflater.inflate(R.layout.shared_media_item, parent, false);
            SharedMediaFragment.Item item = items.get(position);
            ((TextView) view.findViewById(R.id.title)).setText(item.title);
            GridView gridView = (GridView) view.findViewById(R.id.gridView);
            gridView.setTag(position);
            if (type == 0) {
                gridView.setNumColumns(width / Util.dp(100));
                Log.d("colwidt", ""+ width + " "+width / Util.dp(100));
                //int side = width/(width / Util.dp(100));
                int height = (item.messages.size() / (width / Util.dp(100)) +
                        (item.messages.size() % (width / Util.dp(100)) != 0 ? 1 : 0)) * side;
                gridView.setLayoutParams(new LinearLayout.LayoutParams(-1, height));
                //Log.d("colwidt", width + " " + gridView.getNumColumns() + " " + item.messages.size()+" "+type);
                gridView.setAdapter(new SharedMediaImageAdapter(context, item.messages));
            } else {
                gridView.setNumColumns(1);
                int height = item.messages.size() * Util.dp(60);// * Util.dp(100);
                gridView.setLayoutParams(new LinearLayout.LayoutParams(-1, height));
                Log.d("colwidt", width + " " + gridView.getNumColumns() + " " + item.messages.size()+" "+type);
                gridView.setAdapter(new SharedMediaAudioAdapter(context, item.messages));
            }
            gridView.setOnItemLongClickListener(longClickListener);
            gridView.setOnItemClickListener(itemClickListener);
            return view;
        }

        private class SharedMediaImageAdapter extends BaseAdapter {

            private Context context;
            private LayoutInflater lInflater;
            private ArrayList<TdApi.Message> items;

            public SharedMediaImageAdapter(Context context, ArrayList<TdApi.Message> items) {
                this.context = context;
                this.items = items;
                lInflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }

            @Override
            public int getCount() {
                return items.size();
            }

            @Override
            public Object getItem(int position) {
                return items.get(position);
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TdApi.Message message = items.get(position);
                RelativeLayout relativeLayout = (RelativeLayout) lInflater.inflate(R.layout.shared_media_image_item, null);
                relativeLayout.setLayoutParams(new GridView.LayoutParams(side, side));
                final ImageView imageView = (ImageView) relativeLayout.findViewById(R.id.imageView);
                imageView.setAdjustViewBounds(true);
                if (multiSelect) {
                    relativeLayout.findViewById(R.id.uncheck).setVisibility(View.VISIBLE);
                    int[] ids = new int[selectedMessages.size()];
                    for (int i = 0; i < selectedMessages.size(); i++) {
                        ids[i] = selectedMessages.get(i);
                        if (message.id == ids[i]) {
                            relativeLayout.setBackgroundColor(Color.parseColor("#f5f5f5"));
                            relativeLayout.findViewById(R.id.check).setVisibility(View.VISIBLE);
                            int p = Util.dp(10);
                            imageView.setPadding(p, p, p, p);
                            break;
                        }
                    }
                }
                if (message.message instanceof TdApi.MessagePhoto) {
                    Picasso.with(context)
                            .load(new File(TGFunc.getSmallestPhoto(((TdApi.MessagePhoto) message.message).photo.photos).photo.path))
                            .resize(side, side)
                            .centerCrop()
                            .into(imageView);
                }
                return relativeLayout;
            }
        }

        private class SharedMediaAudioAdapter extends BaseAdapter {
            private  ArrayList<TdApi.Message> items;

            public SharedMediaAudioAdapter(Context context, ArrayList<TdApi.Message> items) {
                this.items = items;
                lInflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            }

            @Override
            public int getCount() {
                return items.size();
            }

            @Override
            public Object getItem(int position) {
                return items.get(position);
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TdApi.Message message = items.get(position);
                View view = TGFunc.getMessageAudioView(message, 0f);
                if (multiSelect) {
                    view.findViewById(R.id.download).setVisibility(View.INVISIBLE);
                    view.findViewById(R.id.download_pause).setVisibility(View.INVISIBLE);
                    view.findViewById(R.id.play).setVisibility(View.INVISIBLE);
                    view.findViewById(R.id.pause).setVisibility(View.INVISIBLE);
                    view.findViewById(R.id.uncheck).setVisibility(View.VISIBLE);
                    int[] ids = new int[selectedMessages.size()];
                    for (int i = 0; i < selectedMessages.size(); i++) {
                        ids[i] = selectedMessages.get(i);
                        if (message.id == ids[i]) {
                            view.setBackgroundColor(Color.parseColor("#f5f5f5"));
                            view.findViewById(R.id.check).setVisibility(View.VISIBLE);
                            break;
                        }
                    }
                }
                return view;
            }

        }

    }
}
