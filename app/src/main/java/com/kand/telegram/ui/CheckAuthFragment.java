package com.kand.telegram.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.kand.telegram.R;
import com.kand.telegram.logic.TGFunc;
import com.kand.telegram.logic.Util;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

public class CheckAuthFragment extends Fragment {

    TextView wrongCode;
    EditText code;
    boolean isWrongCode = false, isNewUser, autoMode = false, isLoading = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.check_auth, container, false);
        TGFunc.setToolbar(getActivity(), rootView, getString(R.string.activation_code), true);
        InputMethodManager imgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imgr.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        wrongCode = (TextView) rootView.findViewById(R.id.wrongCode);
        TextView text = (TextView) rootView.findViewById(R.id.text);
        text.setText(text.getText().toString() + "  " + Html.fromHtml("<b>" + TGFunc.getPhoneNumber() + "</b>"));
        code = (EditText) rootView.findViewById(R.id.code);
        code.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    enterCode();
                    return true;
                }
                return false;
            }
        });

        code.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if (isWrongCode) {
                    isWrongCode = false;
                    wrongCode.setVisibility(View.INVISIBLE);
                }

                if (s.length() == 5) {
                    autoMode = true;
                    enterCode();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        setHasOptionsMenu(true);
        return rootView;
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.auth_menu, menu);
        menu.getItem(isLoading ? 1 : 0).setVisible(true);
        menu.getItem(isLoading ? 0 : 1).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_check:
                if (getFragmentManager().getBackStackEntryCount() == 2) {
                    enterCode();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    private void enterCode() {
        if (code.getText().toString().length() == 0) return;
        isLoading = true;
        getActivity().invalidateOptionsMenu();
        Client client = TG.getClientInstance();
        if (client == null) return;
        client.send(new TdApi.SetAuthCode(code.getText().toString()), new Client.ResultHandler() {
            @Override
            public void onResult(final TdApi.TLObject object) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("Login CheckAuth", object.toString());
                        if (object instanceof TdApi.AuthStateOk) {
                            hideSoftKeyboard(getActivity());
                            getFragmentManager().popBackStack();
                            getFragmentManager().popBackStack();
                            if (isNewUser) getFragmentManager().popBackStack();
                            showMessages();
                        } else if (object instanceof TdApi.AuthStateWaitPassword) {
                            TwoStepAuthFragment fragment = new TwoStepAuthFragment();
                            fragment.hint = ((TdApi.AuthStateWaitPassword) object).hint;
                            getFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, 0, 0, R.anim.exit_to_right).add(R.id.container, fragment, "two_step_auth").addToBackStack(null).commit();
                        } else {
                            if (!autoMode) {
                                wrongCode.setVisibility(View.VISIBLE);
                                isWrongCode = true;
                            } else {
                                autoMode = false;
                            }
                            isLoading = false;
                            getActivity().invalidateOptionsMenu();
                        }
                    }
                });

            }
        });
    }

    private void showMessages() {
        if (Util.isTablet) {
            getActivity().findViewById(R.id.container_layout_2).setVisibility(View.GONE);
            getActivity().findViewById(R.id.container_layout_1).setVisibility(View.VISIBLE);
            getActivity().findViewById(R.id.container_shadow).setVisibility(View.VISIBLE);
            getFragmentManager().beginTransaction().replace(R.id.container_left, new MessagesFragment(), "messages").commit();
        } else {
            getFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, 0, 0, R.anim.exit_to_right).replace(R.id.container, new MessagesFragment(), "messages").commit();
        }
    }

}
