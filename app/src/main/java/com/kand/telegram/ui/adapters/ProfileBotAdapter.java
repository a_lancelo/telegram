package com.kand.telegram.ui.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kand.telegram.R;
import com.kand.telegram.logic.TGFunc;
import com.kand.telegram.logic.Util;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;

public class ProfileBotAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater lInflater;
    private TdApi.User user;
    private int [] icons = {0, R.drawable.ic_account, R.drawable.ic_about, 0,
            R.drawable.ic_add, 0, R.drawable.ic_attach_gallery};
    private String [] titles = new String[7];//{R.string.first_name, R.string.phone_number, 0, R.string.shared_media};
    private String [] texts = new String[7];
    //private int [] texts = {R.string.username, R.string.about};
    private boolean hasDescription;
    private int totalCount;
    private ArrayList<TdApi.Message> messages;
    private View.OnClickListener sharedListener;

    public ProfileBotAdapter(Context context, TdApi.User user, TdApi.BotInfo botInfo, ArrayList<TdApi.Message> messages, int totalCount, View.OnClickListener sharedListener) {
        this.context = context;
        lInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.user = user;
        this.messages = messages;
        this.totalCount = totalCount;
        titles[1] = "@"+user.username;
        if (botInfo instanceof TdApi.BotInfoGeneral) {
            if (((TdApi.BotInfoGeneral) botInfo).description.length() != 0) {
                hasDescription = true;
                titles[2] = ((TdApi.BotInfoGeneral) botInfo).shareText;
            } else {
                titles[2] = context.getString(R.string.shared_media);
                icons[2] = R.drawable.ic_attach_gallery;
            }
        }
        titles[4] = context.getString(R.string.add_to_group);
        titles[6] = context.getString(R.string.shared_media);

        texts[1] = context.getString(R.string.username);
        texts[2] = context.getString(R.string.about);
        //texts[5] = String.valueOf(messages == null ? 0 : messages.size());
        this.sharedListener = sharedListener;
    }

    @Override
    public int getCount() {
        return hasDescription ? titles.length : 3;
    }

    @Override
    public Object getItem(int position) {
        return titles[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (position == 0) {
            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, Util.dp(120)));
            return linearLayout;
        }
        if (position == 3 || position == 5) {
            return lInflater.inflate(R.layout.settings_item_3, parent, false);
        }
        View view = null;
        if (position == 1 || position == 2) {
            view = lInflater.inflate(R.layout.settings_item_1, parent, false);
        }
        if (position == 4) {
            view = lInflater.inflate(R.layout.settings_item_2, parent, false);
        }
        if (position == 6 || (position == 2 && !hasDescription)) {
            view = lInflater.inflate(R.layout.settings_item_2, parent, false);
            if (messages != null && messages.size() != 0) {
                view.findViewById(R.id.scrollView).setVisibility(View.VISIBLE);
                TGFunc.showLastImages(view, messages, sharedListener);
                view.setOnClickListener(sharedListener);
            }
            texts[position] = String.valueOf(messages == null ? 0 : totalCount);// messages.size());
        }

        if (view == null)
            Log.d("nullposv", "" + position);

        if (view.findViewById(R.id.title) == null)
            Log.d("nullpos", "" + position);
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(titles[position]);
        if (position < 3 || position == 6) {
            TextView text = (TextView) view.findViewById(R.id.text);
            text.setText(texts[position]);
        }
        ImageView imageView = (ImageView) view.findViewById(R.id.image);
        imageView.setImageResource(icons[position]);
        return view;
    }

}