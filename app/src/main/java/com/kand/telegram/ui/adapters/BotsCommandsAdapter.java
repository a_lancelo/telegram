package com.kand.telegram.ui.adapters;

import java.io.File;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kand.telegram.R;
import com.kand.telegram.logic.BotCommand;
import com.kand.telegram.logic.CircleTransform;
import com.squareup.picasso.Picasso;


public class BotsCommandsAdapter extends BaseAdapter {
    Context context;
    LayoutInflater lInflater;
    BotCommand[] commands;

    public BotsCommandsAdapter(Context context, BotCommand[] commands) {
        this.commands = commands;
        this.context = context;
        lInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return commands.length;
    }

    @Override
    public Object getItem(int position) {
        return null;//chats.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = lInflater.inflate(R.layout.bot_command, parent, false);
        BotCommand command = commands[position];
        ((TextView) view.findViewById(R.id.command)).setText(command.command);
        ((TextView)view.findViewById(R.id.desc)).setText(command.description);
        Picasso.with(context).load(new File(command.path))
                .transform(new CircleTransform())
                .into((ImageView)view.findViewById(R.id.image));
        return view;
    }

}