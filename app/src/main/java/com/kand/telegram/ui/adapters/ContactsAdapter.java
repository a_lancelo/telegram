package com.kand.telegram.ui.adapters;

import java.io.File;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kand.telegram.R;
import com.kand.telegram.logic.CircleTransform;
import com.kand.telegram.logic.TGFunc;
import com.squareup.picasso.Picasso;

import org.drinkless.td.libcore.telegram.TdApi;


public class ContactsAdapter extends BaseAdapter {
    Context context;
    LayoutInflater lInflater;
    TdApi.User[] users;

    public ContactsAdapter(Context context, TdApi.User[] users) {
        this.users = users;
        this.context = context;
        lInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return users.length;
    }

    @Override
    public Object getItem(int position) {
        return users[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TdApi.User user = users[position];
        View view = lInflater.inflate(R.layout.contact_item, parent, false);
        ((TextView) view.findViewById(R.id.title)).setText(user.firstName + " " + user.lastName);
        ((TextView) view.findViewById(R.id.text)).setText(user.type instanceof TdApi.UserTypeBot ?
                context.getString(R.string.has_no_access_to_messages) : TGFunc.getUserStatus(user.status));
        if (user.status instanceof TdApi.UserStatusOnline) {
            ((TextView) view.findViewById(R.id.text)).setTextColor(Color.parseColor("#5b95c2"));
        }

        ImageView imageView = ((ImageView) view.findViewById(R.id.avatar));
        Drawable drawable = new BitmapDrawable(context.getResources(),
                CircleTransform.makeCircleBitmap(TGFunc.getUserPlaceholder(context, user, 50)));

        Picasso.with(context).load(new File(user.profilePhoto.small.path))
                .placeholder(drawable)
                .transform(new CircleTransform())
                .into(imageView);
        return view;
    }

}