package com.kand.telegram.ui.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import com.kand.telegram.logic.TGFunc;

import org.drinkless.td.libcore.telegram.TdApi;


public class MessagesAdapter extends BaseAdapter {
    Context context;
    LayoutInflater lInflater;
    ArrayList<TdApi.Chat> chats;

    public MessagesAdapter(Context context, ArrayList<TdApi.Chat> chats) {
        this.chats = chats;
        this.context = context;
        lInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return chats.size();
    }

    @Override
    public Object getItem(int position) {
        return chats.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout view = new LinearLayout(context);
        TdApi.Chat chat = chats.get(position);
        view.addView(TGFunc.getChatView(context, chat));
        return view;
    }

}