package com.kand.telegram.ui;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.kand.telegram.R;
import com.kand.telegram.logic.CircleProgressBar;
import com.kand.telegram.logic.MusicPlayer;
import com.kand.telegram.logic.SwipeView;
import com.kand.telegram.logic.TGFunc;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;

public class MusicPlaylistFragment extends Fragment {

    ArrayList<TdApi.Message> messages = new ArrayList<>();
    ArrayList<Float> progresses = new ArrayList<>();
    ListView listView;

    private final long mFrequency = 100;
    private final int TICK_WHAT = 2;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message m) {
            TGFunc.updatePlayer(getActivity(), getView());
            sendMessageDelayed(Message.obtain(this, TICK_WHAT), mFrequency);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.messages, container, false);
        TGFunc.setToolbar(getActivity(), rootView, getString(R.string.playlist), true);
        listView = (ListView) rootView.findViewById(R.id.listView);
        listView.setDividerHeight(0);
        messages = MusicPlayer.getInstance().getPlaylist();//playlist;
        for (TdApi.Message message: messages) progresses.add(0f);
        listView.setAdapter(new MusicPlaylistAdapter(getActivity(), messages, progresses));
        ((MainActivity)getActivity()).getService().musicPlaylistFragment = this;
        mHandler.sendMessageDelayed(Message.obtain(mHandler, TICK_WHAT), mFrequency);
        setHasOptionsMenu(true);
        return new SwipeView(rootView);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    public void updateHandler(TdApi.TLObject object) {
        switch (object.getConstructor()) {
            case TdApi.UpdateFile.CONSTRUCTOR:
                updateFile(((TdApi.UpdateFile) object));
                break;
            case TdApi.UpdateFileProgress.CONSTRUCTOR:
                updateFileProgress((TdApi.UpdateFileProgress) object);
                break;
        }
    }

    public void updateFileProgress(TdApi.UpdateFileProgress file) {
        int index = TGFunc.getMessagePosByFileId(getActivity(), messages, file);
        if (index == -1) return;
        if (file.size != 0) {
            float progress = file.ready * 100 / file.size;
            progresses.set(index, progress);
            final LinearLayout view = (LinearLayout) listView.getChildAt(index - listView.getFirstVisiblePosition());
            if (view == null) return;
            CircleProgressBar progressBar = (CircleProgressBar) view.findViewById(R.id.progressBar);
            if (progressBar == null) return;
            progressBar.setProgress(progress);
        }
    }

    private void updateFile(TdApi.UpdateFile file) {
        int index = TGFunc.getMessagePosAndUpdateByFile(messages, file);
        Log.d("file2update fileAudio", file.toString()+" "+index);
        if (index == -1) return;
        final LinearLayout view = (LinearLayout) listView.getChildAt(index - listView.getFirstVisiblePosition());
        view.removeAllViews();
        view.addView(TGFunc.getMessageAudioView(messages.get(index), 0f));
    }

    public class MusicPlaylistAdapter extends BaseAdapter {

        Context context;
        LayoutInflater lInflater;
        ArrayList<TdApi.Message> messages;
        ArrayList<Float> progresses = new ArrayList<Float>();
        ArrayList<TdApi.User> users;

        public MusicPlaylistAdapter(Context context, ArrayList<TdApi.Message> messages, ArrayList<Float> progresses) {
            this.context = context;
            this.users = users;
            this.messages = messages;
            this.progresses = progresses;
            //this.listener = listener;
            lInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return messages.size();
        }

        @Override
        public Object getItem(int position) {
            return messages.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final LinearLayout view = new LinearLayout(context);
            final AbsListView.LayoutParams layoutParams = new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.MATCH_PARENT);
            view.setLayoutParams(layoutParams);

            TdApi.Message message = messages.get(position);
            int fromId = message.fromId;
            Log.d("3date", ""+message.date);
            view.addView(TGFunc.getMessageAudioView(message, 0f));
            return view;
        }
    }

}
