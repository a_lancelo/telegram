package com.kand.telegram.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.kand.telegram.R;
import com.kand.telegram.logic.Util;
import com.kand.telegram.logic.TGFunc;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

public class AuthFragment extends Fragment {
    TextView country;
    EditText code, number;
    private boolean isLoading = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.auth, container, false);
        ((Toolbar)rootView.findViewById(R.id.toolbar)).setContentInsetsAbsolute(Util.dp(16), Util.dp(16));

        rootView.findViewById(R.id.country).setOnClickListener(openCodes);
        try {
            ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        } catch (NullPointerException e) {}
        TGFunc.setToolbar(getActivity(), rootView, getString(R.string.phone_number), false);
        InputMethodManager imgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imgr.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

        country = (TextView) rootView.findViewById(R.id.country);
        code = (EditText) rootView.findViewById(R.id.code);
        number = (EditText) rootView.findViewById(R.id.tel);
        code.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                String state = TGFunc.getCountry(getActivity(), code.getText().toString());

                if (state == null) {
                    country.setText(code.getText().length() == 1 ? R.string.choose_country : R.string.wrong_country_code);
                } else {
                    country.setText(state);
                }
                if (s.length() == 0) {
                    s.append("+");
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

        number.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    sendCode();
                    return true;
                }
                return false;
            }
        });
        setHasOptionsMenu(true);
        return rootView;
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.auth_menu, menu);
        menu.getItem(isLoading ? 1 : 0).setVisible(true);
        menu.getItem(isLoading ? 0 : 1).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_check:
                Log.d("onOptionsItemSelected", "" + getFragmentManager().getBackStackEntryCount());
                if (getFragmentManager().getBackStackEntryCount() == 1) {
                    sendCode();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    View.OnClickListener openCodes = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            hideSoftKeyboard(getActivity());
            getFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, 0, 0, R.anim.exit_to_right).add(R.id.container, new CodeFragment(), "code").addToBackStack(null).commit();
        }
    };

    public void updateView() {
        country.setText(TGFunc.getCountry());
        code.setText("+"+TGFunc.getPhoneNumber());
    }

    private void sendCode() {
        if (country.getText().toString().equals(getString(R.string.choose_country)) || country.getText().toString().equals(getString(R.string.wrong_country_code))) {
            new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.app_name)
                    .setPositiveButton(R.string.ok, null)
                    .setMessage(R.string.wrong_country_code)
                    .show();
        } else {
            isLoading = true;
            getActivity().invalidateOptionsMenu();
            final Client client = TG.getClientInstance();
            final String str = code.getText().toString() + number.getText().toString();
            if (client == null) return;
            client.send(new TdApi.SetAuthPhoneNumber(str), new Client.ResultHandler() {
                @Override
                public void onResult(final TdApi.TLObject object) {
                    //dialog.cancel();
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("Login AuthFr", object.toString());
                            if (object instanceof TdApi.AuthStateWaitCode || object instanceof TdApi.AuthStateWaitName) {
                                TGFunc.setPhoneNumber(getActivity(), str);
                                Fragment fragment = object instanceof TdApi.AuthStateWaitCode ? new CheckAuthFragment() : new SignUpFragment();
                                final FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                fragmentManager.beginTransaction().setCustomAnimations(R.anim.enter_from_right, 0, 0, R.anim.exit_to_right).add(R.id.container, fragment).addToBackStack(null).commit();
                            } else {
                                new AlertDialog.Builder(getActivity())
                                        .setTitle(R.string.app_name)
                                        .setPositiveButton(R.string.ok, null)
                                        .setMessage(R.string.invalid_phone)
                                        .show();
                            }
                            isLoading = false;
                            getActivity().invalidateOptionsMenu();
                        }});
                }
            });
        }
    }


}
