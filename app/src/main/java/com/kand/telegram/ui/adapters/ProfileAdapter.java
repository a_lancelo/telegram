package com.kand.telegram.ui.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.provider.MediaStore;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kand.telegram.R;
import com.kand.telegram.logic.TGFunc;
import com.kand.telegram.logic.Util;
import com.squareup.picasso.Picasso;

import org.drinkless.td.libcore.telegram.TdApi;

import java.io.File;
import java.util.ArrayList;

public class ProfileAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater lInflater;
    private TdApi.User user;
    private int [] icons = {0, R.drawable.ic_account, R.drawable.ic_phone, 0, R.drawable.ic_attach_gallery};
    private String [] titles = new String[5];//{R.string.first_name, R.string.phone_number, 0, R.string.shared_media};
    private String [] texts = new String[5];
    //private int [] texts = {R.string.username, R.string.phone, 0, R.string.disabled};
    private ArrayList<TdApi.Message> messages;
    private int shift = 0, totalCount;
    private View.OnClickListener sharedListener;

    public ProfileAdapter(Context context, TdApi.User user, ArrayList<TdApi.Message> messages, int totalCount, View.OnClickListener sharedListener) {
        this.context = context;
        lInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.user = user;
        this.messages = messages;
        this.totalCount = totalCount;
        titles[1] = "@"+user.username;
        titles[2] = "+"+user.phoneNumber;
        titles[4] = context.getString(R.string.shared_media);

        if (user.username.length() == 0) shift++;
        if (user.phoneNumber.length() == 0) shift++;
        if (user.username.length() == 0 && user.phoneNumber.length() == 0) shift++;

        texts[1] = context.getString(R.string.username);
        texts[2] = context.getString(R.string.phone);
        //texts[3] = String.valueOf(messages == null ? 0 : messages.size());
        this.sharedListener = sharedListener;
    }

    @Override
    public int getCount() {
        return titles.length - shift;
    }

    @Override
    public Object getItem(int position) {
        return titles[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (position == 0) {
            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, Util.dp(120)));
            return linearLayout;
        }
        if (shift == 2 || shift == 3) {
            position += shift;
        } else {
            if (user.username.length() == 0) position++;
            if (user.phoneNumber.length() == 0  && (user.username.length() == 0 || position != 1))  position++;
        }
        if (position == 3) {
            return lInflater.inflate(R.layout.settings_item_3, parent, false);
        }
        View view = null;
        if (position == 1 || position == 2) {
            view = lInflater.inflate(R.layout.settings_item_1, parent, false);
        }
        if (position == 4) {
            view = lInflater.inflate(R.layout.settings_item_2, parent, false);
            if (messages != null && messages.size() != 0) {
                view.findViewById(R.id.scrollView).setVisibility(View.VISIBLE);
                TGFunc.showLastImages(view, messages, sharedListener);
                view.setOnClickListener(sharedListener);
            }
            texts[4] = String.valueOf(messages == null ? 0 : totalCount);// messages.size());
        }
        if (view == null)
            Log.d("nullposv", "" + position);
        if (view.findViewById(R.id.title) == null)
            Log.d("nullpos", ""+position);
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(titles[position]);
        TextView text = (TextView) view.findViewById(R.id.text);
        text.setText(texts[position]);
        ImageView imageView = (ImageView) view.findViewById(R.id.image);
        imageView.setImageResource(icons[position]);
        return view;
    }

}