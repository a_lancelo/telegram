package com.kand.telegram.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.kand.telegram.R;
import com.kand.telegram.logic.BigToolbar;
import com.kand.telegram.logic.CircleTransform;
import com.kand.telegram.logic.SwipeView;
import com.kand.telegram.logic.TGFunc;
import com.kand.telegram.logic.Util;
import com.kand.telegram.ui.adapters.GroupAdapter;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;
import java.util.Arrays;

public class GroupFragment extends Fragment {

    TdApi.GroupChatFull groupChat;
    public TdApi.NotificationSettings notificationSettings;
    private ArrayList<TdApi.Message> messagesMedia = new ArrayList<>();
    private ArrayList<TdApi.Message> messagesAudio = new ArrayList<>();
    private ListView listView;

    AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Log.d("gr pos", "" + position);

            if (position == 1) {
                getFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, 0, 0, R.anim.exit_to_right)
                        .add(R.id.container, new SelectContactFragment(), "selectContact").addToBackStack(null).commit();
            }

            if (position >= 5) {
                ProfileFragment fragment = new ProfileFragment();
                fragment.user = groupChat.participants[position-5].user;
                fragment.botInfo = groupChat.participants[position-5].botInfo;
                getFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, 0, 0, R.anim.exit_to_right)
                        .add(R.id.container, fragment, "profile").addToBackStack(null).commit();
            }
        }
    };

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (messagesMedia.size() == 0 && messagesAudio.size() == 0) return;
            SharedMediaFragment fragment = new SharedMediaFragment();
            fragment.putMessages(groupChat.groupChat.id, messagesMedia, messagesAudio);
            getFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, 0, 0, R.anim.exit_to_right)
                    .add(R.id.container, fragment, "shared_media").addToBackStack(null).commit();
        }
    };

    //@Override
    public View onCreateView1(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.profile, container, false);

        listView = (ListView) rootView.findViewById(R.id.listView);
        listView.setAdapter(new GroupAdapter(getActivity(), groupChat, null, 0, listener));
        listView.setDividerHeight(0);
        listView.setOnItemClickListener(itemClickListener);

        setHasOptionsMenu(true);
        return new SwipeView(rootView);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.profile, container, false);

        TdApi.Chat chat = new TdApi.Chat();
        chat.id = groupChat.groupChat.id;
        TdApi.GroupChatInfo info = new TdApi.GroupChatInfo(groupChat.groupChat);
        chat.type = info;
        Drawable drawable = new BitmapDrawable(getActivity().getResources(),
               CircleTransform.makeCircleBitmap(TGFunc.getChatPlaceholder(getActivity(), chat, Util.dp(65))));
        ArrayList<TdApi.User> users = new ArrayList<>();
        for (int i = 0; i < groupChat.participants.length; i++) {
            users.add(groupChat.participants[i].user);
        }
        new BigToolbar(getActivity()).show(rootView, groupChat.groupChat.title, TGFunc.getChatStatus(users), groupChat.groupChat.photo, drawable, R.drawable.ic_camera);

        listView = (ListView) rootView.findViewById(R.id.listView);
        listView.setAdapter(new GroupAdapter(getActivity(), groupChat, null, 0, listener));
        listView.setDividerHeight(0);
        listView.setOnItemClickListener(itemClickListener);

        Client client = TG.getClientInstance();
        if (client != null) {
            client.send(new TdApi.SearchMessages(groupChat.groupChat.id, "", 0, 100, new TdApi.SearchMessagesFilterPhoto()), new Client.ResultHandler() {
                @Override
                public void onResult(final TdApi.TLObject object) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("SearchMessagesGroup", object.toString());
                            if (object instanceof TdApi.Messages) {
                                messagesMedia = new ArrayList<>(Arrays.asList(((TdApi.Messages) object).messages));
                                listView.setAdapter(new GroupAdapter(getActivity(), groupChat, messagesMedia, ((TdApi.Messages) object).totalCount, listener));
                                //((GroupAdapter)listView.getAdapter()).messages = messagesMedia;
                                //listView.invalidateViews();
                                TGFunc.downloadPreviewPhoto(messagesMedia);
                            }
                        }
                    });
                }
            });
            client.send(new TdApi.SearchMessages(groupChat.groupChat.id, "", 0, 100, new TdApi.SearchMessagesFilterAudio()), new Client.ResultHandler() {
                @Override
                public void onResult(final TdApi.TLObject object) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("SearchMessagesGroupAu", object.toString());
                            if (object instanceof TdApi.Messages) {
                                messagesAudio = new ArrayList<>(Arrays.asList(((TdApi.Messages) object).messages));
                            }
                        }
                    });
                }
            });
        }

        setHasOptionsMenu(true);
        return Util.isTablet ? rootView : new SwipeView(rootView);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.group, menu);
        if (notificationSettings != null) {
            menu.getItem(notificationSettings.muteFor == 0 ? 0 : 1).setVisible(true);
        }
        listView.invalidateViews();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.notifications_on:
            case R.id.notifications_off:
                TGFunc.showNotificationPopupMenu(getView().findViewById(item.getItemId()), groupChat.groupChat.id, notificationSettings);
                break;
            case R.id.add_member:
                getFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, 0, 0, R.anim.exit_to_right)
                        .add(R.id.container, new SelectContactFragment(), "selectContact").addToBackStack(null).commit();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 112 && resultCode == Activity.RESULT_OK) {
            getFragmentManager().popBackStack();
            getFragmentManager().findFragmentByTag("chat").onActivityResult(requestCode, resultCode, data);
        }
    }

    public void updateFile(TdApi.UpdateFile file) {
        if (messagesMedia.size() == 0) return;
        if (TGFunc.updateMessagesByFile(messagesMedia, file)) listView.invalidateViews();
    }

    public void updateDeleteMessages(TdApi.UpdateDeleteMessages object) {
        if (messagesMedia.size() == 0) return;
        if (messagesMedia.get(0).chatId != object.chatId) return;
        for (int id : object.messages) {
            for (int i = messagesMedia.size()-1; i >= 0; i--) {
                if (messagesMedia.get(i).id == id) {
                    messagesMedia.remove(i);
                }
            }
        }
        listView.invalidateViews();
    }

}
