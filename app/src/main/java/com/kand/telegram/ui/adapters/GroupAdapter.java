package com.kand.telegram.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kand.telegram.R;
import com.kand.telegram.logic.CircleTransform;
import com.kand.telegram.logic.TGFunc;
import com.kand.telegram.logic.Util;
import com.squareup.picasso.Picasso;

import org.drinkless.td.libcore.telegram.TdApi;

import java.io.File;
import java.util.ArrayList;

public class GroupAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater lInflater;
    private TdApi.GroupChatFull groupChat;
    private int [] icons = {0, R.drawable.ic_add, 0, R.drawable.ic_attach_gallery, 0};
    private int [] titles = {0, R.string.add_member, 0, R.string.shared_media, 0};
    private int totalCount;
    private View.OnClickListener sharedListener;
    public ArrayList<TdApi.Message> messages;

    public GroupAdapter(Context context, TdApi.GroupChatFull groupChat, ArrayList<TdApi.Message> messages, int totalCount, View.OnClickListener sharedListener) {
        this.context = context;
        lInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.groupChat = groupChat;
        this.messages = messages;
        this.totalCount = totalCount;
        this.sharedListener = sharedListener;
    }

    @Override
    public int getCount() {
        return titles.length + groupChat.participants.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (position == 0) {
            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, Util.dp(120)));
            return linearLayout;
        }
        View view = null;
        if (position < 5) {
            if (position == 2 || position == 4) {
                return lInflater.inflate(R.layout.settings_item_3, parent, false);
            }
            if (position == 1) {
                view = lInflater.inflate(R.layout.settings_item_2, parent, false);
            }
            if (position == 3) {
                view = lInflater.inflate(R.layout.settings_item_2, parent, false);
                if (messages != null && messages.size() != 0) {
                    view.findViewById(R.id.scrollView).setVisibility(View.VISIBLE);
                    TGFunc.showLastImages(view, messages, sharedListener);
                    view.setOnClickListener(sharedListener);
                }
                ((TextView) view.findViewById(R.id.text)).setText(String.valueOf(messages == null ? 0 : totalCount));//messages.size()));
            }
            ((TextView) view.findViewById(R.id.title)).setText(titles[position]);
            ((ImageView) view.findViewById(R.id.image)).setImageResource(icons[position]);
        } else {
            view = lInflater.inflate(R.layout.settings_item_5, parent, false);

            TdApi.User user = groupChat.participants[position-5].user;
            ((TextView) view.findViewById(R.id.title)).setText(user.firstName+" "+user.lastName);// titles[position]);
            ((TextView) view.findViewById(R.id.text)).setText(user.id == TGFunc.getUserId(context) ? context.getString(R.string.online) : (user.type instanceof TdApi.UserTypeBot ?
                    context.getString(R.string.has_no_access_to_messages) : TGFunc.getUserStatus(user.status)));//"Online");//texts[position]);
            if (user.status instanceof TdApi.UserStatusOnline || user.id == TGFunc.getUserId(context)) {
                ((TextView) view.findViewById(R.id.text)).setTextColor(Color.parseColor("#5b95c2"));
            }
            if (position == 5) {
                ((ImageView) view.findViewById(R.id.image)).setImageResource(R.drawable.ic_groupusers);
            }
            Drawable drawable = new BitmapDrawable(context.getResources(),
                    CircleTransform.makeCircleBitmap(TGFunc.getUserPlaceholder(context, user, 65)));
            Picasso.with(context).load(new File(TGFunc.getFileLocalPath(user.profilePhoto.small)))
                    .placeholder(drawable).transform(new CircleTransform()).into(((ImageView) view.findViewById(R.id.avatar)));
        }

        return view;
    }

}