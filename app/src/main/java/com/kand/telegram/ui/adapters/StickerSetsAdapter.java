package com.kand.telegram.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;

import com.kand.telegram.logic.TGFunc;
import com.kand.telegram.logic.Util;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;

public class StickerSetsAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<TdApi.StickerSet> stickerSets;
    private AdapterView.OnItemClickListener listener;
    private int width;

    public StickerSetsAdapter(Context context, ArrayList<TdApi.StickerSet> stickerSets, AdapterView.OnItemClickListener listener) {
        this.context = context;
        this.stickerSets = stickerSets;
        this.listener = listener;
        //Point size = new Point();
        //((Activity)context).getWindowManager().getDefaultDisplay().getSize(size);
        width = TGFunc.getChatWidth();// size.x;
    }

    @Override
    public int getCount() {
        return stickerSets.size();
    }

    @Override
    public Object getItem(int position) {
        return stickerSets.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TdApi.StickerSet stickerSet = stickerSets.get(position);
        Log.d("stickerSets" + position, "" + stickerSet.title);
        Log.d("stickerSet" + position, "" + stickerSet.toString());
        GridView gridView = new GridView(context);
        if (stickerSet.stickers != null) {
            gridView.setNumColumns(width / Util.dp(80));
            gridView.setAdapter(new StickersAdapter(context, stickerSet.stickers));
            gridView.setOnItemClickListener(listener);
            gridView.setTag(position);
            int height = (stickerSet.stickers.length / (width / Util.dp(80)) +
                    (stickerSet.stickers.length % (width / Util.dp(80)) != 0 ? 1 : 0)) * Util.dp(80);
            gridView.setLayoutParams(new AbsListView.LayoutParams(-1, height));
            gridView.setBackgroundColor(Color.parseColor("#f9f9f9"));
        }
        return gridView;
    }

}