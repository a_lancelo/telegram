package com.kand.telegram.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kand.telegram.R;


public class SpinnerAdapter extends BaseAdapter {

    private String[] items;
    private Context context;
    private boolean blackText;

    public SpinnerAdapter(Context context, String[] items) {
        this.context = context;
        this.items = items;
    }

    public SpinnerAdapter(Context context, String[] items, boolean blackText) {
        this.context = context;
        this.items = items;
        this.blackText = blackText;
    }
    
    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public Object getItem(int position) {
        return items[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getDropDownView(int position, View view, ViewGroup parent) {
        if (view == null || !view.getTag().toString().equals("DROPDOWN")) {
            view = ((Activity)context).getLayoutInflater().inflate(R.layout.toolbar_spinner_item_dropdown, parent, false);
            view.setTag("DROPDOWN");
        }
        TextView textView = (TextView) view.findViewById(android.R.id.text1);
        textView.setText(items[position]);
        return view;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null || !view.getTag().toString().equals("NON_DROPDOWN")) {
            view = ((Activity)context).getLayoutInflater().inflate(R.layout.
                    toolbar_spinner_item_actionbar, parent, false);
            view.setTag("NON_DROPDOWN");
        }
        TextView textView = (TextView) view.findViewById(R.id.text);
        textView.setText(items[position]);
        if (blackText) {
            textView.setTextColor(Color.BLACK);
            ((ImageView) view.findViewById(R.id.image)).setColorFilter(0xff000000, PorterDuff.Mode.MULTIPLY);;
        }
        return view;
    }
}
