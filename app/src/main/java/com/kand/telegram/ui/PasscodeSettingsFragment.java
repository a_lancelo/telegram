package com.kand.telegram.ui;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ListView;

import com.kand.telegram.R;
import com.kand.telegram.logic.SwipeView;
import com.kand.telegram.logic.Util;
import com.kand.telegram.logic.TGFunc;
import com.kand.telegram.ui.adapters.PasscodeSettingsAdapter;

public class PasscodeSettingsFragment extends Fragment {

    private ListView listView;

    CompoundButton.OnCheckedChangeListener switchListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                Bundle bundle = new Bundle();
                bundle.putBoolean("setNew", true);
                Fragment fragment = new PasscodeFragment();
                fragment.setArguments(bundle);
                getActivity().getSupportFragmentManager ().beginTransaction()
                        .add(R.id.container, fragment).addToBackStack(null).commit();
            } else {
                Util.getPreferences().edit().putBoolean("passcodeEnabled", false).commit();
                listView.invalidateViews();
            }
        }
    };

    AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (position == 0 || position == 1) {
                if (Util.getPreferences().getBoolean("passcodeEnabled", false)) {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("setNew", true);
                    Fragment fragment = new PasscodeFragment();
                    fragment.setArguments(bundle);
                    getActivity().getSupportFragmentManager ().beginTransaction()
                            .add(R.id.container, fragment).addToBackStack(null).commit();
                }
            }
            if (position == 3) {
                showAutoLockDialog();
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.passcode_settings, container, false);
        TGFunc.setToolbar(getActivity(), rootView, getString(R.string.passcode_lock), true);
        listView = (ListView) rootView.findViewById(R.id.listView);
        listView.setOnItemClickListener(itemClickListener);
        listView.setAdapter(new PasscodeSettingsAdapter(getActivity(), switchListener));
        setHasOptionsMenu(true);
        return new SwipeView(rootView);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Log.d("pass sett", ""+getFragmentManager().getBackStackEntryCount());
                if (getFragmentManager().getBackStackEntryCount() == 2) {
                    getFragmentManager().findFragmentByTag("settings").
                            onActivityResult(109, Activity.RESULT_OK, null);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 110 && resultCode == Activity.RESULT_OK) {
            listView.invalidateViews();
        }
    }

    private void showAutoLockDialog() {
        final String[] array = getResources().getStringArray(R.array.types_autolock);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.auto_lock_info)
                .setNeutralButton(R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                dialog.cancel();
                            }
                        })
                .setSingleChoiceItems(array, Util.getPreferences().getInt("type_auto_lock", 2),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int item) {

                                Util.getPreferences().edit().putInt("type_auto_lock", item).commit();
                                listView.invalidateViews();
                                dialog.cancel();
                            }
                        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

}
