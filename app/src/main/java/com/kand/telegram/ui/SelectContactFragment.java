package com.kand.telegram.ui;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.kand.telegram.R;
import com.kand.telegram.logic.SwipeView;
import com.kand.telegram.logic.TGFunc;
import com.kand.telegram.ui.adapters.ContactsAdapter;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class SelectContactFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.messages, container, false);
        TGFunc.setToolbar(getActivity(), rootView, getString(R.string.select_contact), true);

        Client client = TG.getClientInstance();
        if (client != null) {
            client.send(new TdApi.GetContacts(), new Client.ResultHandler() {
                @Override
                public void onResult(final TdApi.TLObject object) {
                    if (object instanceof TdApi.Contacts) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                ListView listView = (ListView) rootView.findViewById(R.id.listView);

                                ArrayList<TdApi.User> _users = new ArrayList<>(Arrays.asList(((TdApi.Contacts) object).users));
                                Collections.sort(_users, new Comparator<TdApi.User>() {
                                    @Override
                                    public int compare(TdApi.User a, TdApi.User b) {
                                        return a.firstName.compareTo(b.firstName);
                                    }
                                });
                                final TdApi.User[] users = _users.toArray(new TdApi.User[_users.size()]);

                                listView.setAdapter(new ContactsAdapter(getActivity(), users));
                                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                                        final EditText input = new EditText(getActivity());
                                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                                                LinearLayout.LayoutParams.MATCH_PARENT,
                                                LinearLayout.LayoutParams.MATCH_PARENT);
                                        input.setLayoutParams(lp);
                                        input.setInputType(InputType.TYPE_CLASS_NUMBER);
                                        input.setGravity(Gravity.CENTER_HORIZONTAL);
                                        input.setText("50");
                                        input.setSelection(2);

                                        final TdApi.User user = users[position];

                                        String text = String.format(getString(R.string.select_contact_desc), user.firstName + " " + user.lastName);

                                        new AlertDialog.Builder(getActivity())
                                                .setTitle(R.string.app_name)
                                                .setMessage(text)
                                                .setView(input)
                                                .setNegativeButton(R.string.cancel, null)
                                                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        getFragmentManager().popBackStack();
                                                        Intent data = new Intent();
                                                        data.putExtra("selectedContactId", user.id);
                                                        data.putExtra("forwardLimit", Integer.parseInt(input.getText().toString()));
                                                        Fragment fragment = getFragmentManager().findFragmentByTag("group");
                                                        fragment.onActivityResult(112, Activity.RESULT_OK, data);
                                                    }
                                                }).show();
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }
        setHasOptionsMenu(true);
        return new SwipeView(rootView);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

}
