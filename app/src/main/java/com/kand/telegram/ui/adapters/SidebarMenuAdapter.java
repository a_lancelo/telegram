package com.kand.telegram.ui.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kand.telegram.R;

public class SidebarMenuAdapter extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    int [] icons = {R.drawable.menu_settings, R.drawable.ic_logout};
    int [] titles = {R.string.settings, R.string.log_out};

    public SidebarMenuAdapter(Context context) {
        ctx = context;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return titles.length;
    }

    @Override
    public Object getItem(int position) {
        return titles[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = lInflater.inflate(R.layout.menu_item, parent, false);
        TextView textView = (TextView) view.findViewById(R.id.rowText);
        textView.setText(titles[position]);

        Typeface font = Typeface.createFromAsset(ctx.getAssets(), "fonts/rmedium.ttf");
        textView.setTypeface(font);

        ImageView imageView = (ImageView) view.findViewById(R.id.rowIcon);
        imageView.setImageResource(icons[position]);
        imageView.setPadding(px(14), px(8), px(8), px(8));
        return view;
    }

    private int px(int dp) {
        Resources r = ctx.getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
        return (int)px;
    }



}