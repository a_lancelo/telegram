package com.kand.telegram.ui;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.kand.telegram.logic.BotCommand;
import com.kand.telegram.logic.MediaController;
import com.kand.telegram.logic.MediaUtil;
import com.kand.telegram.logic.SwipeView;
import com.kand.telegram.logic.Util;
import com.kand.telegram.logic.CircleProgressBar;
import com.kand.telegram.logic.CircleTransform;
import com.kand.telegram.logic.Emoji;
import com.kand.telegram.logic.TGService;
import com.kand.telegram.ui.adapters.BotsCommandsAdapter;
import com.kand.telegram.ui.adapters.ChatAdapter;
import com.kand.telegram.ui.adapters.EmojiAdapter;
import com.kand.telegram.ui.adapters.EmojiPagerAdapter;
import com.kand.telegram.R;
import com.kand.telegram.logic.TGFunc;
import com.kand.telegram.ui.adapters.StickerSetsAdapter;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ChatFragment extends Fragment {

    public TdApi.Chat chat = new TdApi.Chat();
    private TdApi.BotInfo botInfo;
    ArrayList<TdApi.User> users = new ArrayList<>();
    ArrayList<Integer> typings = new ArrayList<>();
    ArrayList<Long> typings_expired = new ArrayList<>();
    ArrayList<TdApi.Message> messages = new ArrayList<>();
    ArrayList<Float> progresses = new ArrayList<>();
    ArrayList<TdApi.StickerSet> stickerSets = new ArrayList<>();
    ArrayList<View> views = new ArrayList<>();
    Toolbar toolbar;
    LinearLayout BCLayout;
    ListView listView, BCListView, stickerMenu;
    View emptyView;
    ImageView slash, command, kb_command, floatingButton;
    EditText message;
    Client client;
    long chatId;
    int userId;
    int offset = 0, limit = 20;
    boolean isDownloadedAllChats = false, isLoading = false, isPreload = true, isFirstLoad = true, wasScroll = false,
    hasBot = false, hasCommandKb;
    public boolean isFirstTime;
    long curTypingExpired;

    boolean isEmojiShowing = false, wasEmojiPressed = false, isBotKbPrepare = false;

    int menuIcons[] = {R.drawable.ic_emoji_recent,
            R.drawable.ic_emoji_smile,
            R.drawable.ic_emoji_flower,
            R.drawable.ic_emoji_bell,
            R.drawable.ic_emoji_car,
            R.drawable.ic_emoji_symbol,
            R.drawable.ic_smiles_sticker,
            R.drawable.ic_emoji_backspace};

    DialogInterface.OnClickListener okClearHistoryDialog = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            client.send(new TdApi.DeleteChatHistory(chatId), deleteChatHistory);
        }
    };

    Client.ResultHandler sendMessage = new Client.ResultHandler() {
        @Override
        public void onResult(final TdApi.TLObject object) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Log.d("sendMsg", object.toString());

                    if (object instanceof TdApi.Message) {
                        TdApi.Message message = (TdApi.Message) object;
                        if (messages.size() == 0 || (messages.size() > 0 && !TGFunc.getFormedDate(messages.get(messages.size()-1).date * 1000L).equals(TGFunc.getFormedDate(message.date * 1000L)))) {
                            messages.add( new TdApi.Message(0, 0, 0, message.date, 0, 0, 0, null, null));
                            progresses.add(0f);
                        }

                        messages.add(((TdApi.Message) object));
                        progresses.add(0f);
                        downloadPreviewFiles(messages);
                        offset++;

                        TGService service = ((MainActivity) getActivity()).getService();
                        if (service != null) {
                            service.updateNewMessage((TdApi.Message) object);
                            service.sendAnswer(TdApi.UpdateNewMessage.CONSTRUCTOR);
                        }

                        if (listView.getAdapter() == null) {
                            ChatAdapter chatsAdapter = new ChatAdapter(getActivity(), users, messages, progresses);
                            //listView.setEmptyView(getView().findViewById(android.R.id.empty));
                            listView.setAdapter(chatsAdapter);
                            return;
                        }
                        ((BaseAdapter)listView.getAdapter()).notifyDataSetChanged();
                        unreadCount = 0;
                        showUnreadPanel(unreadCount);
                        scrollToPosition(messages.size()-1);

                    }
                }
            });
        }
    };

    Client.ResultHandler downloadFile = new Client.ResultHandler() {
        @Override
        public void onResult(final TdApi.TLObject object) {
            Log.d("preDownload 2", "obj: " + object.toString());
        }
    };

    Client.ResultHandler deleteChatHistory = new Client.ResultHandler() {
        @Override
        public void onResult(final TdApi.TLObject object) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Log.d("del", object.toString());
                    messages.clear();
                    progresses.clear();
                    if (listView == null) return;
                    listView.invalidateViews();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    MessagesFragment fragment = (MessagesFragment) fragmentManager.findFragmentByTag("main");
                    if (fragment == null) return;
                    fragment.deleteTopMessage(chatId);
                }
            });

            }
    };

    Client.ResultHandler getUserFull = new Client.ResultHandler() {
        @Override
        public void onResult(final TdApi.TLObject object) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                 @Override
                 public void run() {
                     Log.d("getUserFull", object.toString());
                     if (object instanceof TdApi.UserFull) {
                         botInfo = ((TdApi.UserFull)object).botInfo;

                         if (isFirstTime) {
                             getView().findViewById(R.id.bot_first_start).setVisibility(View.VISIBLE);
                             if (botInfo instanceof TdApi.BotInfoGeneral) {
                                 ((TextView) getView().findViewById(R.id.bot_description)).setText(((TdApi.BotInfoGeneral) botInfo).description);
                             }
                             getView().findViewById(R.id.start).setOnClickListener(new View.OnClickListener() {
                                 @Override
                                 public void onClick(View v) {
                                     TdApi.InputMessageText message = new TdApi.InputMessageText("/start");
                                     sendMessage(message);
                                     Log.d("BotStart", "");
                                 }
                             });
                         }


                     }
                 }
             });
        }
    };

    TdApi.GroupChatFull groupChat;

    Client.ResultHandler getChatUsers = new Client.ResultHandler() {
        @Override
        public void onResult(final TdApi.TLObject object) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Log.d("msg user-1", object.toString());
                    if (object instanceof TdApi.GroupChatFull) {
                        groupChat = (TdApi.GroupChatFull)object;
                        users.clear();
                        for (int i = 0; i < ((TdApi.GroupChatFull) object).participants.length; i++) {
                            TdApi.User user = ((TdApi.GroupChatFull) object).participants[i].user;
                            users.add(user);
                            if (user.type instanceof TdApi.UserTypeBot) {
                                hasBot = true;
                            }
                        }
                    }
                    if (hasBot) slash.setVisibility(View.VISIBLE);
                    updateToolbar();
                    downloadChatPhotos();
                    getChatHistory();

                    Log.d("msg user0", object.toString());

                }
            });

        }
    };

    Client.ResultHandler getChatHistory = new Client.ResultHandler() {
        @Override
        public void onResult(final TdApi.TLObject object) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    if (getActivity() == null) return;

                    Log.d("scroll 1-2", "");
                    if (object instanceof TdApi.Error) {
                        Log.d("getChatHistory error = ", object.toString());
                        return;
                    }

                    Log.d("isPreload", "" + isPreload);
                    Log.d("ChatMessages1", object.toString());

                    TdApi.Message[] arrayMessages = ((TdApi.Messages) object).messages;
                    offset += arrayMessages.length;
                    isLoading = false;

                    if (arrayMessages.length == 0) {
                        isDownloadedAllChats = true;
                    } else {
                        if (messages.size() != 0) messages.remove(0);
                        messages.add(0, new TdApi.Message(0, 0, 0, arrayMessages[arrayMessages.length - 1].date, 0, 0, 0, null, null));
                        progresses.add(0, 0f);
                    }
                    for (int i = 0; i < arrayMessages.length; i++) {
                        Log.d("replyMarkupMessageId", ""+chat.replyMarkupMessageId + "  " + arrayMessages[i].id);
                        if (chat.replyMarkupMessageId == arrayMessages[i].id) {
                            changeReplyMarkup(arrayMessages[i]);
                        }

                        messages.add(1, arrayMessages[i]);
                        progresses.add(1, 0f);

                        if (messages.size() > 2) {
                            TdApi.Message message = messages.get(1);
                            TdApi.Message messageNext = messages.get(2);
                            if (messageNext.message != null && message.message != null) {
                                Log.d("Dates 1", "" + TGFunc.getFormedDate(messageNext.date * 1000L) + " " + TGFunc.getFormedDate(message.date * 1000L));
                                if (!TGFunc.getFormedDate(messageNext.date * 1000L).equals(TGFunc.getFormedDate(message.date * 1000L))) {
                                    Log.d("DateK0", "" + messages.size());
                                    progresses.add(2, 0f);
                                    messages.add(2, new TdApi.Message(0, 0, 0, messageNext.date, 0, 0, 0, null, null));
                                    //Log.d("4date", ""+messageNext.date);

                                    Log.d("DateK1", "" + messages.size());
                                    Log.d("DateK2", "" + messages.size());
                                }
                            }
                        }
                    }

                    if (isPreload) {
                        isPreload = false;
                        listView.setEmptyView(getView().findViewById(android.R.id.empty));

                          ChatAdapter chatsAdapter = new ChatAdapter(getActivity(), users, messages, progresses);
                          listView.setAdapter(chatsAdapter);


                        listView.setOnScrollListener(scrollListener);

                        listView.setStackFromBottom(true);
                        // listView.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
                        Log.d("listView.getCount()", "" + listView.getCount());

                        if (getArguments().getInt("addBot") != 0) {
                            client.send(new TdApi.AddChatParticipant(chatId, getArguments().getInt("addBot"), 0), new Client.ResultHandler() {
                                @Override
                                public void onResult(TdApi.TLObject object) {
                                    Log.d("AddChatParticipant", object.toString());
                                }
                            });
                        }

                        getChatHistory();
                        return;
                    }

                    downloadPreviewFiles(messages);

                    if (isFirstLoad) {
                        isFirstLoad = false;
                        //scrollToPosition(messages.size() - 1);
                        showUnreadPanel(unreadCount);
                        scrollToUnread();
                        unreadCount = 0;
                        return;
                    }

                    Log.d("listView.getCount() 2 =", "" + listView.getCount());

                    scrollToPosition(listView.getFirstVisiblePosition() + arrayMessages.length);

                }
            });

        }
    };

    private DialogInterface.OnClickListener okLeaveGroupDialog = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            leaveGroup();
        }
    };

    View.OnClickListener onSendMessage = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            TdApi.InputMessageText messageText =
                    new TdApi.InputMessageText(message.getText().toString());
            sendMessage(messageText);
            message.setText("");
        }
    };


    private String convert(long paramLong) {
        String str = "";
        for (int i = 0; ; i++) {
            if (i >= 4) {
                return str;
            }
            int j = (int)(0xFFFF & paramLong >> 16 * (3 - i));
            if (j != 0) {
                str = str + (char)j;
            }
        }
    }

    AdapterView.OnItemClickListener emojiClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            message.setText(message.getText()+""+convert(Emoji.data[kbPageNumber][position]));
            message.setText(Emoji.replaceEmoji(message.getText(), message.getPaint().getFontMetricsInt(), 20));
            if (kbPageNumber != 0) {
                addRecentEmoji(Emoji.data[kbPageNumber][position]);
            }
            message.setSelection(message.length());
        }
    };

    AdapterView.OnItemClickListener stickerClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            int pos = (int)((GridView)view.getParent()).getTag();
            Log.d("44pos", "" + pos);

            int count = TGFunc.getPreferences(getActivity()).getInt("countTimesStickerSet_" + stickerSets.get(pos).id, 0);
            count++;
            TGFunc.getPreferences(getActivity()).edit().putInt("countTimesStickerSet_" + stickerSets.get(pos).id, count).commit();

            TdApi.File sticker = stickerSets.get(pos).stickers[position].sticker;
            TdApi.InputFile inputFile = sticker.path.length() != 0 ? new TdApi.InputFileLocal(sticker.path) :
                    new TdApi.InputFilePersistentId(sticker.persistentId);
            addRecentSticker(sticker.persistentId);
            TdApi.InputMessageSticker message = new TdApi.InputMessageSticker(inputFile);//new TdApi.InputFileId(stickers[position].sticker.id));
            sendMessage(message);
            hideSoftKeyboard(getActivity());
        }
    };

    AdapterView.OnItemClickListener botsCommandsClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            BotCommand[] commands = searchBotsCommands(message.getText().toString().substring(1, message.length()));
            message.setText("");
            TdApi.InputMessageText message = new TdApi.InputMessageText(commands[position].command + (chatId >= 0 ? "": ( "@" + commands[position].username)));
            sendMessage(message);
            BCListView.setVisibility(View.GONE);
        }
    };

    AdapterView.OnItemClickListener usernameClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            BotCommand command = searchUsernames(message.getText().toString().substring(1, message.length()))[position];
            message.setText("@"+command.username);
            message.setSelection(message.getText().length());
            BCListView.setVisibility(View.GONE);
        }
    };

    AbsListView.OnScrollListener scrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            Log.d("scroll state", ""+scrollState);
            if (scrollState == 0) {
                wasScroll = false;
            }
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            Log.d("scroll on", "" + firstVisibleItem+" "+visibleItemCount+" "+totalItemCount);
            if ((firstVisibleItem < 15 && !wasScroll) || firstVisibleItem == 0) {//5
                wasScroll = true;
                getChatHistory();
                Log.d("scroll 1", ""+firstVisibleItem);
            }
            int pos = messages.size() - unreadCount;
            if (firstVisibleItem < pos && firstVisibleItem+visibleItemCount >= pos && floatingButton.getVisibility() == View.VISIBLE) {
                floatingButton.setVisibility(View.GONE);
                unreadCount = 0;
            }

        }
    };

    View.OnClickListener toolbarListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (users.size() < 2) return;
            if (chatId >= 0) {
                if (users.size() == 0) return;
                ProfileFragment fragment = new ProfileFragment();
                fragment.user = users.get(1);
                fragment.botInfo = botInfo;
                fragment.notificationSettings = chat.notificationSettings;
                showInfoFragment(fragment, "profile");
                //getFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, 0, 0, R.anim.exit_to_right).add(R.id.container, fragment, "profile").addToBackStack(null).commit();
            } else {
                if (groupChat == null) return;
                GroupFragment fragment = new GroupFragment();
                fragment.groupChat = groupChat;
                fragment.notificationSettings = chat.notificationSettings;
                showInfoFragment(fragment, "group");
                //getFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, 0, 0, R.anim.exit_to_right).add(R.id.container, fragment, "group").addToBackStack(null).commit();
            }
        }

        private void showInfoFragment(Fragment fragment, String tag) {
            if (Util.isTablet) {
                //getActivity().findViewById(R.id.container_layout_2).setVisibility(View.VISIBLE);
                TGFunc.showContainer(ChatFragment.this);
                getFragmentManager().beginTransaction().add(R.id.container, fragment, tag).addToBackStack(null).commit();
            } else {
                getFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, 0, 0, R.anim.exit_to_right)
                        .add(R.id.container, fragment, tag).addToBackStack(null).commit();
            }
        }

    };

    View.OnClickListener kbMenuListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if ((int)v.getTag() < 7) {
                viewPager.setCurrentItem((int) v.getTag());
            } else {
                Log.d("message.getText().l", ""+message.getText().length());
                if (message.getText().length() == 0) return;
                message.dispatchKeyEvent(new KeyEvent(0, 67));
            }
        }
    };

    View.OnClickListener stickerMenuListener = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            if ((int) v.getTag() == -1) {
                viewPager.setCurrentItem(0);

                LinearLayout header = (LinearLayout) getView().findViewById(R.id.header);
                header.setTranslationX(0);

                //LinearLayout header2 = (LinearLayout) rootView.findViewById(R.id.header2);
                HorizontalScrollView header2 = (HorizontalScrollView) getView().findViewById(R.id.header2);
                header2.setTranslationX(header.getLayoutParams().width);
            }

            stickerMenu.post(new Runnable() {
                @Override
                public void run() {
                    stickerMenu.setSelection((int) v.getTag());
                }
            });
            for (int i = 0; i < ((LinearLayout)v.getParent()).getChildCount(); i++) {
                ((LinearLayout)v.getParent()).getChildAt(i).setBackgroundColor(Color.parseColor("#f9f9f9"));
            }
            if ((int) v.getTag() == -1) {
                ((LinearLayout)v.getParent()).getChildAt(1).setBackgroundColor(Color.parseColor("#dddddd"));
            } else {
                v.setBackgroundColor(Color.parseColor("#dddddd"));
            }
        }
    };

    int kbPageNumber;
    ViewPager viewPager;

    private boolean isRecordRunning;
    private int recordDuration;


    private final long mFrequency = 100;
    private final int TICK_WHAT = 2;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message m) {
            updateRecordStopwatch();
            TGFunc.updatePlayer(getActivity(), getView());
            sendMessageDelayed(Message.obtain(this, TICK_WHAT), mFrequency);
        }
    };


    public View onCreateView2(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.chat, container, false);
        TGFunc.setToolbar(getActivity(), rootView, getArguments().getString("chatName"), true);
        toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        toolbar.setOnClickListener(toolbarListener);
        setHasOptionsMenu(true);
        listView = (ListView) rootView.findViewById(R.id.listView);

        client = TG.getClientInstance();
        chatId = getArguments().getLong("chatId");
        userId = getArguments().getInt("userId");
        Log.d("IDS", "" + chatId + " " + userId);
        //getChat();
        ((MainActivity)getActivity()).getService().onChatUpdateHandler = this;

        return rootView;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        Log.d("onCreateView", "CF");
        final View rootView = inflater.inflate(R.layout.chat, container, false);
        rootView.setX(320);
        message = (EditText) rootView.findViewById(R.id.message);
        onCreateView3(rootView);
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                //checkNotification();// inflater, container, savedInstanceState);
                rootView.animate().x(0).setDuration(400);
            }
        }, 400);
        //return rootView;
        //rootView.setAlpha(0);
        ///?????????

        return Util.isTablet ? rootView : new SwipeView(rootView);
    }

    //@Override
    public void onCreateView3(final View rootView) {//} LayoutInflater inflater, ViewGroup container,
                             //Bundle savedInstanceState) {
        Log.d("onCreateView", "CF");
        //final View rootView = inflater.inflate(R.layout.chat, container, false);
        TGFunc.setToolbar(getActivity(), rootView, getArguments().getString("chatName"), true);
        toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        toolbar.setOnClickListener(toolbarListener);
        if (Util.isTablet) {
            ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
            actionBar.setHomeAsUpIndicator(R.drawable.edit_cancel);
        }
        setHasOptionsMenu(true);

        listView = (ListView) rootView.findViewById(R.id.listView);
        BCLayout = (LinearLayout) rootView.findViewById(R.id.bots_comands_layout);
        BCListView = (ListView) rootView.findViewById(R.id.bots_comands);
        emptyView = rootView.findViewById(android.R.id.empty);

        slash = (ImageView) rootView.findViewById(R.id.slash);
        command = (ImageView) rootView.findViewById(R.id.command);
        kb_command = (ImageView) rootView.findViewById(R.id.command_keyboard);

        floatingButton = (ImageView) rootView.findViewById(R.id.floating_button);
        floatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //scrollToPosition(messages.size() - 1);
                scrollToUnread();
                floatingButton.setVisibility(View.GONE);
                unreadCount = 0;
            }
        });

        slash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                message.setText("/");
                message.setSelection(1);
                showSoftKeyboard(getActivity());
            }
        });

        command.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEmojiShowing) {
                    isEmojiShowing = false;
                    //showSoftKeyboard(getActivity());
                    getView().findViewById(R.id.smile).setVisibility(View.VISIBLE);
                    getView().findViewById(R.id.keyboard).setVisibility(View.GONE);
                    getView().findViewById(R.id.keyboard_smiles).setVisibility(View.GONE);
                }

                isBotKbPrepare = true;
                hideSoftKeyboard(getActivity());

                getView().findViewById(R.id.markupKeyboard).setVisibility(View.VISIBLE);
                kb_command.setVisibility(View.VISIBLE);
                command.setVisibility(View.GONE);
            }
        });

        kb_command.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getView().findViewById(R.id.markupKeyboard).setVisibility(View.GONE);
                kb_command.setVisibility(View.GONE);
                command.setVisibility(View.VISIBLE);
                showSoftKeyboard(getActivity());
            }
        });

        message = (EditText) rootView.findViewById(R.id.message);
        message.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if (System.currentTimeMillis() > curTypingExpired) {
                    curTypingExpired = System.currentTimeMillis() + 3000;
                    Log.d("curTypingExpired", curTypingExpired+" "+System.currentTimeMillis());
                    if (client != null) {
                        client.send(new TdApi.SendChatAction(chatId, new TdApi.SendMessageTypingAction()), new Client.ResultHandler() {
                            @Override
                            public void onResult(TdApi.TLObject object) {
                                Log.d("MySendChatAction", object.toString());// "KEYCODE = " + keyCode + " action = " + event.getAction());
                            }
                        });
                    }
                }

                if (s.length() == 0) {
                    getView().findViewById(R.id.attach).setVisibility(View.VISIBLE);
                    getView().findViewById(R.id.mic_btn).setVisibility(View.VISIBLE);
                    getView().findViewById(R.id.send).setVisibility(View.GONE);

                    if (hasCommandKb) {
                        command.setVisibility(View.VISIBLE);
                    } else if (hasBot) {
                        slash.setVisibility(View.VISIBLE);
                    }
                    BCLayout.setVisibility(View.GONE);

                    getActivity().invalidateOptionsMenu();
                } else {
                    getView().findViewById(R.id.attach).setVisibility(View.GONE);
                    getView().findViewById(R.id.mic_btn).setVisibility(View.GONE);
                    getView().findViewById(R.id.send).setVisibility(View.VISIBLE);
                    slash.setVisibility(View.GONE);
                    command.setVisibility(View.GONE);
                    getActivity().invalidateOptionsMenu();
                }
                if (getBotsCommands().length == 0) return;

                if (s.length() > 0) {
                    String subStr = s.length() > 1 ? s.toString().substring(1, s.length()) : "";
                    if (s.toString().substring(0, 1).equals("/")) {
                        BCLayout.setVisibility(View.VISIBLE);
                        BCListView.setAdapter(new BotsCommandsAdapter(getActivity(), searchBotsCommands(subStr)));
                        BCListView.setOnItemClickListener(botsCommandsClickListener);
                    }
                    if (chatId < 0 && s.toString().substring(0, 1).equals("@")) {
                        BCLayout.setVisibility(View.VISIBLE);
                        BCListView.setAdapter(new BotsCommandsAdapter(getActivity(), searchUsernames(subStr)));
                        BCListView.setOnItemClickListener(usernameClickListener);
                    }
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        message.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.d("onKeyView msg", "KEYCODE = " + keyCode + " action = " + event.getAction());
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    Log.d("onKeyView msg", "KEYCODE_BACK");
                    onBackPressed();
                    return true;
                }
                return false;
            }
        });
        message.requestFocus();

        hideSoftKeyboard(getActivity());
        rootView.findViewById(R.id.smile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wasEmojiPressed = true;
                isEmojiShowing = true;
                //message.requestFocus();
                hideSoftKeyboard(getActivity());

                if (hasCommandKb) {
                    getView().findViewById(R.id.command_keyboard).setVisibility(View.GONE);
                    getView().findViewById(R.id.markupKeyboard).setVisibility(View.GONE);
                    command.setVisibility(View.VISIBLE);
                }
                getView().findViewById(R.id.smile).setVisibility(View.GONE);
                getView().findViewById(R.id.keyboard).setVisibility(View.VISIBLE);
                getView().findViewById(R.id.keyboard_smiles).setVisibility(View.VISIBLE);
                Point size = new Point();
                getActivity().getWindowManager().getDefaultDisplay().getSize(size);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        Util.getPreferences().getInt(size.x < size.y ? "kb_height" : "kb_height_land", Util.dp(200)));
                getView().findViewById(R.id.keyboard_smiles).setLayoutParams(layoutParams);
            }
        });

        rootView.findViewById(R.id.keyboard).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isEmojiShowing = false;
                //message.requestFocus();
                showSoftKeyboard(getActivity());
                getView().findViewById(R.id.smile).setVisibility(View.VISIBLE);
                getView().findViewById(R.id.keyboard).setVisibility(View.GONE);
                getView().findViewById(R.id.keyboard_smiles).setVisibility(View.GONE);
            }
        });

        rootView.findViewById(R.id.attach).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAttach();
            }
        });

        final LinearLayout micBtn = (LinearLayout) rootView.findViewById(R.id.mic_btn);
        final TextView slideCancel = (TextView) rootView.findViewById(R.id.record_text_cancel);

        rootView.findViewById(R.id.mic).setOnTouchListener(new View.OnTouchListener() {
            float lx, x;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        isRecordRunning = true;
                        lx = event.getX();
                        onRecordStart(v);
                        MediaUtil.getInstance().startRecording();
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        slideCancel.setTranslationX(event.getX() - lx);
                        micBtn.setTranslationX(event.getX() - lx);
                        if (slideCancel.getX() < Util.dp(70)) {
                            MediaUtil.getInstance().stopRecording();
                            onRecordEnd(v);
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        if (!isRecordRunning) return false;
                        //Log.d("mic", "click stop");
                        MediaUtil.getInstance().stopRecording();
                        if (recordDuration > 1000) {
                            TdApi.InputMessageVoice messageVoice = new TdApi.InputMessageVoice(new TdApi.InputFileLocal(TGFunc.getDir() + "temp_voice.ogg"), recordDuration / 1000);
                            sendMessage(messageVoice);
                        }
                        onRecordEnd(v);
                        break;
                }
                return false;
            }

            private void onRecordStart(View v) {
                //Log.d("mic", "click start = " + micBtn.getTranslationX());
                rootView.findViewById(R.id.msg_panel).setVisibility(View.INVISIBLE);
                rootView.findViewById(R.id.record_panel).setVisibility(View.VISIBLE);

                micBtn.getLayoutParams().width = Util.dp(150);
                micBtn.getLayoutParams().height = Util.dp(150);
                ((RelativeLayout.LayoutParams) micBtn.getLayoutParams())
                        .setMargins(0, 0, Util.dp(-50), Util.dp(-50));

                View shadow = rootView.findViewById(R.id.mic_shadow);
                shadow.setBackgroundResource(R.drawable.shadow_round);
                shadow.getLayoutParams().width = Util.dp(110);
                shadow.getLayoutParams().height = Util.dp(110);

                v.setBackgroundResource(R.drawable.shadow_round_red);
                ((ImageView) v).setImageResource(R.drawable.ic_mic_white);
                v.getLayoutParams().width = Util.dp(100);
                v.getLayoutParams().height = Util.dp(100);
                int p = Util.dp(35);
                v.setPadding(p, p, p, p);

                //micBtn.requestLayout();
            }

            private void onRecordEnd(View v) {
                isRecordRunning = false;
                recordDuration = 0;

                rootView.findViewById(R.id.msg_panel).setVisibility(View.VISIBLE);
                rootView.findViewById(R.id.record_panel).setVisibility(View.GONE);
                v.setBackgroundResource(0);
                ((ImageView) v).setImageResource(R.drawable.ic_mic);

                int s = Util.dp(50);

                micBtn.getLayoutParams().width = s;
                micBtn.getLayoutParams().height = s;
                ((RelativeLayout.LayoutParams) micBtn.getLayoutParams())
                        .setMargins(0, 0, 0, 0);
                micBtn.setTranslationX(0);

                View shadow = rootView.findViewById(R.id.mic_shadow);
                shadow.setBackgroundResource(0);
                shadow.getLayoutParams().width = s;
                shadow.getLayoutParams().height = s;

                v.getLayoutParams().width = s;
                v.getLayoutParams().height = s;
                int p = Util.dp(12);
                v.setPadding(p, p, p, p);

                //micBtn.requestLayout();
            }
        });

        rootView.findViewById(R.id.send).setOnClickListener(onSendMessage);
        client = TG.getClientInstance();
        chatId = getArguments().getLong("chatId");
        userId = getArguments().getInt("userId");
        Log.d("IDS", "" + chatId + " " + userId);
        //getChat();
        unreadCount = chat.unreadCount;
        ((MainActivity)getActivity()).getService().onChatUpdateHandler = this;
        getChatUsers();

        stickerSets = ((MainActivity)getActivity()).getService().stickerSets;
        stickerSets.set(0, getRecentStickers(stickerSets));
        sortStickerSets(stickerSets);

        views = new ArrayList<>();
        final LinearLayout header = (LinearLayout) rootView.findViewById(R.id.header);

        Point size = new Point();
        getActivity().getWindowManager().getDefaultDisplay().getSize(size);

        final int width = size.x;
        final HorizontalScrollView header2 = (HorizontalScrollView) rootView.findViewById(R.id.header2);
        header2.getLayoutParams().width = size.x;//header.getLayoutParams().width;
        Log.d("header.getLP().width", "" + size.x);//header.getLayoutParams().width);
        header2.requestLayout();
        header2.addView(getStickerMenu());

        for (int i = 0; i < 8; i++) {
            header.addView(getKBMenuItem(i));
            if (i < 7) {
                if (i != 6) {
                    GridView gridView = new GridView(getActivity());
                    gridView.setNumColumns(TGFunc.getChatWidth() / Util.dp(45));//size.x / Util.dp(45));
                    if (i == 0) Emoji.data[0] = getRecentEmoji();
                    gridView.setAdapter(new EmojiAdapter(getActivity(), Emoji.data[i]));
                    gridView.setOnItemClickListener(emojiClickListener);
                    views.add(gridView);
                } else {
                    stickerMenu = new ListView(getActivity());
                    stickerMenu.setDividerHeight(0);
                    //listView.setNumColumns(size.x / 80);
                    stickerMenu.setAdapter(new StickerSetsAdapter(getActivity(), stickerSets, stickerClickListener));
                    stickerMenu.setOnScrollListener(new AbsListView.OnScrollListener() {
                        @Override public void onScrollStateChanged(AbsListView view, int scrollState) {}

                        @Override
                        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                            if (header2.getChildCount() > 0) {
                                //Log.d("onScrollStickers co", ""+header2.getChildCount());
                                LinearLayout ll = (LinearLayout) header2.getChildAt(0);
                                for (int i = 0; i < ll.getChildCount(); i++) {
                                    ll.getChildAt(i).setBackgroundColor(Color.parseColor("#f9f9f9"));
                                }

                                if (ll.getChildCount() > firstVisibleItem + 1) {
                                    ll.getChildAt(firstVisibleItem + 1).setBackgroundColor(Color.parseColor("#dddddd"));
                                }
                                Log.d("onScrollStickers", "" + firstVisibleItem);
                            }
                        }
                    });

                    views.add(stickerMenu);
                }
            }
        }

        header.getChildAt(0).findViewWithTag("line").setBackgroundColor(getResources().getColor(android.R.color.holo_blue_dark));
        header.getChildAt(0).setSelected(true);

        viewPager = (ViewPager) rootView.findViewById(R.id.view_pager);
        viewPager.setAdapter(new EmojiPagerAdapter(views));
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Log.d("onPageScrolled", String.format("pos = %d, offset = %f, px = %d", position, positionOffset, positionOffsetPixels));
                if (position == 5) {
                    LinearLayout header = (LinearLayout) rootView.findViewById(R.id.header);
                    header.setTranslationX(-positionOffsetPixels);

                    header2.setTranslationX(-positionOffsetPixels);
                }
            }

            @Override
            public void onPageSelected(int position) {
                Log.d("onPageSelected", "pos = " + position);

                kbPageNumber = position;

                LinearLayout v = (LinearLayout) getView().findViewById(R.id.header);
                for (int i = 0; i < v.getChildCount(); i++) {
                    v.getChildAt(i).findViewWithTag("line").setBackgroundColor(0xddd);
                    v.getChildAt(i).setSelected(false);
                }
                v.getChildAt(position).findViewWithTag("line").setBackgroundColor(getResources().getColor(android.R.color.holo_blue_dark));
                v.getChildAt(position).setSelected(true);

                if (position == 6) {
                    LinearLayout header = (LinearLayout) rootView.findViewById(R.id.header);
                    header.setTranslationX(-width);
                    header2.setTranslationX(-width);
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                Log.d("onPageScrollStateChange", "state = " + state);
            }
        });

        rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (getView() == null) return;
                int heightDiff = rootView.getRootView().getHeight() - rootView.getHeight();
                Log.d("heightDiff", "" + heightDiff);
                if (heightDiff >= Util.dp(150) && !wasEmojiPressed) {

                    getView().findViewById(R.id.smile).setVisibility(View.VISIBLE);
                    getView().findViewById(R.id.keyboard).setVisibility(View.GONE);
                    if (isEmojiShowing) {
                        isEmojiShowing = false;
                        getView().findViewById(R.id.keyboard_smiles).setVisibility(View.GONE);
                    }

                    Log.d("getStatusBarHeight", "" + getStatusBarHeight());


                    Point size = new Point();
                    getActivity().getWindowManager().getDefaultDisplay().getSize(size);
                    Util.getPreferences().edit().putInt(size.x < size.y ? "kb_height" : "kb_height_land", heightDiff - getStatusBarHeight()).commit();
                }
                if (wasEmojiPressed) wasEmojiPressed = false;

                if (getView() == null) return;
                View view = getView().findViewById(R.id.markupKeyboard);
                if (heightDiff >= Util.dp(150) && !isBotKbPrepare) {
                    if (view.getVisibility() == View.VISIBLE) {
                        view.setVisibility(View.GONE);
                        kb_command.setVisibility(View.GONE);
                        command.setVisibility(View.VISIBLE);
                    }
                }
                if (isBotKbPrepare) isBotKbPrepare = false;
            }
        });

        mHandler.sendMessageDelayed(Message.obtain(mHandler, TICK_WHAT), mFrequency);
//        return Util.isTablet ? rootView : new SwipeView(rootView);
    }

    @Override
    public void onPause() {
        super.onPause();
        message.clearFocus();
    }

    @Override
    public void onResume() {
        super.onResume();
        message.requestFocus();
        //message.clearFocus();
        //hideSoftKeyboard(getActivity());
        Activity activity = getActivity();
        if (activity == null || activity.getCurrentFocus() == null) return;
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(message.getWindowToken(), 0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        TGService service = ((MainActivity) getActivity()).getService();
        if (service != null) service.onChatUpdateHandler = null;
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.chat, menu);
        if (chatId < 0) menu.getItem(1).setVisible(true);
        if (((EditText)getView().findViewById(R.id.message)).length() > 0) {
            menu.getItem(0).setVisible(false);
            menu.getItem(1).setVisible(false);
            menu.getItem(2).setVisible(false);
            menu.getItem(3).setVisible(true);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_clear:
                new AlertDialog.Builder(getActivity())
                        .setTitle(R.string.clear_history)
                        .setPositiveButton(R.string.ok, okClearHistoryDialog)
                        .setNegativeButton(R.string.cancel, null)
                        .setMessage(R.string.clear_history_text)
                        .show();
                break;
            case R.id.action_leave:
                new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.app_name)
                    .setPositiveButton(R.string.ok, okLeaveGroupDialog)
                    .setNegativeButton(R.string.cancel, null)
                    .setMessage(R.string.leave_group_text)
                    .show();
                break;
            case R.id.action_mute:
                break;
            case R.id.action_attach:
                showAttach();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 112 && resultCode == Activity.RESULT_OK) {
            Log.d("forwardLimit", ""+data.getIntExtra("forwardLimit", 0));
            client.send(new TdApi.AddChatParticipant(chatId, data.getIntExtra("selectedContactId", 0), data.getIntExtra("forwardLimit", 0)), new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {
                    Log.d("AddChatParticipant", object.toString());
                }
            });
        }
    }

    private void onBackPressed() {
        if (isEmojiShowing) {
            isEmojiShowing = false;
            //showSoftKeyboard(getActivity());
            getView().findViewById(R.id.smile).setVisibility(View.VISIBLE);
            getView().findViewById(R.id.keyboard).setVisibility(View.GONE);
            getView().findViewById(R.id.keyboard_smiles).setVisibility(View.GONE);
        } else if (getView().findViewById(R.id.markupKeyboard).getVisibility() == View.VISIBLE) {
            isBotKbPrepare = false;
            getView().findViewById(R.id.markupKeyboard).setVisibility(View.GONE);
            command.setVisibility(View.VISIBLE);
            kb_command.setVisibility(View.GONE);
        } else {
            if (Util.isTablet) {
                ((FrameLayout)getActivity().findViewById(R.id.container_right)).removeAllViews();
            } else {
                getFragmentManager().popBackStack();
            }
        }
    }

    private BotCommand[] searchUsernames(String sub) {
        if (sub.length() == 0) return getUsernames();
        ArrayList<BotCommand> commandsList = new ArrayList<>();
            for (TdApi.User user : users) {
                if (user.username.length() >= sub.length() && user.username.substring(0, sub.length()).equals(sub)) {
                    commandsList.add(getUsername(user));
                }
            }
        return commandsList.toArray(new BotCommand[commandsList.size()]);
    }

    private BotCommand[] getUsernames() {
        ArrayList<BotCommand> commandsList = new ArrayList<>();
        for (TdApi.User user : users) {
            if (user.username.length() != 0) {
                commandsList.add(getUsername(user));
            }
        }
        return commandsList.toArray(new BotCommand[commandsList.size()]);
    }

    private BotCommand getUsername(TdApi.User user) {
        BotCommand command = new BotCommand();
        command.command = user.firstName+" "+user.lastName;
        command.description = "@"+user.username;
        command.path = user.profilePhoto.small.path;
        command.username = user.username;
        return command;
    }

    private BotCommand[] searchBotsCommands(String sub) {
        if (sub.length() == 0) return getBotsCommands();
        ArrayList<BotCommand> commandsList = new ArrayList<>();
        if (groupChat != null) {
            for (int i = 0; i < groupChat.participants.length; i++) {
                if (groupChat.participants[i].botInfo instanceof TdApi.BotInfoGeneral) {
                    TdApi.BotCommand commands[] = ((TdApi.BotInfoGeneral) groupChat.participants[i].botInfo).commands;
                    for (TdApi.BotCommand command : commands) {
                        if (command.command.length() >= sub.length() && command.command.substring(0, sub.length()).equals(sub)) {
                            TdApi.User user = groupChat.participants[i].user;
                            commandsList.add(getBotCommand(command, user.profilePhoto.small.path, user.username));
                        }
                    }
                }
            }
        } else if (botInfo != null && botInfo instanceof TdApi.BotInfoGeneral) {
            TdApi.BotCommand commands[] = ((TdApi.BotInfoGeneral)botInfo).commands;
            for (TdApi.BotCommand command : commands) {
                if (command.command.length() >= sub.length() && command.command.substring(0, sub.length()).equals(sub)) {
                    commandsList.add(getBotCommand(command, users.get(1).profilePhoto.small.path, users.get(1).username));
                }
            }
        }
        return commandsList.toArray(new BotCommand[commandsList.size()]);
    }

    private BotCommand[] getBotsCommands() {
        ArrayList<BotCommand> commandsList = new ArrayList<>();
        if (groupChat != null) {
            for (int i = 0; i < groupChat.participants.length; i++) {
                if (groupChat.participants[i].botInfo instanceof TdApi.BotInfoGeneral) {
                    TdApi.BotCommand commands[] = ((TdApi.BotInfoGeneral) groupChat.participants[i].botInfo).commands;
                    for (TdApi.BotCommand command : commands) {
                        TdApi.User user = groupChat.participants[i].user;
                        commandsList.add(getBotCommand(command, user.profilePhoto.small.path, user.username));
                    }
                }
            }
        } else if (botInfo != null && botInfo instanceof TdApi.BotInfoGeneral) {
            TdApi.BotCommand commands[] = ((TdApi.BotInfoGeneral)botInfo).commands;
            for (TdApi.BotCommand command : commands) {
                commandsList.add(getBotCommand(command, users.get(1).profilePhoto.small.path, users.get(1).username));
            }
        }
        return commandsList.toArray(new BotCommand[commandsList.size()]);
    }

    private BotCommand getBotCommand(TdApi.BotCommand tcommand, String path, String username) {
        BotCommand command = new BotCommand();
        command.command = "/"+tcommand.command;
        command.description = tcommand.description;
        command.path = path;
        command.username = username;
        return command;
    }

    private void changeLoudnessLevelRecord(int i) {
        if (getView() == null) return;
        View shadow = getView().findViewById(R.id.mic_shadow);
        shadow.setBackgroundResource(R.drawable.shadow_round);
        Log.d("level =", "" + i);
        i = i/(127/40);
        Log.d("level new =", "" + i);
        if (i > 40) i = 40;
        shadow.getLayoutParams().width = Util.dp(110+i);
        shadow.getLayoutParams().height = Util.dp(110+i);
    }

    private void updateRecordStopwatch() {
        if (isRecordRunning) {
            recordDuration += 100;
            if (getView() == null) return;
            TextView textView = (TextView) getView().findViewById(R.id.record_time);
            if (textView != null) {
                textView.setText(TGFunc.getFormedDuration(recordDuration/1000));
            }
            changeLoudnessLevelRecord(MediaUtil.getInstance().getLoudnessRecord());
        }
    }

    private long[] getRecentEmoji() {
        String[] recentEmojiS = Util.getPreferences().getString("smiles", "").split(":");
        long recentEmoji[] = new long[recentEmojiS.length];
        for (int i = 0; i < recentEmojiS.length; i++) {
            recentEmoji[i] = Long.parseLong(recentEmojiS[i]);
        }
        return recentEmoji;
    }

    public static void addRecentEmoji(long code) {
        String id = String.valueOf(code);
        String smiles = Util.getPreferences().getString("smiles", "");
        Log.d("addRecentEmoji", ""+id+"    "+smiles);
        for (String recentId : smiles.split(":")) {
            if (recentId.equals(id)) {
                return;
            }
        }
        if (smiles.length() != 0) {
            id = id + ":" + smiles;
        }
        Util.getPreferences().edit().putString("smiles", id).commit();
    }

    private TdApi.StickerSet getRecentStickers(ArrayList<TdApi.StickerSet> stickerSets) {
        ArrayList<TdApi.Sticker> stickers = new ArrayList<>();
        String stickerIds[] = Util.getPreferences().getString("stickers", "").split("-");
        Log.d("rec st", "" + stickerIds.length);
        if (stickerSets.size() != 0) stickerSets.get(0).stickers = null;

        for (String id : stickerIds) {
            for (TdApi.StickerSet stickerSet : stickerSets) {
                if (stickerSet.stickers != null) {
                    for (TdApi.Sticker sticker : stickerSet.stickers) {
                        Log.d("rec str", "" + id + "   "+sticker.sticker.persistentId);
                        if (sticker.sticker.persistentId.equals(id)) {
                            stickers.add(sticker);
                            break;
                        }
                    }
                }
            }
        }
        TdApi.StickerSet newStickerSet = new TdApi.StickerSet();
        newStickerSet.stickers = stickers.toArray(new TdApi.Sticker[stickers.size()]);
        return newStickerSet;
    }

    private void addRecentSticker(String id) {
        String stickers = Util.getPreferences().getString("stickers", "");
        for (String recentId : stickers.split("-")) {
            if (recentId.equals(id)) {
                return;
            }
        }
        if (stickers.length() != 0) {
            id = id + "-" + stickers;
        }
        Util.getPreferences().edit().putString("stickers", id).commit();
    }

    public void updateEmoji() {
        for (View view : views) {
            if (view instanceof GridView) {
                ((GridView) view).invalidateViews();
            }
        }
    }

    private void sortStickerSets(ArrayList<TdApi.StickerSet> stickerSets) {
        Collections.sort(stickerSets, new Comparator<TdApi.StickerSet>() {
            @Override
            public int compare(TdApi.StickerSet lhs, TdApi.StickerSet rhs) {
                Log.d("lhs, rhs", "" + lhs.id + " " + rhs.id);
                if (lhs.id == 0) return -1;
                if (rhs.id == 0) return +1;
                int a = TGFunc.getPreferences(getActivity()).getInt("countTimesStickerSet_" + lhs.id, 0);
                int b = TGFunc.getPreferences(getActivity()).getInt("countTimesStickerSet_" + rhs.id, 0);
                //double a = lhs.rating;
                //double b = rhs.rating;

                Log.d("a, b ", "" + lhs.title + " " + a + " rhs = " + rhs.title + " " + b);
                return a > b ? -1 : a < b ? +1 : 0;
            }
        });
        for (TdApi.StickerSet stickerSet : stickerSets) {
            if (stickerSet.stickers != null) {
                Arrays.sort(stickerSet.stickers, new Comparator<TdApi.Sticker>() {
                    @Override
                    public int compare(TdApi.Sticker lhs, TdApi.Sticker rhs) {
                        //int a = TGFunc.getPreferences(getActivity()).getInt("countTimesStickerSet_" + lhs.id, 0);
                        //int b = TGFunc.getPreferences(getActivity()).getInt("countTimesStickerSet_" + rhs.id, 0);
                        double a = lhs.rating;
                        double b = rhs.rating;
                        Log.d("st rating a, b ", "" + a + " rhs = " + b);
                        return a > b ? -1 : a < b ? +1 : 0;
                    }
                });
            }
        }
    }

    public static ArrayList<TdApi.StickerSet> cloneStickerSets(List<TdApi.StickerSet> list) {
        ArrayList<TdApi.StickerSet> clone = new ArrayList<>();//list.size());
        for(TdApi.StickerSet item: list) clone.add(item);//.clone());
        return clone;
    }

    private void scrollToUnread() {
        scrollToPosition(unreadCount > 0 ? messages.size() - 1 - unreadCount : messages.size() - 1);
    }

    private void scrollToPosition(final int position) {
        listView.post(new Runnable() {
            @Override
            public void run() {
                listView.setSelection(position);
            }
        });
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private void showAttach() {
        hideSoftKeyboard(getActivity());
        Bundle bundle = new Bundle();
        bundle.putLong("chatId", chatId);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        Fragment fragment = new AttachmentFragment();
        fragment.setArguments(bundle);
        TGFunc.showContainer(this);
        fragmentManager.beginTransaction().add(R.id.container, fragment, "attach").addToBackStack(null).commit();
    }

    public static void showSoftKeyboard(Activity activity) {
        if (activity == null) return;
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public static void hideSoftKeyboard(Activity activity) {
        if (activity == null || activity.getCurrentFocus() == null) return;
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    public void updateHandler(TdApi.TLObject object) {
        if (getFragmentManager() == null) return;
        switch (object.getConstructor()) {
            case TdApi.UpdateNewMessage.CONSTRUCTOR:
                Log.d("Chat Upd msg", object.toString());
                updateNewMessage(object);
                break;
            case TdApi.UpdateDeleteMessages.CONSTRUCTOR:
                updateDeleteMessages((TdApi.UpdateDeleteMessages) object);
                break;
            case TdApi.UpdateFile.CONSTRUCTOR:
                Log.d("Photo Upd file", object.toString());
                updateFile(((TdApi.UpdateFile) object));
                break;
            case TdApi.UpdateUser.CONSTRUCTOR:
            case TdApi.UpdateUserStatus.CONSTRUCTOR:
                Log.d("Upd user", object.toString());
                updateUser(object);
                break;
            case TdApi.UpdateFileProgress.CONSTRUCTOR:
                updateFileProgress((TdApi.UpdateFileProgress) object);
                break;
            case TdApi.UpdateChatReplyMarkup.CONSTRUCTOR:
                Log.d("UpdateChatReplyInner", object.toString());
                TdApi.UpdateChatReplyMarkup updateChatReplyMarkup = (TdApi.UpdateChatReplyMarkup) object;
                if (chat.id == updateChatReplyMarkup.chatId) {
                    chat.replyMarkupMessageId = updateChatReplyMarkup.replyMarkupMessageId;
                    if (chat.replyMarkupMessageId == 0) {
                        hideBotCommandKeyboard();
                    }
                }
                break;
            case TdApi.UpdateUserAction.CONSTRUCTOR:
                TdApi.UpdateUserAction action = (TdApi.UpdateUserAction) object;
                if (action.chatId == chatId) {
                    int pos = -1;
                    for (int i = 0; i < typings.size(); i++) {
                        if (typings.get(i) == action.userId) {
                            pos = i;
                            break;
                        }
                    }
                    if (action.action instanceof TdApi.SendMessageTypingAction) {
                        if (pos == -1) {
                            typings.add(action.userId);
                            pos = typings.size()-1;
                        }
                        typings_expired.add(pos, System.currentTimeMillis() + 4500);
                        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                for (int i = typings_expired.size()-1; i >= 0; i--) {
                                    if (System.currentTimeMillis() > typings_expired.get(i)) {
                                        typings.remove(i);
                                        typings_expired.remove(i);
                                    }
                                }
                                updateToolbar();
                            }
                        }, 5000);
                    }
                    if (action.action instanceof TdApi.SendMessageCancelAction) {
                        if (pos != -1) {
                            typings.remove(pos);
                            typings_expired.remove(pos);
                        }
                    }
                    updateToolbar();
                }
                break;
        }
    }

    private void changeReplyMarkup(TdApi.Message message) {
        Log.d("replyMarkup", "" + message.replyMarkup.toString());
        TdApi.ReplyMarkup replyMarkup = message.replyMarkup;
        switch (replyMarkup.getConstructor()) {
            case TdApi.ReplyMarkupShowKeyboard.CONSTRUCTOR:
                break;
        }
        if (replyMarkup instanceof TdApi.ReplyMarkupForceReply) {
            chat.replyMarkupMessageId = message.id;
            hideBotCommandKeyboard();

            getView().findViewById(R.id.reply).setVisibility(View.VISIBLE);
            getView().findViewById(R.id.reply_cancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    chat.replyMarkupMessageId = 0;
                    getView().findViewById(R.id.reply).setVisibility(View.GONE);
                }
            });
            TdApi.User user = TGFunc.getUserById(users, message.fromId);
            String username = user.firstName + " " + user.lastName;
            ((TextView) getView().findViewById(R.id.reply_name)).setText(username);
            ((TextView)getView().findViewById(R.id.reply_message)).setText(TGFunc.getTopMessage(getActivity(), username, message.message));
        }
        if (replyMarkup instanceof TdApi.ReplyMarkupHideKeyboard) {
            chat.replyMarkupMessageId = message.id;
            hideBotCommandKeyboard();
        }
        if (replyMarkup instanceof TdApi.ReplyMarkupShowKeyboard) {
            isBotKbPrepare = true;
            hideSoftKeyboard(getActivity());

            chat.replyMarkupMessageId = message.id;
            slash.setVisibility(View.GONE);
            command.setVisibility(View.GONE);
            kb_command.setVisibility(View.VISIBLE);
            hasCommandKb = true;

            final Point size = new Point();
            getActivity().getWindowManager().getDefaultDisplay().getSize(size);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            layoutParams.weight = 1;

            final ScrollView markupKeyboard = (ScrollView) getView().findViewById(R.id.markupKeyboard);
            int height = Util.getPreferences().getInt(size.x < size.y ? "kb_height" : "kb_height_land", Util.dp(200));
            markupKeyboard.getLayoutParams().height = height == 0 ? Util.dp(200) : height;

            markupKeyboard.setVisibility(View.VISIBLE);

            LinearLayout linearLayout = new LinearLayout(getActivity());
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            linearLayout.setLayoutParams(layoutParams);
            int m = Util.dp(5);
            ((LinearLayout.LayoutParams)linearLayout.getLayoutParams()).setMargins(m, m, m, m);

            m = Util.dp(2);
            int mtb = Util.dp(1);
            TdApi.ReplyMarkupShowKeyboard markup = (TdApi.ReplyMarkupShowKeyboard)replyMarkup;
            for (int i = 0; i < markup.rows.length; i++) {
                Log.d("mkb", "" + markup.rows.length + " " + i + " = " + markup.rows[i].length);
                LinearLayout row = new LinearLayout(getActivity());
                row.setOrientation(LinearLayout.HORIZONTAL);
                for (int j = 0; j < markup.rows[i].length; j++) {
                    LinearLayout ll = new LinearLayout(getActivity());
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    lp.weight = 1;
                    lp.setMargins(m, mtb, m, mtb);

                    final Button button = new Button(getActivity());
                    button.setText(markup.rows[i][j]);
                    button.setBackgroundColor(Color.GRAY);
                    //button.setPadding(p, p, p, p);
                    button.getLayoutParams();
                    button.setBackgroundResource(R.drawable.command);
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getView().findViewById(R.id.reply).setVisibility(View.GONE);
                            TdApi.InputMessageText message = new TdApi.InputMessageText(button.getText().toString());
                            client.send(new TdApi.SendMessage(chatId, chatId >= 0 ? 0 : chat.replyMarkupMessageId, false, new TdApi.ReplyMarkupForceReply(true), message), sendMessage);
                        }
                    });
                    ll.addView(button, lp);
                    row.addView(ll, layoutParams);
                }
                linearLayout.addView(row, layoutParams);
            }
            markupKeyboard.removeAllViews();
            markupKeyboard.addView(linearLayout);//, layoutParams);
        }
    }

    private void hideBotCommandKeyboard() {
        slash.setVisibility(View.VISIBLE);
        command.setVisibility(View.GONE);
        kb_command.setVisibility(View.GONE);
        getView().findViewById(R.id.markupKeyboard).setVisibility(View.GONE);
        hasCommandKb = false;
        isBotKbPrepare = false;
        showSoftKeyboard(getActivity());
    }

    private View getKBMenuItem(int i) {
        LinearLayout linearLayout = new LinearLayout(getActivity());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParams.weight = 1;
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setOnClickListener(kbMenuListener);
        linearLayout.setTag(i);


        ImageView imageView = new ImageView(getActivity());
        imageView.setImageResource(menuIcons[i]);
        imageView.setLayoutParams(layoutParams);
        int px = Util.dp(6);
        imageView.setPadding(px, px, px, px);

        linearLayout.addView(imageView);

        View view = new View(getActivity());
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Util.dp(2)));
        view.setBackgroundColor(0xddd);
        view.setTag("line");
        linearLayout.addView(view);

        return linearLayout;
    }

    private View getStickerMenu() {
        LinearLayout header = new LinearLayout(getActivity());

        int menuIcons[] = {R.drawable.ic_emoji_smile,
                R.drawable.ic_emoji_recent};

        for (int i = 0; i < 2; i++) {
            LinearLayout linearLayout = new LinearLayout(getActivity());
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(Util.dp(50), Util.dp(50));//ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            layoutParams.weight = 1;
            linearLayout.setLayoutParams(layoutParams);
            linearLayout.setOrientation(LinearLayout.VERTICAL);
            linearLayout.setOnClickListener(stickerMenuListener);
            linearLayout.setTag(i - 1);

            ImageView imageView = new ImageView(getActivity());
            imageView.setImageResource(menuIcons[i]);
            imageView.setLayoutParams(layoutParams);
            int px = Util.dp(7);
            imageView.setPadding(px, px, px, px);

            linearLayout.addView(imageView);

            header.addView(linearLayout, linearLayout.getLayoutParams());
        }

        for (int i = 1; i < stickerSets.size(); i++) {
            TdApi.StickerSet stickerSet = stickerSets.get(i);
            if (stickerSet.stickers != null) {
                LinearLayout linearLayout = new LinearLayout(getActivity());
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(Util.dp(50), Util.dp(50));//ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                layoutParams.weight = 1;
                linearLayout.setLayoutParams(layoutParams);
                linearLayout.setOrientation(LinearLayout.VERTICAL);
                linearLayout.setOnClickListener(stickerMenuListener);
                linearLayout.setTag(i);

                ImageView imageView = new ImageView(getActivity());
                if (stickerSet.stickers.length != 0) {
                    TGFunc.loadSticker(stickerSet.stickers[0].thumb.photo.path, imageView);
                }

                imageView.setLayoutParams(layoutParams);
                int px = Util.dp(10);
                imageView.setPadding(px, px, px, px);

                linearLayout.addView(imageView);

                header.addView(linearLayout, linearLayout.getLayoutParams());
            }
        }
        return header;
    }

    public void downloadFile(int id) {
        client.send(new TdApi.DownloadFile(id), downloadFile);
    }

    private void downloadChatPhotos() {
        for (int i = 0; i < users.size(); i++) {
            int id = getPhotoIdByUser(users.get(i));
            if (id != 0) {
                downloadFile(id);
            }
        }
    }

    private void leaveGroup() {
        Client client = TG.getClientInstance();
        if (client == null) return;
        client.send(new TdApi.DeleteChatParticipant(chatId, userId), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                MessagesFragment fragment = (MessagesFragment) fragmentManager.findFragmentByTag("main");
                if (fragment == null) return;
                fragment.deleteChat(chatId);
                fragmentManager.popBackStack();
            }
        });
    }

    int size = 0;

    private void downloadPreviewFiles(ArrayList<TdApi.Message> messages) {
        for (int i = 0; i < messages.size(); i++) {
            if (messages.get(i).message != null &&TGFunc.getFreeSpace()-size > 200000) {
                TdApi.MessageContent message = messages.get(i).message;
                TdApi.PhotoSize[] photos;
                switch (message.getConstructor()) {
                    case TdApi.MessageSticker.CONSTRUCTOR:
                        if (((TdApi.MessageSticker) message).sticker.sticker.path.length() == 0) {
                            downloadFile(((TdApi.MessageSticker) message).sticker.sticker);
                        }
                        break;
                    case TdApi.MessagePhoto.CONSTRUCTOR:
                        photos = ((TdApi.MessagePhoto) message).photo.photos;

                        if (TGFunc.getSmallestPhoto(photos).photo.path.length() == 0) {
                            downloadFile(TGFunc.getSmallestPhoto(photos).photo);
                        }
                        if (TGFunc.getBetterPhoto(photos).photo.path.length() == 0) {
                            downloadFile(TGFunc.getBetterPhoto(photos).photo);
                        }
                        break;
                    case TdApi.MessageDocument.CONSTRUCTOR:
                        if (((TdApi.MessageDocument) message).document.thumb.photo.path.length() == 0) {
                            downloadFile(((TdApi.MessageDocument) message).document.thumb.photo);
                        }
                        break;
                    case TdApi.MessageWebPage.CONSTRUCTOR:
                        photos = ((TdApi.MessageWebPage) message).webPage.photo.photos;
                        if (photos.length == 0) break;
                        if (TGFunc.getSmallestPhoto(photos).photo.path.length() == 0) {
                            downloadFile(TGFunc.getSmallestPhoto(photos).photo);
                        }
                        if (TGFunc.getBetterPhoto(photos).photo.path.length() == 0) {
                            downloadFile(TGFunc.getBetterPhoto(photos).photo);
                        }
                        break;
                    case TdApi.MessageVideo.CONSTRUCTOR:
                        if (((TdApi.MessageVideo) message).video.thumb.photo.path.length() == 0) {
                            downloadFile(((TdApi.MessageVideo) message).video.thumb.photo);
                        }
                        break;
                }
            }
        }
    }

    private void downloadFile(TdApi.File file) {
        client.send(new TdApi.DownloadFile(file.id), downloadFile);
        size += file.size;
    }

    public int getPhotoIdByUser(TdApi.User user) {
        return user.profilePhoto.small.id;
    }

    public void sendMessage(final TdApi.InputMessageContent message) {
        if (isFirstTime) {
            client.send(new TdApi.CreatePrivateChat(users.get(1).id), new Client.ResultHandler() {
                @Override
                public void onResult(final TdApi.TLObject object) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("CreatePrivateChat", object.toString());
                            if (object instanceof TdApi.Chat) {
                                isFirstTime = false;
                                Log.d("BotStart", "0" + users.get(1).toString());
                                if (users.get(1).type instanceof TdApi.UserTypeBot) {
                                    getView().findViewById(R.id.bot_first_start).setVisibility(View.GONE);
                                    Log.d("BotStart", "1");
                                }

                                Log.d("BotStart", "2");
                                sendMessage(message);
                            }
                        }
                    });
                }
            });
        } else {
            int replyId = 0;
            if (getView().findViewById(R.id.reply).getVisibility() == View.VISIBLE) {
                getView().findViewById(R.id.reply).setVisibility(View.GONE);
                replyId = chat.replyMarkupMessageId;
            }
            client.send(new TdApi.SendMessage(chatId, replyId, false, new TdApi.ReplyMarkupForceReply(true), message), sendMessage);
        }
    }

    public void getChatUsers() {
        if (chatId >= 0) {
            if (!isFirstTime) {
                users.add(((TdApi.PrivateChatInfo) chat.type).user);
            } else {
                listView.setEmptyView(emptyView);
            }
            if (users.get(1).type instanceof TdApi.UserTypeBot) {
                hasBot = true;
                slash.setVisibility(View.VISIBLE);
            }
            updateToolbar();
            getChatHistory();
            if (users.get(1).type instanceof TdApi.UserTypeBot) {
                client.send(new TdApi.GetUserFull(users.get(1).id), getUserFull);
            }
        } else {
            client.send(new TdApi.GetGroupChatFull((int) chatId), getChatUsers);
        }
    }

    public void getChatHistory() {
        if (!isDownloadedAllChats && !isLoading) {
            isLoading = true;
            Log.d("getChatHistory", "chatId = " + chatId + " userId = "+ userId);
            client.send(new TdApi. GetChatHistory(chatId,
                    userId, offset, limit), getChatHistory);
        }
    }

    public void updateFile(TdApi.UpdateFile file) {
        if (chat != null) {
            if (file.file.id == TGFunc.getPhotoIdByChat(chat)) {
                TGFunc.setChatPhoto(chat, file);
                updateToolbar();
            }
        }

        updateFileInSharedMedia(file);
        updateStickerThumbs(file);

        int index = TGFunc.getMessagePosAndUpdateByFile(messages, file);
        if (index == -1) return;
        updateListItem(index);
    }

    public void updateFileProgress(TdApi.UpdateFileProgress file) {
        int index = TGFunc.getMessagePosByFileId(getActivity(), messages, file);
        if (index == -1) return;
        if (file.size != 0) {
            float progress = file.ready * 100 / file.size;
            progresses.set(index, progress);
            final LinearLayout view = (LinearLayout) listView.getChildAt(index - listView.getFirstVisiblePosition());
            if (view == null) return;
            CircleProgressBar progressBar = (CircleProgressBar) view.findViewById(R.id.progressBar);
            if (progressBar == null) return;
            progressBar.setProgress(progress);
        }
    }

    public void updateVoiceProgress() {
        TdApi.UpdateFile file = new TdApi.UpdateFile(new TdApi.File(MediaController.getInstance().audioId, "", 0, ""));
        int index = TGFunc.getMessagePosByFileId(getActivity(), messages, file);
        if (index == -1) return;

        final LinearLayout view = (LinearLayout) listView.getChildAt(index - listView.getFirstVisiblePosition());
        if (view == null) return;
        SeekBar seekBar = (SeekBar) view.findViewById(R.id.seekBar);
        if (seekBar == null) return;
        seekBar.setProgress((int) (MediaController.getInstance().audioProgress * 100));
    }

    public void updateListView() {
        listView.invalidateViews();
    }

    private void updateStickerThumbs(TdApi.UpdateFile object) {
        if (stickerSets.size() == 0) return;
        for (TdApi.StickerSet stickerSet : stickerSets) {
            if (stickerSet.stickers != null) //break;
            for (TdApi.Sticker sticker : stickerSet.stickers) {
                if (sticker.thumb.photo.id == object.file.id) {
                    sticker.thumb.photo = object.file;
                    if (views.size() == 7) {
                        View view = views.get(6);
                        if (view instanceof ListView) {
                            ((ListView) view).invalidateViews();
                            final HorizontalScrollView header2 = (HorizontalScrollView) getView().findViewById(R.id.header2);
                            if (header2 != null) {
                                header2.removeAllViews();
                                header2.addView(getStickerMenu());
                            }
                        }
                    }
                    break;
                }
            }
        }
    }

    private void updateFileInSharedMedia(TdApi.UpdateFile object) {
        GroupFragment groupFragment = (GroupFragment) getFragmentManager().findFragmentByTag("group");
        if (groupFragment != null) {
            groupFragment.updateFile(object);
        }
        ProfileFragment profileFragment = (ProfileFragment) getFragmentManager().findFragmentByTag("profile");
        if (profileFragment != null) {
            profileFragment.updateFile(object);
        }
    }

    private void updateListItem(final int index) {
        final LinearLayout view = (LinearLayout) listView.getChildAt(index - listView.getFirstVisiblePosition());
        if (view == null) {
            return;
        }

        TdApi.Message message = messages.get(index);
        int fromId = message.fromId;
        TdApi.User user = TGFunc.getUserById(users, fromId);

        if (user != null || message.message == null) {
            view.removeAllViews();
            if (messages.get(index).message instanceof TdApi.MessageDocument) {
                Log.d("updateListItem = ", "" + messages.get(index).message.toString());
            }

            view.addView(TGFunc.getMessageView(getActivity(), user, messages.get(index), progresses.get(index), false));
        } else {
            Client client = TG.getClientInstance();
            if (client != null)
            client.send(new TdApi.GetUser(fromId), new Client.ResultHandler() {
                @Override
                public void onResult(final TdApi.TLObject object) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            TdApi.User user = (TdApi.User) object;
                            users.add(user);
                            view.removeAllViews();
                            view.addView(TGFunc.getMessageView(getActivity(), user, messages.get(index), progresses.get(index), false));

                        }
                    });
                }
            });
        }

    }

    private void updateUserInfo(TdApi.TLObject object, TdApi.User user) {
        switch (object.getConstructor()) {
            case TdApi.UpdateUserStatus.CONSTRUCTOR:
                user.status = ((TdApi.UpdateUserStatus) object).status;
                break;
            case TdApi.UpdateUser.CONSTRUCTOR:
                TdApi.User updateUser = ((TdApi.UpdateUser) object).user;
                TGFunc.updateUser(user, updateUser);
                if (updateUser.profilePhoto.small.path.length() == 0) {
                    downloadFile(user.profilePhoto.small.id);
                }
                break;
        }
    }

    public void updateUser(TdApi.TLObject object) {
        int id = TGFunc.getUserId(object);
        int pos = TGFunc.getUserPosById(users, id);
        if (pos != -1) {
            updateUserInfo(object, users.get(pos));
            updateToolbar();
            if (listView == null || listView.getAdapter() == null) return;
            ((BaseAdapter)listView.getAdapter()).notifyDataSetChanged();
        }
    }

    int unreadCount;

    public void updateNewMessage(TdApi.TLObject object) {
        TdApi.Message message = ((TdApi.UpdateNewMessage) object).message;
        if (chatId != message.chatId) return;
        final TdApi.TLObject obj = message.message;
        TdApi.GroupChat groupChat = chat.type instanceof TdApi.GroupChatInfo ?
                ((TdApi.GroupChatInfo) chat.type).groupChat : null;
        switch (obj.getConstructor()) {
            case TdApi.MessageChatChangePhoto.CONSTRUCTOR:
                TdApi.Photo photo = ((TdApi.MessageChatChangePhoto) message.message).photo;
                TdApi.PhotoSize[] photos = photo.photos;
                for (int j = 0; j < photos.length; j++) {
                    if (photos[j].type.equals("a")) {
                        groupChat.photo.small = photos[j].photo;
                    }
                    if (photos[j].type.equals("b")) {
                        groupChat.photo.big = photos[j].photo;
                    }
                }
                downloadFile(groupChat.photo.small.id);
                break;
            case TdApi.MessageChatDeletePhoto.CONSTRUCTOR:
                if (chat.id == message.chatId) {
                    TdApi.File fileEmpty = new TdApi.File(0, "", 0, "");
                    groupChat.photo.small = fileEmpty;
                    groupChat.photo.big = fileEmpty;
                }
                break;
            case TdApi.MessageChatChangeTitle.CONSTRUCTOR:
                groupChat.title = ((TdApi.MessageChatChangeTitle) message.message).title;
                break;
        }
        chat.topMessage = message;

        changeReplyMarkup(message);

        boolean isBottom = false;

        Log.d("lastVP", ""+listView.getLastVisiblePosition()+" "+(messages.size()-1));
        if (userId != message.fromId) unreadCount++;

        if (messages.size() == 0 || (messages.size() > 0 && !TGFunc.getFormedDate(messages.get(messages.size()-1).date * 1000L).equals(TGFunc.getFormedDate(message.date * 1000L)))) {
            messages.add( new TdApi.Message(0, 0, 0, message.date, 0, 0, 0, null, null));
            progresses.add(0f);
        }

        messages.add(message);
        progresses.add(0f);
        if (messages.size()-2 != listView.getLastVisiblePosition()) {
            showUnreadPanel(unreadCount);
            if (unreadCount != 0 && floatingButton.getVisibility() == View.GONE) {
                floatingButton.setVisibility(View.VISIBLE);
                floatingButton.setTranslationY(350);
                floatingButton.animate().translationYBy(-350).setDuration(500);
            }
        } else {
            isBottom = true;
            unreadCount = 0;
            showUnreadPanel(unreadCount);
        }

        ((BaseAdapter)listView.getAdapter()).notifyDataSetChanged();
        updateToolbar();
        if (isBottom) {
            scrollToPosition(messages.size()-1);
        }

        client.send(new TdApi.GetChatHistory(chatId, userId, 0, 1), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                Log.d("NewUpdateRes", object.toString());
            }
        });

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        MessagesFragment fragment = (MessagesFragment) fragmentManager.findFragmentByTag("main");
        if (fragment == null) return;
        fragment.decUnreadCount(chatId);
    }

    private void updateDeleteMessages(TdApi.UpdateDeleteMessages object) {
        if (chatId != object.chatId) return;
        for (int id : object.messages) {
            for (int i = messages.size()-1; i >= 0; i--) {
                if (messages.get(i).id == id) {
                    messages.remove(i);
                }
            }
        }
        listView.invalidateViews();

        GroupFragment groupFragment = (GroupFragment) getFragmentManager().findFragmentByTag("group");
        if (groupFragment != null) {
            groupFragment.updateDeleteMessages(object);
        }
        ProfileFragment profileFragment = (ProfileFragment) getFragmentManager().findFragmentByTag("profile");
        if (profileFragment != null) {
            profileFragment.updateDeleteMessages(object);
        }
    }

    private void showUnreadPanel(int unreadCount) {
        removeUnreadPanel();
        if (unreadCount > 0) {
            if (messages.size() >= unreadCount) {
                TdApi.Message unread = new TdApi.Message();
                unread.forwardDate = unreadCount;
                int pos = messages.size() - unreadCount;
                messages.add(pos, unread);
                progresses.add(pos, 0f);
            }
        } else {
            floatingButton.setVisibility(View.GONE);
        }
    }

    private void removeUnreadPanel() {
        for (int i = messages.size()-1; i >= 0; i--) {
            if (messages.get(i).message == null && messages.get(i).date == 0) {
                messages.remove(i);
                progresses.remove(i);
            }
        }
    }

    private void updateToolbar() {
        toolbar.setTitle(isFirstTime ? users.get(1).firstName + " " + users.get(1).lastName : TGFunc.getChatTitle(chat));
        String path = "";
        TdApi.File file = null;
        Bitmap bitmap;
        if (isFirstTime || chat.type instanceof TdApi.PrivateChatInfo) {
            if (users.get(1).id == TGFunc.getUserId(getActivity())) {
                toolbar.setSubtitle(R.string.online);
            } else {
                boolean isTyping = false;
                for (Integer id : typings) {
                    for (TdApi.User user : users) {
                        if (id == user.id) {
                            isTyping = true;
                        }
                    }
                }
                if (isTyping) {
                    toolbar.setSubtitle(R.string.typing);
                } else {
                    toolbar.setSubtitle(users.get(1).type instanceof TdApi.UserTypeBot ?
                            getString(R.string.bot) : TGFunc.getUserStatus(users.get(1).status));
                }
            }
            file = users.get(1).profilePhoto.small;
        } else if (chat.type instanceof TdApi.GroupChatInfo) {
            String typing = "";
            for (Integer id : typings) {
                for (TdApi.User user : users) {
                    if (id == user.id) {
                        typing += user.firstName+", ";
                    }
                }
            }
            if (typing.length() > 0) {
                typing = typing.substring(0, typing.length()-2) +" "+ getString(R.string.is_typing);
                toolbar.setSubtitle(typing);
            } else {
                toolbar.setSubtitle(TGFunc.getChatStatus(users));
            }
            file = ((TdApi.GroupChatInfo)chat.type).groupChat.photo.small;
        }
        if (file != null && file.path.length() != 0) {
            path = file.path;
        }
        File image = new File(path);
        if (image.exists()) {
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);
            bitmap = Bitmap.createScaledBitmap(bitmap, Util.dp(40), Util.dp(40), true);
        } else {
            if (!isFirstTime) {
                bitmap = TGFunc.getChatPlaceholder(getActivity(), chat, Util.dp(40));
            } else {
                bitmap = TGFunc.getUserPlaceholder(getActivity(), users.get(1), Util.dp(40));
            }
        }
        if (bitmap == null) return;
        toolbar.setLogo(new BitmapDrawable(getResources(), CircleTransform.makeCircleBitmap(bitmap)));
    }

}
