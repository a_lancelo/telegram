package com.kand.telegram.ui.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.kand.telegram.logic.TGFunc;
import com.kand.telegram.logic.Util;

import org.drinkless.td.libcore.telegram.TdApi;

public class StickersAdapter extends BaseAdapter {

    private Context context;

    TdApi.Sticker[] stickers;

    public StickersAdapter(Context context, TdApi.Sticker[] stickers) {
        this.context = context;
        this.stickers = stickers;
    }

    @Override
    public int getCount() {
        return stickers.length;
    }

    @Override
    public Object getItem(int position) {
        return stickers[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(context);
        imageView.setLayoutParams(new GridView.LayoutParams(Util.dp(80), Util.dp(80)));
        int p = Util.dp(7);
        imageView.setPadding(p, p, p, p);
        TGFunc.loadSticker(stickers[position].thumb.photo.path, imageView);
        return imageView;
    }

}