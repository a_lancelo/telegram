package com.kand.telegram.ui.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import com.kand.telegram.logic.TGFunc;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;


public class ChatAdapter extends BaseAdapter {

    Context context;
    LayoutInflater lInflater;
    ArrayList<TdApi.Message> messages;
    ArrayList<Float> progresses = new ArrayList<Float>();
    ArrayList<TdApi.User> users;

    public ChatAdapter(Context context, ArrayList<TdApi.User> users, ArrayList<TdApi.Message> messages, ArrayList<Float> progresses) {
        this.context = context;
        this.users = users;
        this.messages = messages;
        this.progresses = progresses;
        lInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Object getItem(int position) {
        return messages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final LinearLayout view = new LinearLayout(context);
        final AbsListView.LayoutParams layoutParams = new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.MATCH_PARENT);
        view.setLayoutParams(layoutParams);

        TdApi.Message message = messages.get(position);

        int fromId = message.fromId;
        TdApi.User user = TGFunc.getUserById(users, fromId);

        if (user != null || message.message == null) {
            view.addView(TGFunc.getMessageView(context, user, message, progresses.get(position), false), layoutParams);
        } else {
            final TdApi.Message message1 = message;
            Client client = TG.getClientInstance();
            if (client != null)
                client.send(new TdApi.GetUser(fromId), new Client.ResultHandler() {
                    @Override
                    public void onResult(final TdApi.TLObject object) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Log.i("user info", object.toString());
                                TdApi.User user = (TdApi.User) object;
                                users.add(user);
                                view.addView(TGFunc.getMessageView(context, user, message1, progresses.get(position), false), layoutParams);
                            }
                        });
                }
            });
        }
        return view;
    }

}