package com.kand.telegram.ui;

import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.kand.telegram.R;
import com.kand.telegram.logic.Emoji;
import com.kand.telegram.logic.MusicPlayer;
import com.kand.telegram.logic.Util;
import com.kand.telegram.logic.TGFunc;
import com.kand.telegram.logic.TGService;

public class MainActivity extends AppCompatActivity {

    final String LOG_TAG = "2MainActivity";
    TGService service;
    ServiceConnection sConn;
    boolean bound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Util.context = this;
        Util.isTablet = getResources().getBoolean(R.bool.isTablet);
        TGFunc.context = this;
        MusicPlayer.context = this;
        Emoji.applicationContext = this;
        Emoji.density = getResources().getDisplayMetrics().density;

        setContentView(R.layout.main);

        final FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
                    MessagesFragment fragment = (MessagesFragment) fragmentManager.findFragmentByTag("messages");
                    if (fragment != null) fragment.updateMenu();
                    if (Util.isTablet) {
                        findViewById(R.id.container_layout_2).setVisibility(View.GONE);
                    } else {
                        DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
                        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                    }
                }
                if (getSupportFragmentManager().getBackStackEntryCount() == 1 && !Util.isTablet) {
                    DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                }
                //Toast.makeText(MainActivity.this, ""+getSupportFragmentManager().getBackStackEntryCount(), Toast.LENGTH_SHORT).show();
                Log.d("OnBackStackChCount", "" + getSupportFragmentManager().getBackStackEntryCount());
            }
        });

        Fragment fragment = TGFunc.isFirstTime() ? new PreviewAuthFragment(): new MessagesFragment();
        String tag = TGFunc.isFirstTime() ? "preview_auth" : "messages";
        int id = R.id.container;
        if (Util.isTablet) {
            if (TGFunc.isFirstTime()) {
                findViewById(R.id.container_layout_2).setVisibility(View.VISIBLE);
                //findViewById(R.id.image_for_auth).setVisibility(View.VISIBLE);
            } else {
                findViewById(R.id.container_layout_1).setVisibility(View.VISIBLE);
                findViewById(R.id.container_shadow).setVisibility(View.VISIBLE);
                id = R.id.container_left;
            }
            int leftWidth = getResources().getDisplayMetrics().widthPixels / 100 * 35;
            if (leftWidth < Util.dp(320)) {
                leftWidth = Util.dp(320);
            }
            findViewById(R.id.container_left).getLayoutParams().width = leftWidth;
            findViewById(R.id.container_shadow).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = getSupportFragmentManager().getBackStackEntryCount();
                    for (int i = count; i >= 0; i--) {
                        getSupportFragmentManager().popBackStack();
                    }
                }
            });
        }
        fragmentManager.beginTransaction().replace(id, fragment, tag).commit();
        Log.d("getDir", "" + Environment.getRootDirectory() + " " + Environment.getDataDirectory() + " "
                + Environment.getDownloadCacheDirectory());
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (bound) {
            service.isActive = false;
            unbindService(sConn);
        }
        bound = false;
        setIntent(new Intent());
        Log.d("Act", "onPause");
        if (Util.getPreferences().getBoolean("passcodeEnabled", false)) { //appLocked
            Util.getPreferences().edit().putLong("lock_lastActivity", System.currentTimeMillis()).commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(LOG_TAG, "onResume");
        ((NotificationManager) getSystemService(NOTIFICATION_SERVICE)).cancel(1);
        startService();

        if (getIntent().getBooleanExtra("openPlayer", false)) {
            if (Util.isTablet) {
                findViewById(R.id.container_layout_2).setVisibility(View.VISIBLE);
            }
            getSupportFragmentManager().beginTransaction().add(R.id.container, new MusicPlayerFragment(), "music_player").addToBackStack(null).commit();
        }

        if (Util.getPreferences().getBoolean("passcodeEnabled", false)) {
            if (getSupportFragmentManager().findFragmentByTag("passcode") != null) return;
            //locked = true;
            if (Util.getPreferences().getBoolean("appLocked", false)) {
                showPasscode();
                //getSupportFragmentManager().beginTransaction().add(R.id.container, new PasscodeFragment(), "passcode").addToBackStack(null).commit();
            } else {
                long time = 0;
                switch (Util.getPreferences().getInt("type_auto_lock", 2)) {
                    case 0:
                        time = 60;
                        break;
                    case 1:
                        time = 5*60;
                        break;
                    case 2:
                        time = 60*60;
                        break;
                    case 3:
                        time = 5*60*60;
                        break;
                    case 4:
                        return;
                }
                time *= 1000L;
                time += Util.getPreferences().getLong("lock_lastActivity", 0);
                Log.d(LOG_TAG, "onResume"+System.currentTimeMillis()+" "+time);

                if (System.currentTimeMillis() > time) {
                    showPasscode();
                    //getSupportFragmentManager().beginTransaction().add(R.id.container, new PasscodeFragment(), "passcode").addToBackStack(null).commit();
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        if (TGFunc.isFirstTime() && getService() != null) getService().stopSelf();
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                Log.d(LOG_TAG, "home");
                getSupportFragmentManager().popBackStack();
                TGFunc.hideSoftKeyboard(this);
                if (Util.isTablet && getSupportFragmentManager().getBackStackEntryCount() == 0) {
                    ((FrameLayout)findViewById(R.id.container_right)).removeAllViews();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void startService() {
        Intent intent = new Intent(this, TGService.class);
        sConn = new ServiceConnection() {
            public void onServiceConnected(ComponentName name, IBinder binder) {
                Log.d(LOG_TAG, "MainActivity onServiceConnected");
                service = ((TGService.MyBinder) binder).getService();
                bound = true;
            }

            public void onServiceDisconnected(ComponentName name) {
                Log.d(LOG_TAG, "MainActivity onServiceDisconnected");
                bound = false;
            }
        };
        startService(intent);
        bindService(intent, sConn, BIND_AUTO_CREATE);
    }

    public TGService getService() {
        if (service == null) Log.d("getService app null", "");
        if (bound) {
            return service;
        } else {
            return null;
        }
    }

    private void showPasscode() {
        if (Util.isTablet) {
            findViewById(R.id.container_fullscreen).setVisibility(View.VISIBLE);
            getSupportFragmentManager().beginTransaction().add(R.id.container_fullscreen, new PasscodeFragment(), "passcode").addToBackStack(null).commit();
        } else {
            getSupportFragmentManager().beginTransaction().add(R.id.container, new PasscodeFragment(), "passcode").addToBackStack(null).commit();
        }
    }

}
