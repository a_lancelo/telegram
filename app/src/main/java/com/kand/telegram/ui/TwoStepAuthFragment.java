package com.kand.telegram.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.kand.telegram.R;
import com.kand.telegram.logic.TGFunc;
import com.kand.telegram.logic.Util;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

public class TwoStepAuthFragment extends Fragment {

    String hint;
    EditText code;
    private boolean isLoading = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.two_step_auth, container, false);
        TGFunc.setToolbar(getActivity(), rootView, getString(R.string.password), true);
        InputMethodManager imgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imgr.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        code = (EditText) rootView.findViewById(R.id.code);
        code.setHint(hint);
        code.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    enterCode();
                    return true;
                }
                return false;
            }
        });
        setHasOptionsMenu(true);
        return rootView;
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.auth_menu, menu);
        menu.getItem(isLoading ? 1 : 0).setVisible(true);
        menu.getItem(isLoading ? 0 : 1).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_check:
                enterCode();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    private void enterCode() {
        if (code.getText().toString().length() == 0) return;
        isLoading = true;
        getActivity().invalidateOptionsMenu();
        Client client = TG.getClientInstance();
        if (client == null) return;
        client.send(new TdApi.CheckAuthPassword(code.getText().toString()), new Client.ResultHandler() {
            @Override
            public void onResult(final TdApi.TLObject object) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("TwoStep CheckAuth", object.toString());
                        //dialog.cancel();
                        if (object instanceof TdApi.AuthStateOk) {
                            hideSoftKeyboard(getActivity());

                            getFragmentManager().popBackStack();
                            getFragmentManager().popBackStack();
                            getFragmentManager().popBackStack();
                            //getFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, 0, 0, R.anim.exit_to_right).replace(R.id.container, new MessagesFragment(), "messages").commit();
                            showMessages();
                        } else {
                            code.setText("");
                            new AlertDialog.Builder(getActivity())
                                    .setTitle(R.string.app_name)
                                    .setPositiveButton(R.string.ok, null)
                                    .setMessage(R.string.wrong_paasword)
                                    .show();
                            code.setText("");
                            isLoading = false;
                            getActivity().invalidateOptionsMenu();
                        }
                    }
                });

            }
        });
    }

    private void showMessages() {
        if (Util.isTablet) {
            getActivity().findViewById(R.id.container_layout_2).setVisibility(View.GONE);
            getActivity().findViewById(R.id.container_layout_1).setVisibility(View.VISIBLE);
            getActivity().findViewById(R.id.container_shadow).setVisibility(View.VISIBLE);
            getFragmentManager().beginTransaction().replace(R.id.container_left, new MessagesFragment(), "messages").commit();
        } else {
            getFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, 0, 0, R.anim.exit_to_right).replace(R.id.container, new MessagesFragment(), "messages").commit();
        }
    }

}
