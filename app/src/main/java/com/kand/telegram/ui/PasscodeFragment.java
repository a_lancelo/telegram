package com.kand.telegram.ui;

import android.app.Activity;
import android.content.Context;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.Prediction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.kand.telegram.R;
import com.kand.telegram.logic.Util;
import com.kand.telegram.logic.PatternView;
import com.kand.telegram.logic.TGFunc;
import com.kand.telegram.ui.adapters.PinKeyboardAdapter;
import com.kand.telegram.ui.adapters.SpinnerAdapter;

import java.io.File;
import java.util.ArrayList;

public class PasscodeFragment extends Fragment {

    private int type;
    private String pass = "", savedPass = "";
    private boolean checkPassMode = true, checkNewPassMode = false, onResumeApp = false;
    private static GestureLibrary sStore;

    View.OnKeyListener backListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            Log.d("onKeyView", "KEYCODE = "+keyCode + " action = "+ event.getAction());
            if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                Log.d("onKeyView", "KEYCODE_BACK");
                if (onResumeApp) {
                    getActivity().finish();
                } else {
                    getFragmentManager().popBackStack();
                }
                return true;
            }
            return false;
        }
    };

    AdapterView.OnItemClickListener pinKeyboardListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (position == 11) {
                if (pass.length() != 0) pass = pass.substring(0, pass.length()-1);
            } else {
                pass += position < 9 ? ""+(position+1) : (position == 10 ? "0" : "");
            }
            ((EditText) getView().findViewById(R.id.editText)).setText(pass);
        }
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.passcode, container, false);
        type = Util.getPreferences().getInt("passcode_type", 0);
        if (getArguments() != null) {
            TGFunc.setToolbar(getActivity(), rootView, getString(R.string.passcode_lock), true);
            checkPassMode = !getArguments().getBoolean("setNew", false);
        } else {
            checkPassMode = true;
            //((MainActivity)getActivity()).locked = true;
            Util.getPreferences().edit().putBoolean("appLocked", true).commit();
            onResumeApp = true;
            rootView.findViewById(R.id.toolbar).setVisibility(View.GONE);
        }
        if (!checkPassMode) {
            try {
                ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
                ((Spinner)rootView.findViewById(R.id.spinner)).setAdapter(new SpinnerAdapter(getActivity(), getResources().getStringArray(R.array.types_passcode)));
                ((Spinner)rootView.findViewById(R.id.spinner)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        pass = "";
                        mGesture = null;

                        type = position;
                        showPassLock(getView(), type);
                    }
                    @Override public void onNothingSelected(AdapterView<?> parent) {}
                });
            } catch (Exception e) { Log.d("passcode", e.toString());}
            setHasOptionsMenu(true);
        }
        showPassLock(rootView, type);
        return rootView;
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.auth_menu, menu);
        //if () menu
        Log.d("fragment.onCreateOpt", "Passcode");
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_check:
                Log.d("onOptionsItemSelected", "" + getFragmentManager().getBackStackEntryCount());
                Log.d("checkPassMode", "" + checkPassMode);
                if (type != 2 || (type == 2 && !checkNewPassMode && !checkPassMode)) {
                    onEnterEnd();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(backListener);

        Log.d("fragment.onResume", "Passcode");
    }

    private void showPassLock(View rootView, final int type) {
        if ((checkPassMode && !onResumeApp) || checkNewPassMode) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(true);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getResources().getStringArray(R.array.types_passcode)[type]);
        }
        hideSoftKeyboard(getActivity());
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout view = (LinearLayout) rootView.findViewById(R.id.linearLayout);
        view.removeAllViews();
        View inflateView = null;
        switch (type) {
            case 0:
                inflateView = inflater.inflate(R.layout.passcode_pin, null);
                GridView gridView = (GridView) inflateView.findViewById(R.id.gridView);
                gridView.setAdapter(new PinKeyboardAdapter(getActivity()));
                gridView.setOnItemClickListener(pinKeyboardListener);
                view.addView(inflateView, view.getLayoutParams());
                ((EditText) inflateView.findViewById(R.id.editText)).addTextChangedListener(new TextWatcher() {
                    public void afterTextChanged(Editable s) {
                        if (s.length() == 4) {
                            onEnterEnd();
                        }
                    }

                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                    public void onTextChanged(CharSequence s, int start, int before, int count) {}
                });
                break;
            case 1:
                inflateView = inflater.inflate(R.layout.passcode_password, null);
                EditText editText = (EditText) inflateView.findViewById(R.id.editText);
                editText.setOnKeyListener(backListener);
                if (checkPassMode || checkNewPassMode) {
                    ((EditText) inflateView.findViewById(R.id.editText)).addTextChangedListener(new TextWatcher() {
                        public void afterTextChanged(Editable s) {
                            onEnterEnd();
                        }
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                        public void onTextChanged(CharSequence s, int start, int before, int count) {}
                    });
                }
                view.addView(inflateView, view.getLayoutParams());
                showSoftKeyboard(getActivity());
                break;
            case 2:
                inflateView = inflater.inflate(R.layout.passcode_gesture, null);
                GestureOverlayView overlay = (GestureOverlayView) inflateView.findViewById(R.id.gestures_overlay);
                if (checkPassMode || checkNewPassMode) {
                    overlay.addOnGesturePerformedListener(performedListener);
                } else {
                    overlay.addOnGestureListener(new GesturesProcessor());
                }
                view.addView(inflateView, view.getLayoutParams());
                break;
            case 3:
                inflateView = inflater.inflate(R.layout.passcode_pattern, null);
                PatternView patternView = (PatternView) inflateView.findViewById(R.id.patternView);
                patternView.setOnEnterPatternListener(new PatternView.OnEnterPatternListener() {
                    @Override
                    public void onStart() {
                        pass = "";
                    }
                    @Override
                    public void onEnter(String pass) {
                        PasscodeFragment.this.pass = pass;
                        if (checkPassMode || checkNewPassMode) {
                            onEnterEnd();
                        }
                    }
                });
                view.addView(inflateView, view.getLayoutParams());
                break;
        }
        if (checkPassMode && (onResumeApp || (type != 0 && type != 1))) inflateView.findViewById(R.id.logo).setVisibility(View.VISIBLE);

        ((TextView)inflateView.findViewById(R.id.textView)).setText(
                getActivity().getResources().getStringArray(checkPassMode ? R.array.types_lock_check_pass :
                        (checkNewPassMode ? R.array.types_lock_check_new_pass : R.array.types_lock_new_pass))[type]);
    }

    private void onEnterEnd() {
        if (checkPassMode) {
            if (type == 0 || type == 1) {
                pass = ((EditText) getView().findViewById(R.id.editText)).getText().toString();
            }
            if (type != 2) {
                String password = Util.getPreferences().getString("password", "");
                Log.d("pass", pass + " " + password);
                if (!password.equals(pass)) return;
            }
            sendResultOK();
        } else {
            if (checkNewPassMode) {
                if (type == 0 || type == 1) {
                  pass = ((EditText) getView().findViewById(R.id.editText)).getText().toString();
                }
                if (type != 2 && !savedPass.equals(pass)) return;
                Util.getPreferences().edit().putString("password", savedPass).putInt("passcode_type", type).
                        putBoolean("passcodeEnabled", true).commit();
                sendResultOK();
            } else {
                if (type == 0 || type == 1) {
                    pass = ((EditText) getView().findViewById(R.id.editText)).getText().toString();
                }
                if ((pass.length() == 0 && type != 2) || (mGesture == null && type == 2)) return;
                if (type == 2) addGesture();
                getView().findViewById(R.id.spinner).setVisibility(View.GONE);
                savedPass = pass;
                pass = "";
                checkNewPassMode = true;
                showPassLock(getView(), type);
            }
        }
    }

    private void sendResultOK() {
        hideSoftKeyboard(getActivity());
        getFragmentManager().popBackStack();

        Fragment fragment = getFragmentManager().findFragmentByTag("passcode_settings");
        if (fragment == null) {
            fragment = getFragmentManager().findFragmentByTag("settings");
        }
        if (fragment != null) {
            Log.d("onActivityResult", "1");
            fragment.onActivityResult(110, Activity.RESULT_OK, null);
        }

        if (onResumeApp) {
            Util.getPreferences().edit().putBoolean("appLocked", false).commit();
            MessagesFragment fragment1 = (MessagesFragment) getFragmentManager().findFragmentByTag("settings");
            if (fragment1 != null) fragment1.updateMenu();
        }

    }

    static GestureLibrary getStore() {
        final File mStoreFile = new File(TGFunc.getDir(), "gestures"); //new File(Environment.getExternalStorageDirectory(), "gestures");
        if (sStore == null) {
            sStore = GestureLibraries.fromFile(mStoreFile);
        }
        return sStore;
    }

    static void clearStore() {
        new File(TGFunc.getDir(), "gestures").delete();
        sStore = null;
    }

    public void addGesture() {
        if (mGesture != null) {
            clearStore();
            final String name = "gest";
            final GestureLibrary store = getStore();
            store.addGesture(name, mGesture);
            store.save();
        }
    }

    GestureOverlayView.OnGesturePerformedListener performedListener = new GestureOverlayView.OnGesturePerformedListener() {
        @Override
        public void onGesturePerformed(GestureOverlayView overlay, Gesture gesture) {
            for (String st : getStore().getGestureEntries()) {
                Log.d("pre store", st);
            }
            Log.d("redictions.size() all =", ""+getStore().getGestureEntries().size());
            if (getStore().getGestureEntries().size() == 0) onEnterEnd();

            ArrayList<Prediction> predictions = getStore().recognize(gesture);
            Log.d("predictions.size() = ", "" + predictions.size());
            if (predictions.size() > 0) {
                Prediction prediction = predictions.get(0);
                Log.d("prediction.score = ", ""+prediction.score);
                if (prediction.score > 1.0) {
                    Log.d("prediction.name = ", prediction.name);
                    //unlock();
                    onEnterEnd();
                }
            }
        }
    };

    public static void showSoftKeyboard(Activity activity) {
        if (activity == null) return;
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public static void hideSoftKeyboard(Activity activity) {
        if (activity == null || activity.getCurrentFocus() == null) return;
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    private static final float LENGTH_THRESHOLD = 120.0f;

    private Gesture mGesture;

    private class GesturesProcessor implements GestureOverlayView.OnGestureListener {
        public void onGestureStarted(GestureOverlayView overlay, MotionEvent event) {
            mGesture = null;
        }

        public void onGesture(GestureOverlayView overlay, MotionEvent event) {
        }

        public void onGestureEnded(GestureOverlayView overlay, MotionEvent event) {
            mGesture = overlay.getGesture();
            if (mGesture.getLength() < LENGTH_THRESHOLD) {
                overlay.clear(false);
            }
        }

        public void onGestureCancelled(GestureOverlayView overlay, MotionEvent event) {
        }
    }

}
