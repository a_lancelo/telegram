package com.kand.telegram.ui;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaMetadataRetriever;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.kand.telegram.R;
import com.kand.telegram.logic.CircleProgressBar;
import com.kand.telegram.logic.MusicPlayer;
import com.kand.telegram.logic.SwipeView;
import com.kand.telegram.logic.TGFunc;

public class MusicPlayerFragment extends Fragment {

    private final long mFrequency = 100;
    private final int TICK_WHAT = 2;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message m) {
            updatePlayer();
            sendMessageDelayed(Message.obtain(this, TICK_WHAT), mFrequency);
        }
    };

    private TextView title, performer, current, duration;
    private ImageView cover, play, pause, download, download_pause, repeat, shuffle;
    View shadow_blue;
    private SeekBar seekBar;
    private CircleProgressBar progressBar;

    private boolean progressChange, coverAdded, coverExist;
    MusicPlayer service;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle(R.string.messages);
        View rootView = inflater.inflate(R.layout.music_player, container, false);
        TGFunc.setToolbar(getActivity(), rootView, "", true);

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_back_grey);

        service = MusicPlayer.getInstance();

        cover = (ImageView) rootView.findViewById(R.id.cover);
        progressBar = (CircleProgressBar) rootView.findViewById(R.id.progressBar);

        play = (ImageView) rootView.findViewById(R.id.play);
        pause = (ImageView) rootView.findViewById(R.id.pause);
        download = (ImageView) rootView.findViewById(R.id.download);
        download_pause = (ImageView) rootView.findViewById(R.id.download_pause);
        repeat = (ImageView) rootView.findViewById(R.id.repeat);
        shuffle = (ImageView) rootView.findViewById(R.id.shuffle);

        shadow_blue = rootView.findViewById(R.id.shadow_blue);

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("click", "arrow");
                pause.setVisibility(View.VISIBLE);
                play.setVisibility(View.GONE);
                service.playAudio();
            }
        });

        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("click", "pause");
                pause.setVisibility(View.GONE);
                play.setVisibility(View.VISIBLE);
                service.pauseAudio();
            }
        });

        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("click", "arrow");
                download_pause.setVisibility(View.VISIBLE);
                download.setVisibility(View.GONE);
                TGFunc.downloadFile(service.getFileId());
            }
        });

        download_pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("click", "pause");
                download_pause.setVisibility(View.GONE);
                download.setVisibility(View.VISIBLE);
                service.progress = 0;
                TGFunc.cancelDownloadFile(service.getFileId());
            }
        });

        rootView.findViewById(R.id.previous).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                service.previous();
                setEmptyCover();
            }
        });

        rootView.findViewById(R.id.next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                service.next();
                setEmptyCover();
            }
        });

        repeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                service.changeRepeatMode();
            }
        });

        shuffle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                service.changeShuffle();
            }
        });

        title = (TextView) rootView.findViewById(R.id.title);
        performer = (TextView) rootView.findViewById(R.id.performer);
        current = (TextView) rootView.findViewById(R.id.current);
        duration = (TextView) rootView.findViewById(R.id.duration);
        seekBar = (SeekBar) rootView.findViewById(R.id.seekBar);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Log.d("seekBar", "onProgressChanged fromUser = " + fromUser);
                if (fromUser) {
                    service.seekAudio(progress);
                    current.setText(service.getPlayerProgress());
                    progressChange = false;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                Log.d("seekBar", "onStartTrackingTouch");
                progressChange = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Log.d("seekBar", "onStopTrackingTouch");

            }
        });

        mHandler.sendMessageDelayed(Message.obtain(mHandler, TICK_WHAT), 0);
        setHasOptionsMenu(true);
        return new SwipeView(rootView);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(coverExist ? R.menu.music_player_white : R.menu.music_player_grey, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.playlist:
                getFragmentManager().beginTransaction().setCustomAnimations(R.anim.enter_from_right, 0, 0, R.anim.exit_to_right)
                .add(R.id.container, new MusicPlaylistFragment(), "musicPlaylist").addToBackStack(null).commit();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updatePlayer() {
        if (getActivity() == null) return;
        //TGService service = ((MainActivity) getActivity()).getService();
        //if (service == null) return;
        if (title == null) return;
        title.setText(service.audioTitle);
        performer.setText(service.audioPerformer);
        duration.setText(service.getPlayerDuration());
        Log.d("getPlayerPercent = ", "" + service.getPlayerPercentProgress());
        if (!progressChange) {
            current.setText(service.getPlayerProgress());
            seekBar.setProgress(service.getPlayerPercentProgress());
        }

        if (service.isDownload) {
            if (service.progress == 0) {
                download.setVisibility(View.VISIBLE);
                download_pause.setVisibility(View.GONE);
                progressBar.setColor(Color.TRANSPARENT);
            } else {
                download.setVisibility(View.GONE);
                download_pause.setVisibility(View.VISIBLE);
                progressBar.setColor(Color.parseColor("#569ace"));
            }
            progressBar.setProgress(service.progress);
            shadow_blue.setVisibility(View.GONE);
            pause.setVisibility(View.GONE);
            play.setVisibility(View.GONE);
        } else {
            if (service.isPlaying()) {
                pause.setVisibility(View.VISIBLE);
                play.setVisibility(View.GONE);
            } else {
                pause.setVisibility(View.GONE);
                play.setVisibility(View.VISIBLE);
            }
            shadow_blue.setVisibility(View.VISIBLE);
        }

        shuffle.setImageResource(service.isShuffle ? R.drawable.ic_shuffle_blue :
                (coverExist ? R.drawable.ic_shuffle_white : R.drawable.ic_shuffle_grey));
        repeat.setImageResource(service.isRepeat ? R.drawable.ic_repeat_blue :
                (coverExist ? R.drawable.ic_repeat_white : R.drawable.ic_repeat_grey));

        if (!coverAdded && service.audioPath != null && service.audioPath.length() != 0) setImage(service.audioPath);
    }

    private void setImage(String path) {
        coverAdded = true;
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(path);
        byte[] data = mmr.getEmbeddedPicture();
        if (data == null) return;
        Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
        cover.setImageBitmap(bitmap);
        coverExist = true;

        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_back);
        getActivity().invalidateOptionsMenu();
    }

    public void setEmptyCover() {
        coverAdded = false;
        coverExist = false;
        cover.setImageResource(R.drawable.ic_nocover);
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_back_grey);
        getActivity().invalidateOptionsMenu();
    }

}
