package com.kand.telegram.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kand.telegram.R;
import com.kand.telegram.logic.TGFunc;
import com.squareup.picasso.Picasso;

import org.drinkless.td.libcore.telegram.TdApi;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class AttachmentFragment extends Fragment {

    ArrayList<String> links = new ArrayList<String>();
    ArrayList<Boolean> states = new ArrayList<Boolean>();
    int count = 0;

    View.OnClickListener lastPhoto = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!states.get((int)v.getTag())) {
                v.findViewById(R.id.view).setVisibility(View.VISIBLE);
                v.findViewById(R.id.check).setVisibility(View.VISIBLE);
                count++;
            } else {
                v.findViewById(R.id.view).setVisibility(View.GONE);
                v.findViewById(R.id.check).setVisibility(View.GONE);
                count--;
            }
            states.set((int)v.getTag(), !states.get((int)v.getTag()));
            Log.d("tag lp", ""+v.getTag());
            TextView textView = (TextView) getView().findViewById(R.id.choose_from_text);
            if (count == 0) {
                textView.setText(R.string.choose_from);
            } else {
                textView.setText(getString(R.string.send)+" "+count+" "+getString(R.string.photos));
            }
        }
    };

    View.OnClickListener takePhoto = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException ex) {}
                if (photoFile != null) {
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                            Uri.fromFile(photoFile));
                    startActivityForResult(takePictureIntent, 2);
                }
            }
        }
    };

    View.OnClickListener choose_from = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (count == 0) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,
                        "Select Picture"), 3);
            } else {
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                ChatFragment fragment = (ChatFragment) fragmentManager.findFragmentByTag("chat");
                if (fragment == null) return;

                for (int i = 0; i < links.size(); i++) {
                    if (states.get(i)) {
                        String path = links.get(i);
                        Log.d("attach path", path);
                        TdApi.InputMessagePhoto message = new TdApi.InputMessagePhoto(new TdApi.InputFileLocal(path), "");
                        fragment.sendMessage(message);
                    }
                }

                if (fragmentManager.findFragmentByTag("attach") != null)
                    fragmentManager.popBackStack();
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.attach, container, false);
        rootView.findViewById(R.id.take_photo).setOnClickListener(takePhoto);
        rootView.findViewById(R.id.choose_from).setOnClickListener(choose_from);
        showLastImages(rootView);
        return rootView;
    }


    String mCurrentPhotoPath;

    private File createImageFile() throws IOException {
        String imageFileName = ".photo_for_send.jpg";
        File image = new File(TGFunc.getDir()+imageFileName);
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        Log.d("attach path", mCurrentPhotoPath);
        return image;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("attach res", ""+resultCode);
        if (resultCode == Activity.RESULT_OK) {
            Log.d("attach req", ""+requestCode);
            if (requestCode == 2 || requestCode == 3) {
                Log.d("attach req2", ""+requestCode);

                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                ChatFragment fragment = (ChatFragment) fragmentManager.findFragmentByTag("chat");
                if (fragment == null) return;
                String path = requestCode == 2 ? TGFunc.getDir()+".photo_for_send.jpg": getPath(data.getData());

                Log.d("attach path", ""+path);
                //??TdApi.InputMessagePhoto message = new TdApi.InputMessagePhoto(path);
                //??fragment.sendMessage(message);
                TdApi.InputMessagePhoto message = new TdApi.InputMessagePhoto(new TdApi.InputFileLocal(path), "");
                fragment.sendMessage(message);

                if (fragmentManager.findFragmentByTag("attach") != null)
                    fragmentManager.popBackStack();
            }
        }
    }

    public String getPath(Uri uri) {
        if( uri == null ) {
            return null;
        }
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
        if( cursor != null ){
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }

    public void showLastImages(View rootView) {
        String[] projection = new String[]{
                MediaStore.Images.ImageColumns._ID,
                MediaStore.Images.ImageColumns.DATA,
                MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME,
                MediaStore.Images.ImageColumns.DATE_TAKEN,
                MediaStore.Images.ImageColumns.MIME_TYPE
        };
        final Cursor cursor = getActivity().getContentResolver()
                .query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null,
                        null, MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC");

        if (cursor.moveToFirst()) {
            LinearLayout linearLayout = (LinearLayout) rootView.findViewById(R.id.lastImages);
            int count = 0;
            while(!cursor.isAfterLast() && count != 40) {
                LayoutInflater lInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                RelativeLayout relativeLayout = (RelativeLayout) lInflater.inflate(R.layout.attach_item, null);

                relativeLayout.setTag(count);
                relativeLayout.setOnClickListener(lastPhoto);

                final ImageView imageView = (ImageView) relativeLayout.findViewById(R.id.imageView);
                imageView.setAdjustViewBounds(true);
                String imageLocation = cursor.getString(1);

                links.add(imageLocation);
                states.add(false);

                Log.d("imageLocation", imageLocation);
                TGFunc.loadSticker(imageLocation, imageView);

                Picasso.with(getActivity()).load(new File(imageLocation))
                        .resize(82, 82)
                        .centerCrop()
                        .noPlaceholder()
                        .into(imageView);
                linearLayout.addView(relativeLayout);
                cursor.moveToNext();
                count++;
            }
        }

    }

}
