package com.kand.telegram.logic;

import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.kand.telegram.R;
import com.squareup.picasso.Picasso;

import org.drinkless.td.libcore.telegram.TdApi;

import java.io.File;
import java.util.ArrayList;

public class ImagesView extends FrameLayout {

    private int pos;
    private float fx, fy, cx, cy, scale = 1.0f, pScale, fDiffZoom, smx, smy;
    private boolean viewMoving, slideToRight, isAnimated, isSliding, isMoving, isZooming;
    private Context context;
    //private Bitmap bitmap;
    private ArrayList<TdApi.Message> messages;
    private ImageView imageView1, imageView2, imageView3;

    public ImagesView(Context context, ArrayList<TdApi.Message> messages, int pos) {
        super(context);
        this.context = context;
        this.messages = messages;
        this.pos = pos;
        imageView1 = new ImageView(context);
        imageView2 = new ImageView(context);
        imageView3 = new ImageView(context);
        setBackgroundColor(Color.parseColor("#000000"));
        /*imageView1.setBackgroundColor(Color.parseColor("#000000"));
        imageView2.setBackgroundColor(Color.parseColor("#000000"));
        imageView3.setBackgroundColor(Color.parseColor("#000000"));*/
        LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParams.gravity = Gravity.CENTER;
        setLayoutParams(layoutParams);
        imageView1.setLayoutParams(layoutParams);
        imageView2.setLayoutParams(layoutParams);
        imageView3.setLayoutParams(layoutParams);
        addView(imageView3);
        addView(imageView2);
        addView(imageView1);
        //imageView1.setVisibility(GONE);

        Point size = new Point();
        ((Activity)context).getWindowManager().getDefaultDisplay().getSize(size);
        int w = size.x;
        imageView1.setX(-w);
        //imageView1.setAlpha(255);
        //imageView3.setAlpha(255);
        imageView3.setAlpha(0);
        Log.d("ImagesViewW", "" + w + " " + imageView1.getX()+" "+imageView2.getAlpha());
        //imageView2.setAlpha(255);
        Log.d("SizeMes", "" + messages.size());
        loadImages();
    }

    public ImagesView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (isAnimated) return false;
        int dx = (int)(event.getX() - fx);//Math.max(0, (int) (event.getX() - fx));
        Log.d("ImagesView", "" + event.getX() + " " + event.getY() + " " + event.getAction() + " " + viewMoving);
        Log.d("ImagesView 2", "" + event.getAction() + " " + viewMoving + " " + dx + " " + event.getPointerCount());
        //checkDoubleTap(event);
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN:
                fx = event.getX();
                fy = event.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                cx = event.getX();
                viewMoving = true;

                if (event.getPointerCount() == 2 && !isSliding) {// && imageView2.getX() == 0) {// Math.abs(cx-fx) < Util.dp(20)) {
                    if (!isZooming) {
                        isZooming = true;
                        isMoving = false;
                        pScale = scale;
                        fDiffZoom = event.getX(0) - event.getX(1);
                        if (fDiffZoom < 0) fDiffZoom = -fDiffZoom;
                    }
                }

                if (!isZooming) {
                    if (scale > 1) {
                        isMoving = true;
                    }
                    if (scale == 1) {
                        isMoving = false;
                    }
                }

                if (isMoving) {
                    boolean slideToRight = event.getX() < fx;
                    boolean slideToTop = event.getY() > fy;
                    Bitmap bitmap = ((BitmapDrawable)imageView2.getDrawable()).getBitmap();
                    //Log.d("moc slide", "" + slideToRight + " " + imageView2.getWidth()+" "+imageView2.getHeight());
                    //Log.d("moc slide b", "" + slideToRight + " " + bitmap.getWidth()+" "+bitmap.getHeight());
                    Log.d("moc slide b", "" + slideToRight + " " + getBitmapWidth()+" "+getBitmapHeight());

                    if ((imageView2.getX() > 0 && slideToRight) || (imageView2.getX() + getMeasuredWidth() < getBitmapWidth() && !slideToRight)) {
                        imageView2.setX(smx + event.getX() - fx);
                    } else {
                        isMoving = false;
                    }
                    Log.d("moc slideT", "" + slideToTop);// + " " + imageView2.getWidth()+" "+imageView2.getHeight()+" "+);
                    if (bitmap != null && bitmap.getHeight() > getMeasuredHeight()) {
                        if ((imageView2.getY() > 0 && slideToTop) || (imageView2.getY() + getMeasuredHeight() < getBitmapHeight() && !slideToTop)) {
                            //imageView2.setX(smx + event.getX() - fx);
                            imageView2.setY(smy + event.getY() - fy);
                        }
                    }
                    Log.d("moc", "" +imageView2.getMeasuredWidth()+" "+imageView2.getMeasuredHeight() + " "+(event.getX() - fx) + " " + imageView2.getX() + " " + (event.getY() - fy) + " " + imageView2.getY());
                }

                Log.d("sizeBit", "" + getBitmapWidth()+" "+getBitmapHeight() + " " +isMoving);

                if (isZooming && event.getPointerCount() == 2) {
                    float diffZoom = event.getX(0) - event.getX(1);
                    if (diffZoom < 0) diffZoom = -diffZoom;
                    scale = pScale + 3*(diffZoom - fDiffZoom) / getMeasuredWidth();
                    imageView2.setScaleX(scale);
                    imageView2.setScaleY(scale);//1 + 3*(event.getX(0) - event.getX(1) - fDiffZoom) / getMeasuredWidth());
                }

                if (!isMoving && !isZooming) {
                    isSliding = true;
                    slideToRight = event.getX() < fx;
                    slideImage();
                }
                break;
            case MotionEvent.ACTION_UP:
                viewMoving = false;
                fDiffZoom = 0;
                boolean wasApply = false;
                if (!isZooming && !isMoving) {
                    if (fx - event.getX() > getMeasuredWidth() / 3 && slideToRight && pos > 0) { //*2
                        Log.d("sVi", "r " + (fx - event.getX()) + " " + (getMeasuredWidth() / 3) * 2);
                        wasApply = true;
                        isAnimated = true;
                        imageView2.animate().x(-getMeasuredWidth()). //getMeasuredWidth() - (fx - event.getX())).
                            setDuration(400).setListener(new Animator.AnimatorListener() {
                            @Override public void onAnimationStart(Animator animation) {}
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                Log.d("posIV2", "Here "+pos);
                                imageView2.animate().setListener(null);
                                //if (pos > 0)
                                pos--;
                                loadImages();
                                imageView2.setX(0);
                                imageView3.setAlpha(0);//255);
                                imageView3.setScaleX(1);
                                imageView3.setScaleY(1);
                                isAnimated = false;
                            }
                            @Override public void onAnimationCancel(Animator animation) {}
                            @Override public void onAnimationRepeat(Animator animation) {}
                        });
                        imageView3.animate().scaleX(1).scaleY(1).//alpha(255).
                        setDuration(400);
                    }

                    if (event.getX() - fx > getMeasuredWidth() / 3 && !slideToRight && pos < messages.size()-1) {
                        Log.d("sVi", "l " + (event.getX() - fx) + " " + (getMeasuredWidth() / 3));
                        wasApply = true;
                        isAnimated = true;
                        imageView1.animate().x(0). //getMeasuredWidth() - (event.getX() - fx)).
                            setDuration(400).setListener(new Animator.AnimatorListener() {
                            @Override public void onAnimationStart(Animator animation) {}
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                imageView1.animate().setListener(null);
                                //if (pos < messages.size()-1)
                                pos++;
                                Log.d("posIIV", ""+pos);
                                loadImages();
                                imageView1.setX(-getMeasuredWidth());
                                imageView2.setX(0);
                                imageView2.setAlpha(255);
                                imageView2.setScaleX(1);
                                imageView2.setScaleY(1);
                                isAnimated = false;
                            }
                            @Override public void onAnimationCancel(Animator animation) {}
                            @Override public void onAnimationRepeat(Animator animation) {}
                        });
                        //???
                        imageView2.animate().scaleX(0.5f).scaleY(0.5f).//alpha(255).
                        setDuration(400);
                    }
                }

                if (!wasApply) {
                    imageView1.animate().x(-getMeasuredWidth()).setDuration(400);
                    //imageView1.setX(-getMeasuredWidth());
                    if (!isMoving && !isZooming) {
                        //imageView2.setX(0);
                        //imageView2.setY(0);
                        imageView2.animate().x(0).y(0).scaleX(1).scaleY(1).setDuration(400);
                        //imageView2.setScaleX(1);
                        //imageView2.setScaleY(1);
                    }
                    if (scale < 1) {
                        scale = 1;
                        imageView2.animate().scaleX(scale).scaleY(scale).setDuration(400);
                        //imageView2.setScaleX(scale);
                        //imageView2.setScaleY(scale);
                    }
                    if (scale > 3) {
                        scale = 3;
                        //imageView2.setScaleX(scale);
                        //imageView2.setScaleY(scale);
                        imageView2.animate().scaleX(scale).scaleY(scale).setDuration(400);
                        //imageView2.animate().scaleX(s)
                    }
                    imageView2.setAlpha(255);
                    imageView3.setAlpha(0);//255);
                    //imageView3.setScaleX(1);
                    //imageView3.setScaleY(1);
                    imageView3.animate().scaleX(1).scaleY(1).setDuration(400);
                } else {
                    scale = 1.0f;
                }

                if (isMoving) {
                    /*if (imageView2.getX() < 0) {
                        imageView2.setX(0);
                    }
                    if (imageView2.getMeasuredWidth() + imageView2.getX() < getMeasuredWidth()) {
                        imageView2.setX(-(imageView2.getMeasuredWidth() - getMeasuredWidth()));
                    }*/

                    smx = imageView2.getX();
                    smy = imageView2.getY();
                }

                isSliding = false;
                isMoving = false;
                isZooming = false;
                break;
        }
        return true;// viewMoving;//super.onTouchEvent(event);
    }

    int clickCount = 0, MAX_DURATION = 500;
    long startTime, duration;

    private void checkDoubleTap(MotionEvent event) {
        switch(event.getAction()) { //& MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                startTime = System.currentTimeMillis();
                clickCount++;
                break;
            case MotionEvent.ACTION_UP:
                long time = System.currentTimeMillis() - startTime;
                duration=  duration + time;
                if (clickCount == 2) {
                    if (duration<= MAX_DURATION) {
                        Log.d("IView", "Double");
                        scale = scale <= 1.0 ? 3.0f : 1.0f;
                        imageView2.setScaleX(scale);
                        imageView2.setScaleY(scale);
                    }
                    clickCount = 0;
                    duration = 0;
                    break;
                }
        }
    }

    private void slideImage() {
        /*if (slideToRight && -getWidth()/3 > imageView2.getX() && pos == 0) {
            return;
        }
        if (!slideToRight && getWidth()/3 < imageView2.getX() && pos == messages.size()-1) {
            return;
        }*/

        /*if (Math.abs(cx-fx) < Util.dp(20)) {
            //isSliding = false;
            return;
        }*/

        if (!slideToRight) {// && pos+1 != messages.size()) {//  && pos != 0) {
            imageView1.setVisibility(VISIBLE);
            imageView3.setVisibility(GONE);

            Log.d("mi", "l "+(cx-fx));
            //if (pos+1 == messages.size() && cx - fx > Util.dp(20)) return;

            imageView1.setX(cx - fx - getMeasuredWidth());// - cx - fx);
            int a = 255 - (255 * (int) (cx - fx) / getMeasuredWidth());
            a -= 100;
            imageView2.setAlpha(Math.max(127, a));
            //imageView2.setAlpha(Math.max(127, 255 - (255 * (int) (cx - fx) / getMeasuredWidth())));
            Log.d("mia", "al r " + imageView2.getAlpha()+" "+Math.max(127, 255 - (255 * (int) (cx - fx) / getMeasuredWidth())));
            imageView2.setScaleX(scale - (cx - fx) / getMeasuredWidth()); //1 -
            imageView2.setScaleY(scale - (cx - fx) / getMeasuredWidth()); //1 -
        }

        if (slideToRight) {// && pos != 0) {// && pos+1 != messages.size()) {
            imageView1.setVisibility(GONE);
            imageView3.setVisibility(VISIBLE);

            Log.d("mi", "r " + (cx - fx));
            //if (pos == 0 && cx - fx > Util.dp(20)) return;
            //255 - neprozrachnaya
            imageView2.setX(cx - fx);// - getMeasuredWidth());// - cx - fx);

            imageView3.setAlpha(Math.max(127, 255 - (255 * (int) (cx - fx) / getMeasuredWidth())-100));
            Log.d("mia", "al r " + imageView3.getAlpha()+" "+Math.max(127, 255 - (255 * (int) (cx - fx) / getMeasuredWidth())));
            imageView3.setScaleX(-(cx - fx) / getMeasuredWidth());
            imageView3.setScaleY(-(cx - fx) / getMeasuredWidth());
        }
    }

    private void loadImage(int pos, ImageView imageView) {
        if (messages.size() <= pos || pos < 0 || !(messages.get(pos).message instanceof TdApi.MessagePhoto)) {
            Log.d("ImEmp", "");
            imageView.setImageDrawable(null);
            //imageView.setImageResource(0);
            return;
        }
        TdApi.PhotoSize photo = TGFunc.getBetterPhoto(((TdApi.MessagePhoto) messages.get(pos).message).photo.photos);
        String path = photo.photo.path;

        Picasso.with(context).load(new File(path))//getFileLocalPath(file)))
                //.centerCrop()
                .into(imageView);

        /*BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        bitmap = BitmapFactory.decodeFile(path, options);

        imageView.setImageBitmap(bitmap);*/
    }

    private void loadImages() {
        Log.d("posIV", ""+pos);
        //if (pos == 0 || pos+1 == messages.size()) return;
        loadImage(pos+1, imageView1);
        loadImage(pos, imageView2);
        loadImage(pos-1, imageView3);
        Log.d("posIVI", "" + pos);
    }

    private int getBitmapWidth() {
        Bitmap bitmap = ((BitmapDrawable)imageView2.getDrawable()).getBitmap();
        if (bitmap == null) return 0;
        float scaleL = getWidth() > getHeight() ? bitmap.getWidth() / getWidth() : bitmap.getHeight() / getHeight();
        Log.d("gBit1W", ""+ bitmap.getWidth() +" "+ getWidth()+ " "+ bitmap.getHeight() +" "+ getHeight());
        Log.d("gBitW", ""+scaleL+" "+scale+" "+(getWidth() > getHeight())+" "+ bitmap.getWidth() / getWidth()+ " "+ bitmap.getHeight() / getHeight());
        return (int)(bitmap.getWidth()/scaleL * scale);
    }

    private int getBitmapHeight() {
        Bitmap bitmap = ((BitmapDrawable)imageView2.getDrawable()).getBitmap();
        if (bitmap == null) return 0;
        int scaleL = getWidth() > getHeight() ? bitmap.getWidth() / getWidth() : bitmap.getHeight() / getHeight();
        Log.d("gBit1H", ""+ bitmap.getWidth() +" "+ getWidth()+ " "+ bitmap.getHeight() +" "+ getHeight());
        Log.d("gBitH", ""+scaleL+" "+scale+" "+(getWidth() > getHeight())+" "+ bitmap.getWidth() / getWidth()+ " "+ bitmap.getHeight() / getHeight());
        return (int)(bitmap.getHeight()/scaleL * scale);
    }

}
