package com.kand.telegram.logic;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.kand.telegram.R;
import com.kand.telegram.ui.ChatFragment;
import com.kand.telegram.ui.ImageViewerFragment;
import com.kand.telegram.ui.MainActivity;
import com.kand.telegram.ui.MusicPlaylistFragment;
import com.kand.telegram.ui.SharedMediaFragment;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;
import java.util.Arrays;

public class TGService extends Service {

    private final String LOG_TAG = "AppService";
    private NotificationManager notificationManager;
    private Client client;
    private int offset = 0, limit = 20;

    public boolean isActive = false;
    public TdApi.User user = new TdApi.User();
    public ChatFragment onChatUpdateHandler;
    public MusicPlaylistFragment musicPlaylistFragment;
    public SharedMediaFragment sharedMediaFragment;
    public ImageViewerFragment imageViewerFragment;

    public boolean isDownloadedAllChats = false, isLoading = false;
    public ArrayList<TdApi.Chat> chats = new ArrayList<>();
    public ArrayList<TdApi.StickerSet> stickerSets = new ArrayList<>();

    Client.ResultHandler emptyHandler = new Client.ResultHandler() {
        @Override public void onResult (TdApi.TLObject object) {}
    };

    Client.ResultHandler getMe = new Client.ResultHandler() {
        @Override
        public void onResult(final TdApi.TLObject object) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Log.d("getMe", object.toString());
                    if (object instanceof TdApi.User) {
                        user = (TdApi.User) object;
                        downloadFile(user.profilePhoto.small.id);
                        TGFunc.setUserId(getApplicationContext(), user.id);
                        sendAnswer(TdApi.GetMe.CONSTRUCTOR);
                    } else {
                        getMe();
                    }
                }
            });

        }
    };

    Client.ResultHandler getChat = new Client.ResultHandler() {
        @Override
        public void onResult(final TdApi.TLObject object) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    chats.add(0, (TdApi.Chat) object);
                    downloadFile(TGFunc.getChatPhotoId(chats.get(0)));
                }
            });

        }
    };

    Client.ResultHandler getChats = new Client.ResultHandler() {
        @Override
        public void onResult(final TdApi.TLObject object) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Log.d("Login Main", object.toString());
                    if (object instanceof TdApi.Chats) {
                        TdApi.Chat[] newChats = ((TdApi.Chats) object).chats;

                        if (newChats.length != 0) {
                            chats.addAll(Arrays.asList(newChats));
                            downloadChatPhotos();

                            sendAnswer(TdApi.GetChats.CONSTRUCTOR);
                            offset += newChats.length;
                        } else {
                            isDownloadedAllChats = true;
                        }
                        isLoading = false;
                    } else if (object instanceof TdApi.Error) {
                        sendAnswer(Util.LogOut);
                    }
                }
            });

        }
    };

    Client.ResultHandler updateHandler = new Client.ResultHandler() {
        @Override
        public void onResult(final TdApi.TLObject object) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Log.d("UPD hand", ""+object.toString());
                    int userId, pos = -1;
                    switch (object.getConstructor()) {
                        case TdApi.UpdateUser.CONSTRUCTOR:
                            TdApi.User updateUser = ((TdApi.UpdateUser)object).user;
                            userId = updateUser.id;
                            TGFunc.updateUser(TGFunc.getUserByIdFromChats(userId, chats), updateUser);
                            if (user.id == userId) {
                                Log.d("yes", "");
                                Log.d("yes 2", updateUser.toString());
                                TGFunc.updateUser(user, updateUser);
                                sendAnswer(Util.UpdateCurrentUser);
                            }
                            downloadFile(updateUser.profilePhoto.small.id);
                            break;
                        case TdApi.UpdateUserStatus.CONSTRUCTOR:
                            Log.d("UpdateUserStatus", object.toString()+" "+user.id);

                            userId = ((TdApi.UpdateUserStatus)object).userId;
                            TdApi.UserStatus status = ((TdApi.UpdateUserStatus)object).status;
                            TGFunc.getUserByIdFromChats(userId, chats).status = status;
                            if (user.id == userId) user.status = status;
                            pos = TGFunc.getChatPosByUserId(chats, userId);
                            break;

                        case TdApi.UpdateChatReadInbox.CONSTRUCTOR:
                            TdApi.Chat chat = TGFunc.getChatById(chats, ((TdApi.UpdateChatReadInbox) object).chatId);
                            int unreadCount = ((TdApi.UpdateChatReadInbox) object).unreadCount;
                            if (chat.unreadCount != unreadCount) {
                                chat.unreadCount = unreadCount;
                                pos = TGFunc.getChatPositionById(chats, ((TdApi.UpdateChatReadInbox) object).chatId);
                            }
                            break;
                        case TdApi.UpdateNewMessage.CONSTRUCTOR:
                            pos = TGFunc.getChatPositionById(chats, ((TdApi.UpdateNewMessage)object).message.chatId);
                            if (pos == -1) pos = 0;
                            updateNewMessage(((TdApi.UpdateNewMessage)object).message);
                            break;

                        case TdApi.UpdateFile.CONSTRUCTOR:
                            TdApi.File file = ((TdApi.UpdateFile) object).file;
                            updateSticker(file);
                            pos = updateFile(file);
                            if (updateProfilePhotoByFile(user.profilePhoto, file)) {
                                sendAnswer(Util.UpdateCurrentUser);
                            }
                            break;

                        case TdApi.UpdateChatReplyMarkup.CONSTRUCTOR:
                            Log.d("UpdateChatReplyMarkup", object.toString());
                            TdApi.UpdateChatReplyMarkup updateChatReplyMarkup = (TdApi.UpdateChatReplyMarkup) object;
                            TdApi.Chat chatU = TGFunc.getChatById(chats, updateChatReplyMarkup.chatId);
                            chatU.replyMarkupMessageId = updateChatReplyMarkup.replyMarkupMessageId;
                            break;
                        case TdApi.UpdateStickers.CONSTRUCTOR:
                            Log.d("UpdateStickers", object.toString());
                            getStickers();
                            break;

                        case TdApi.UpdateNotificationSettings.CONSTRUCTOR:
                            Log.d("UpdateNotificationSett", object.toString());
                            TdApi.UpdateNotificationSettings notificationSettings = ((TdApi.UpdateNotificationSettings)object);
                            if (notificationSettings.scope instanceof TdApi.NotificationSettingsForChat) {
                                for (int i = 0; i < chats.size(); i++) {
                                    if (chats.get(i).id == ((TdApi.NotificationSettingsForChat)notificationSettings.scope).chatId) {
                                        pos = i;
                                        chats.get(i).notificationSettings = notificationSettings.notificationSettings;
                                        break;
                                    }
                                }
                            }
                            break;

                        default:
                            Log.d("Upd else", object.toString());
                            break;

                    }
                    sendAnswer(object.getConstructor(), pos, object);

                    if (onChatUpdateHandler != null) onChatUpdateHandler.updateHandler(object);
                    MusicPlayer.getInstance().updateHandler(object);
                    if (musicPlaylistFragment != null) musicPlaylistFragment.updateHandler(object);
                    if (sharedMediaFragment != null) sharedMediaFragment.updateHandler(object);
                    if (imageViewerFragment != null) imageViewerFragment.updateHandler(object);
                }
            });
        }
    };

    public void onCreate() {
        super.onCreate();
        Log.d(LOG_TAG, "MyService onCreate");
        serverConnect();
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        takeWakeLock();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(LOG_TAG, "MyService onStartCommand");
        isActive = true;
        if (user != null) sendAnswer(TdApi.GetMe.CONSTRUCTOR);
        if (chats.size() != 0) sendAnswer(TdApi.GetChats.CONSTRUCTOR);
        return super.onStartCommand(intent, flags, startId);
    }

    public IBinder onBind(Intent arg0) {
        Log.d(LOG_TAG, "MyService onBind");
        isActive = true;
        return new MyBinder();
    }

    public void onRebind(Intent intent) {
        super.onRebind(intent);
        Log.d(LOG_TAG, "MyService onRebind");
        isActive = true;
    }

    public boolean onUnbind(Intent intent) {
        Log.d(LOG_TAG, "MyService onUnbind");
        isActive = false;
        return super.onUnbind(intent);
    }

    public void onDestroy() {
        super.onDestroy();
        TG.stopClient();
        Log.d(LOG_TAG, "MyService onDestroy");
    }

    private void serverConnect() {
        TG.setDir(TGFunc.getDir());
        TG.setUpdatesHandler(updateHandler);

        client = TG.getClientInstance();

        if (!TGFunc.isFirstTime()) {
            getPrimaryData();
        }
    }

    public void getPrimaryData() {
        Log.d("getPrimaryData", "");
        user = new TdApi.User();
        chats = new ArrayList<>();
        getMe();
        getChats();

        final long mFrequency = 100;
        final int TICK_WHAT = 2;
        Handler mHandler = new Handler() {
            public void handleMessage(Message m) {
                updateLineStatus();
                sendMessageDelayed(Message.obtain(this, TICK_WHAT), mFrequency);
            }
        };
        mHandler.sendMessageDelayed(Message.obtain(mHandler, TICK_WHAT), mFrequency);
    }

    private void getMe() {
        client.send(new TdApi.GetMe(), getMe);
    }

    public void getChats() {
        client.send(new TdApi.GetChats(offset, limit), getChats);
    }

    public void getStickers() {
        client.send(new TdApi.GetStickerSets(), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if (object instanceof TdApi.StickerSets) {
                    TdApi.StickerSetInfo[] stickersSetInfo = ((TdApi.StickerSets) object).sets;
                    stickerSets = new ArrayList<>();
                    stickerSets.add(new TdApi.StickerSet());
                    for (int i = 0; i < stickersSetInfo.length; i++) {
                        final int j = i+1;
                        stickerSets.add(new TdApi.StickerSet());
                        client.send(new TdApi.GetStickerSet(stickersSetInfo[i].id), new Client.ResultHandler() {
                            @Override
                            public void onResult(TdApi.TLObject object) {
                                if (object instanceof TdApi.StickerSet) {
                                    stickerSets.set(j, (TdApi.StickerSet) object);
                                    
                                    for (TdApi.Sticker sticker : ((TdApi.StickerSet)object).stickers) {
                                        if (sticker.thumb.photo.path.length() == 0) downloadFile(sticker.thumb.photo.id);
                                    }
                                    
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    private void downloadChatPhotos() {
        for (int i=0; i < chats.size(); i++) {
            if (TGFunc.getChatPhotoId(chats.get(i)) != 0) {
                downloadFile(TGFunc.getChatPhotoId(chats.get(i)));
            }
        }
    }

    private void downloadFile(int id) {
        client.send(new TdApi.DownloadFile(id), emptyHandler);
    }

    private int updateFile(TdApi.File file) {
        for (int i = 0; i < chats.size(); i++) {
            if (chats.get(i).type instanceof TdApi.PrivateChatInfo) {
                if (updateProfilePhotoByFile(((TdApi.PrivateChatInfo)chats.get(i).type).user.profilePhoto, file)) {
                    return i;
                }
            }
            if (chats.get(i).type instanceof TdApi.GroupChatInfo) {
                if (updateProfilePhotoByFile(((TdApi.GroupChatInfo)chats.get(i).type).groupChat.photo, file)) {
                    return i;
                }
            }
        }
        return -1;
    }

    private void updateSticker(TdApi.File file) {
        for (TdApi.StickerSet stickerSet : stickerSets) {
            if (stickerSet.stickers != null) //break;
            for (TdApi.Sticker sticker : stickerSet.stickers) {
                if (sticker.thumb.photo.id == file.id) {
                    sticker.thumb.photo = file;
                }
                if (sticker.sticker.id == file.id) {
                    sticker.sticker = file;
                }
            }
        }
    }

    private boolean updateProfilePhotoByFile(TdApi.ProfilePhoto profilePhoto, TdApi.File file) {
        if (profilePhoto == null) return false;
        if (profilePhoto.small.id == file.id) profilePhoto.small = file;
        if (profilePhoto.big.id == file.id) profilePhoto.big = file;
        return profilePhoto.small.id == file.id || profilePhoto.big.id == file.id;
    }

    public void updateNewMessage(TdApi.Message message) {
        int pos = TGFunc.getChatPositionById(chats, message.chatId);
        if (pos != -1) {
            TdApi.Chat chat = chats.get(pos);
            if (message.fromId != user.id) chat.unreadCount++;
            TdApi.TLObject obj = message.message;
            TdApi.GroupChat groupChat = chat.type instanceof TdApi.GroupChatInfo ?
                    ((TdApi.GroupChatInfo) chat.type).groupChat : null;
            switch (obj.getConstructor()) {
                case TdApi.MessageChatChangePhoto.CONSTRUCTOR:
                    TdApi.Photo photo = ((TdApi.MessageChatChangePhoto) message.message).photo;
                    TdApi.PhotoSize[] photos = photo.photos;
                    for (int j = 0; j < photos.length; j++) {
                        if (photos[j].type.equals("a")) {
                            groupChat.photo.small = photos[j].photo;
                        }
                        if (photos[j].type.equals("b")) {
                            groupChat.photo.big = photos[j].photo;
                        }
                    }
                    downloadFile(groupChat.photo.small.id);
                    break;
                case TdApi.MessageChatDeletePhoto.CONSTRUCTOR:
                    if (chat.id == message.chatId) {
                        TdApi.File fileEmpty = new TdApi.File(0, "", 0, "");
                        groupChat.photo.small = fileEmpty;
                        groupChat.photo.big = fileEmpty;
                    }
                    break;
                case TdApi.MessageChatChangeTitle.CONSTRUCTOR:
                    groupChat.title = ((TdApi.MessageChatChangeTitle) message.message).title;
                    break;
            }
            Log.d(LOG_TAG, "replyMarkup "+message.replyMarkup.toString());
            chat.topMessage = message;
            pickUpChat(pos);
        } else {
            client.send(new TdApi.GetChat(message.chatId), getChat);
        }
    }

    private void pickUpChat(int i) {
        TdApi.Chat chat = chats.get(i);
        for (int j=i; j > 0; j--) {
            chats.set(j, chats.get(j - 1));
        }
        chats.set(0, chat);
    }

    public void updateLineStatus() {
        sendAnswer(Util.UpdateLineStatus);
        boolean statusWasChanged = false;
        for (int i = 0; i < chats.size(); i++) {
            TdApi.Chat chat = chats.get(i);
            if (chat.type instanceof TdApi.PrivateChatInfo) {
                TdApi.User user = ((TdApi.PrivateChatInfo) chat.type).user;
                if (user.status instanceof TdApi.UserStatusOnline) {
                    int timeExpire = ((TdApi.UserStatusOnline) user.status).expires;
                    //Log.d("time expire", "  "+System.currentTimeMillis() / 1000L+"  "+ timeExpire);
                    if (System.currentTimeMillis() / 1000L > timeExpire) {
                        user.status = new TdApi.UserStatusOffline(timeExpire);
                        //updateListItem(TGFunc.getChatPositionById(chats, chat.id));
                        statusWasChanged = true;
                    }
                }
            }
        }
        if (statusWasChanged) sendAnswer(TdApi.UpdateUserStatus.CONSTRUCTOR);
    }

    public void sendRequest(int type, Object... args) {
        switch (type) {
            case TdApi.GetChat.CONSTRUCTOR:
                client.send(new TdApi.GetChat((long)args[0]), getChat);
                break;
            case TdApi.GetChats.CONSTRUCTOR:
                client.send(new TdApi. GetChats(offset, limit), getChats);
                break;
        }
    }

    public void sendAnswer(int type, Object... args) {
        if (isActive) {
            Intent intent = new Intent(TGFunc.BROADCAST_ACTION);
            intent.putExtra("type", type);
            if (args.length > 0) {
                intent.putExtra("pos", (int)args[0]);
            }
            sendBroadcast(intent);
        } else if (type == TdApi.UpdateNewMessage.CONSTRUCTOR) {
            if (((TdApi.UpdateNewMessage) args[1]).message.fromId != user.id && chats.get((int)args[0]).notificationSettings.muteFor == 0) {
                sendNotification(type, args[0], ((TdApi.UpdateNewMessage) args[1]).message.message);
            }
        }
    }

    private void sendNotification(int type, Object... args) {
        SharedPreferences prefs = TGFunc.getPreferences(getApplicationContext());
        String userName = prefs.getString("firstName", "")+" " + prefs.getString("lastName", "");
        String text = TGFunc.getTopMessage(getApplicationContext(), userName, (TdApi.MessageContent)args[1]);
        String title = getString(R.string.app_name);
        String previewTitle = text;

        Notification notification = new NotificationCompat.Builder(this)
                .setContentText(previewTitle)
                .setSmallIcon(R.drawable.notification)
                .setWhen(System.currentTimeMillis())
                .setDefaults(Notification.DEFAULT_SOUND)//| Notification.DEFAULT_VIBRATE)
                .build();

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("type", type);
        intent.putExtra("pos", (int)args[0]);
        Log.d(LOG_TAG, "pos = "+(int)args[0]);

        intent.addFlags(PendingIntent.FLAG_UPDATE_CURRENT);

        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        notification.setLatestEventInfo(this, title, text, pIntent);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        notificationManager.notify(1, notification);
    }


    private PowerManager.WakeLock wakeLock = null;

    private void takeWakeLock() {
        if (wakeLock == null) {
            PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
            wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "TAG");
            wakeLock.setReferenceCounted(false);
        }
        wakeLock.acquire();
    }

    public class MyBinder extends Binder {
        public TGService getService() {
            return TGService.this;
        }
    }

}
