package com.kand.telegram.logic;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.TypedValue;

import java.nio.ByteBuffer;

public class Util {

    public static final int UpdateLineStatus = 0;
    public static final int UpdateCurrentUser = 1;
    public static final int UpdateEmoji = 2;
    public static final int UpdateVoiceProgress = 3;
    public static final int VoiceStopped = 4;
    public static final int LogOut = 5;
    public static Context context;
    public static boolean isTablet;

    static {
        System.loadLibrary("tmessages.7");
    }

    public native static long doPQNative(long _what);
    public native static void loadBitmap(String path, Bitmap bitmap, int scale, int width, int height, int stride);
    public native static int pinBitmap(Bitmap bitmap);
    public native static void blurBitmap(Object bitmap, int radius);
    public native static void calcCDT(ByteBuffer hsvBuffer, int width, int height, ByteBuffer buffer);
    public native static Bitmap loadWebpImage(ByteBuffer buffer, int len, BitmapFactory.Options options);
    public native static Bitmap loadBpgImage(ByteBuffer buffer, int len, BitmapFactory.Options options);
    public native static int convertVideoFrame(ByteBuffer src, ByteBuffer dest, int destFormat, int width, int height, int padding, int swap);
    public native static void aesIgeEncryption(ByteBuffer buffer, byte[] key, byte[] iv, boolean encrypt, int offset, int length);

    public native static int startRecord(String path);
    public native static int writeFrame(ByteBuffer frame, int len);
    public native static void stopRecord();
    public native static int openOpusFile(String path);
    public native static int seekOpusFile(float position);
    public native static int isOpusFile(String path);
    public native static void closeOpusFile();
    public native static void readOpusFile(ByteBuffer buffer, int capacity, int[] args);
    public native static long getTotalPcmDuration();

    public static int dp(int px) {
        Resources r = context.getResources();
        return (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, px, r.getDisplayMetrics());
    }

    public static SharedPreferences getPreferences() {
        return context.getSharedPreferences("telegram", Context.MODE_PRIVATE);
    }

}
