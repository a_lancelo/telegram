package com.kand.telegram.logic;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class PatternView extends View {

    private Paint dotPaint;
    private Paint linePaint1;
    private int w, h, hm, curPos;
    private String pass = "";

    private OnEnterPatternListener onEnterPatternListener;

    public interface OnEnterPatternListener {
        public void onStart();
        public void onEnter(String pass);
    }

    public PatternView(Context context) {
        super(context);
        init(context, null);
    }

    public PatternView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {

        Point size = new Point();
        ((Activity)context).getWindowManager().getDefaultDisplay().getSize(size);
        w = getRootView().getMeasuredWidth();
        h = getRootView().getMeasuredHeight();
        hm = Util.dp(20);

        dotPaint = new Paint();
        dotPaint.setAntiAlias(true);
        dotPaint.setColor(Color.WHITE);
        dotPaint.setStyle(Paint.Style.FILL);

        linePaint1 = new Paint();
        linePaint1.setAntiAlias(true);
        linePaint1.setColor(Color.WHITE);
        linePaint1.setStrokeWidth(Util.dp(5));
        setBackgroundColor(Color.parseColor("#5b95c2"));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Log.d("pass", pass);
        for (int i = 0; i < pass.length()-1; i++) {
            int lx = getX(Integer.parseInt(pass.substring(i, i + 1)));
            int ly = getY(Integer.parseInt(pass.substring(i, i + 1)));
            int x = getX(Integer.parseInt(pass.substring(i + 1, i + 2)));
            int y = getY(Integer.parseInt(pass.substring(i + 1, i + 2)));
            canvas.drawLine(lx, ly, x, y, linePaint1);
        }

        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 3; x++) {
                canvas.drawCircle(w/4 + x * w/4, hm + y * h/3, Util.dp(8), dotPaint);
            }
        }

        if (pass.length() != 0) canvas.drawLine(lx, ly, x, y, linePaint1);
    }

    @Override
    protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld){
        super.onSizeChanged(xNew, yNew, xOld, yOld);
        w = xNew;
        h = yNew;
        invalidate();
    }


    float lx, ly, x, y;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                pass = "";
                lx = event.getX();
                ly = event.getY();
                x = event.getX();
                y = event.getY();
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                int pos = getPosition(event.getX(), event.getY());
                Log.d("pass ote", "pos = "+pos+" curPos ="+curPos);
                if (pos > 0 && pos != curPos && !isPosExist(pos)) {
                    pass += pos;
                    curPos = pos;
                    lx = getX(pos);
                    ly = getY(pos);
                }
                x = event.getX();
                y = event.getY();
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                onActionUp();
                break;
        }
        return true;
    }

    private void onActionUp() {
        checkSpace();
        lx = 0;
        ly = 0;
        x = 0;
        y = 0;
        invalidate();
        if (pass.length() < 5) {
            pass = "";
            return;
        }
        if (onEnterPatternListener != null) {
            onEnterPatternListener.onEnter(pass);
        }
    }

    private void checkSpace() {
        for (int ty = (int)(ly < y ? ly : y); ty < (ly > y ? ly : y); ty=ty+Util.dp(20)) {
            for (int tx = (int)(lx < x ? lx : x); tx < (lx > x ? lx : x); tx=tx+Util.dp(20)) {
                int pos = getPosition(tx, ty);
                Log.d("pass chs", "pos = "+pos+" curPos ="+curPos);
                if (pos > 0 && pos != curPos && !isPosExist(pos)) {
                    pass += pos;
                    curPos = pos;
                    lx = getX(pos);
                    ly = getY(pos);
                }
            }
        }
    }

    public void setOnEnterPatternListener(OnEnterPatternListener onEnterPatternListener) {
        this.onEnterPatternListener = onEnterPatternListener;
    }

    private int getPosition(float cx, float cy) {
        int i = 0, d = Util.dp(20);
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 3; x++) {
                i++;
                if (w/4 + x * w/4 + d > cx && w/4 + x * w/4 - d < cx
                        && hm + y * h/3 + d > cy && hm + y * h/3 - d < cy) {
                    Log.d("pos", ""+i);
                    return i;
                }
            }
        }
        return 0;
    }

    private boolean isPosExist(int pos) {
        for (int i = 0; i < pass.length(); i++) {
            Log.d("pass ex", "i = "+i+" sub ="+Integer.parseInt(pass.substring(i, i + 1)));
            if (pos == Integer.parseInt(pass.substring(i, i + 1))) {
                return true;
            }
        }
        return false;
    }

    private int getX(int pos) {
        int i = 0;
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 3; x++) {
                i++;
                if (i == pos) return w/4 + x * w/4;
            }
        }
        return i;
    }

    private int getY(int pos) {
        int i = 0;
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 3; x++) {
                i++;
                if (i == pos) return hm + y * h/3;
            }
        }
        return i;
    }

}