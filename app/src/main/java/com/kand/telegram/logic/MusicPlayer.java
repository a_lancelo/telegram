package com.kand.telegram.logic;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.kand.telegram.R;
import com.kand.telegram.ui.MainActivity;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class MusicPlayer {

    public String LOG_TAG = "MusicPlayer";
    private NotificationManager notificationManager;
    private long chatId;
    public float progress;
    public String audioPath, audioTitle, audioPerformer;
    public boolean isPlayerRunning, showPlayer, isRepeat, isShuffle, isDownload;
    private int currentPosition, curPos, id;
    public ArrayList<TdApi.Message> playlist = new ArrayList<>();
    public ArrayList<Integer> shuffleProgression = new ArrayList<>();
    private static MusicPlayer musicPlayer;
    public static Context context;

    public static MusicPlayer getInstance() {
        if (musicPlayer == null) {
            musicPlayer = new MusicPlayer();
        }
        return musicPlayer;
    }

    private MusicPlayer() {
        getNotificationManager();
    }

    public void playAudio() {
        Log.d(LOG_TAG, "playAudio");
        isPlayerRunning = true;
        showPlayer = true;
        playSong(audioPath);
    }

    public void playAudio(TdApi.Message message) {
        Log.d(LOG_TAG, "playAudio");
        TdApi.Audio audio = ((TdApi.MessageAudio) message.message).audio;
        id = message.id;
        isPlayerRunning = true;
        showPlayer = true;
        audioPath = audio.audio.path;
        audioTitle = audio.title;
        audioPerformer = audio.performer;
        currentPosition = 0;
        if (audioPath.length() != 0) {
            isDownload = false;
            playSong(audioPath);
            downloadNext();
        } else {
            if (mp != null) mp.stop();
            isDownload = true;
            progress = 1;
            TGFunc.downloadFile(audio.audio.id);
        }
        if (chatId != message.chatId) {
            chatId = message.chatId;
            Client client = TG.getClientInstance();
            if (client != null) {
                client.send(new TdApi.SearchMessages(chatId, "", 0, 100, new TdApi.SearchMessagesFilterAudio()), new Client.ResultHandler() {
                    @Override
                    public void onResult(final TdApi.TLObject object) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Log.d("playlist", object.toString());
                                if (object instanceof TdApi.Messages) {
                                    playlist = new ArrayList<>(Arrays.asList(((TdApi.Messages) object).messages));
                                    Collections.reverse(playlist);
                                    setCurPos();
                                    downloadNext();
                                }
                            }
                        });
                    }
                });
            }
        } else {
            setCurPos();
        }
    }

    public void pauseAudio() {
        Log.d(LOG_TAG, "pauseAudio");
        isPlayerRunning = false;
        if (notificationManager == null) {
            getNotificationManager();
        } else {
            notificationManager.cancel(2);
        }
        currentPosition = mp.getCurrentPosition();
        if (mp != null) mp.stop();
    }

    private void setCurPos() {
        for (int i = 0; i < playlist.size(); i++) {
            if (playlist.get(i).id == id) {
                curPos = i;
            }
        }
    }

    private void getNotificationManager() {
        if (context == null) return;
        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public boolean isPlaying() {
        return isPlayerRunning;
    }

    MediaPlayer mp;

    private void playSong(final String path) {
        if (mp != null) mp.stop();
        mp = new MediaPlayer();
        try {
            mp.reset();
            mp.setDataSource(path);
            mp.prepare();
            mp.start();
            sendAudioNotification();
            mp.seekTo(currentPosition);
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer arg0) {
                    if (isRepeat) playSong(path); else next();
                }
            });
        } catch (IOException e) {}
    }

    public void previous() {
        curPos = curPos == 0 ? playlist.size()-1 : curPos-1;
        int pos = isShuffle ? shuffleProgression.get(curPos) : curPos;
        playAudio(playlist.get(pos));
    }

    public void next() {
        curPos = playlist.size() > curPos+1 ? curPos+1 : 0;
        int pos = isShuffle ? shuffleProgression.get(curPos) : curPos;
        playAudio(playlist.get(pos));
    }

    private void downloadNext() {
        if (playlist.size() == 0) return;
        int nextPos = playlist.size() > curPos+1 ? curPos+1 : 0;
        nextPos = isShuffle ? shuffleProgression.get(nextPos) : nextPos;
        TdApi.File file = ((TdApi.MessageAudio) playlist.get(nextPos).message).audio.audio;
        if (file.path.length() == 0) {
            Log.d("downloadNext", ((TdApi.MessageAudio) playlist.get(nextPos).message).audio.title);
            TGFunc.downloadFile(file.id);
        }
    }

    public ArrayList<TdApi.Message> getPlaylist() {
        if (isShuffle) {
            ArrayList<TdApi.Message> messages = new ArrayList<>();
            for (int i = 0; i < shuffleProgression.size(); i++) {
                messages.add(playlist.get(shuffleProgression.get(i)));
            }
            return messages;
        }
        return playlist;
    }

    public void seekAudio(int position) {
        if (mp!= null) {
            mp.seekTo(mp.getDuration() / 100 * position);
            if (!mp.isPlaying()) {
                currentPosition = mp.getCurrentPosition();
                playAudio();
            }
        }
    }

    public int getPlayerPercentProgress() {
        if (mp == null) return 0;
        return mp!= null && mp.getDuration() != 0 ? (mp.getCurrentPosition()*100/mp.getDuration()) : 0;
    }

    public String getPlayerProgress() {
        return mp!= null ? TGFunc.getFormedDuration(mp.getCurrentPosition() / 1000) : "0:00";
    }

    public String getPlayerDuration() {
        return mp!= null ? TGFunc.getFormedDuration(mp.getDuration()/1000) : "0:00";
    }

    public int getMessageId() {
        return id;
    }

    public int getFileId() {
        int pos = isShuffle ? shuffleProgression.get(curPos) : curPos;
        return playlist.size() > pos ? ((TdApi.MessageAudio)playlist.get(pos).message).audio.audio.id : 0;
    }

    public void changeRepeatMode() {
        isRepeat = !isRepeat;
    }

    public void changeShuffle() {
        isShuffle = !isShuffle;
        if (isShuffle) {
            Random random = new Random(System.currentTimeMillis());
            for (TdApi.Message message : playlist) {
                int pos = 0;
                boolean isExist = true;
                while (isExist) {
                    isExist = false;
                    pos = random.nextInt(playlist.size());
                    if (shuffleProgression.size() != 0)
                    for (int val : shuffleProgression) {
                        if (pos == val) {
                            isExist = true;
                        }
                    }
                }
                if (pos == curPos) curPos = shuffleProgression.size()-1;
                shuffleProgression.add(pos);
            }
        } else {
            curPos = shuffleProgression.get(curPos);
            shuffleProgression.clear();
        }
        downloadNext();
    }

    public void addToPlaylist(TdApi.Message message) {
        playlist.add(message);
        shuffleProgression.add(playlist.size()-1);
    }

    public void updateHandler(TdApi.TLObject object) {
        switch (object.getConstructor()) {
            case TdApi.UpdateFile.CONSTRUCTOR:
                updateFile(((TdApi.UpdateFile) object));
                break;
            case TdApi.UpdateFileProgress.CONSTRUCTOR:
                updateFileProgress((TdApi.UpdateFileProgress) object);
                break;
        }
    }

    public void updateFileProgress(TdApi.UpdateFileProgress file) {
        if (file.fileId == getFileId() && file.size != 0) {
            progress = file.ready * 100 / file.size;
        }
    }

    private void updateFile(TdApi.UpdateFile updateFile) {
        TdApi.File file = updateFile.file;
        if (file.id == getFileId() && file.size != 0) {
            int pos = isShuffle ? shuffleProgression.get(curPos) : curPos;
            ((TdApi.MessageAudio)playlist.get(pos).message).audio.audio = file;
            isDownload = false;
            playAudio(playlist.get(pos));
        }
    }

    private void sendAudioNotification() {
        String text = audioPerformer;
        String title = audioTitle;
        String previewTitle = text;

        Notification notification = new NotificationCompat.Builder(context)
                .setContentText(previewTitle)
                .setSmallIcon(R.drawable.notification)
                .setWhen(System.currentTimeMillis())
                .build();

        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("openPlayer", true);

        intent.addFlags(PendingIntent.FLAG_UPDATE_CURRENT);

        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);// | PendingIntent.FLAG_ONE_SHOT);

        notification.setLatestEventInfo(context, title, text, pIntent);
        notification.flags |= Notification.FLAG_NO_CLEAR;

        if (notificationManager == null) {
            getNotificationManager();
        } else {
            notificationManager.notify(2, notification);
        }
    }

}
