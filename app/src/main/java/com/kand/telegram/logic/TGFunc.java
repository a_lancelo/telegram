package com.kand.telegram.logic;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.StatFs;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.format.DateUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.URLSpan;
import android.transition.TransitionInflater;
import android.util.Log;
import android.util.LruCache;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewParent;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kand.telegram.R;
import com.kand.telegram.ui.ChatFragment;
import com.kand.telegram.ui.ImageViewerFragment;
import com.kand.telegram.ui.MainActivity;
import com.kand.telegram.ui.MessagesFragment;
import com.kand.telegram.ui.MusicPlayerFragment;
import com.kand.telegram.ui.ProfileFragment;
import com.squareup.picasso.Picasso;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

public class TGFunc {

    public final static String BROADCAST_ACTION = "com.kand.telegram";

    public static void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

        fileOrDirectory.delete();
    }

    public static String getDir() {
        Log.d("getDir", "" + Environment.getRootDirectory() + " " + Environment.getDataDirectory() + " "
                + Environment.getDownloadCacheDirectory() + " " + Environment.getExternalStorageDirectory());

        String dir = //"/data/data/com.kand.telegram";// "/mnt/sdcard";
                Environment.getExternalStorageDirectory().getAbsolutePath();

        File direct = new File(dir + "/data");
        if (!direct.exists()) direct.mkdirs();
        direct = new File(dir + "/data/Telegram++");
        if (!direct.exists()) direct.mkdirs();
        return direct.getAbsolutePath() + "/";
    }

    public static long getFreeSpace() {
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        long bytesAvailable = (long) stat.getBlockSize() * (long) stat.getAvailableBlocks();
        Log.e("", "Available bytes : " + bytesAvailable);
        return bytesAvailable;
    }

    public static String readableFileSize(long size) {
        if (size <= 0) return "0";
        final String[] units = new String[]{"B", "kB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    /* SharedPreferences */

    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences("telegram", Context.MODE_PRIVATE);
    }

    public static void setFirstTimeState(Context context, boolean state) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putBoolean("firstTime", state).commit();
    }

    public static boolean isFirstTime() {//Context context) {
        return getPreferences(context).getBoolean("firstTime", true);
    }

    public static void setPhoneNumber(Context context, String phone) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString("phoneNumber", phone);
        editor.commit();
    }

    public static void setCountryCode(String country, String code) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putString("country", country);
        editor.putString("phoneNumber", code);
        editor.commit();
    }

    public static void setUserId(Context context, int userId) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putInt("userId", userId);
        editor.commit();
    }

    public static String getCountry() {
        return getPreferences(context).getString("country", context.getString(R.string.russia));
    }

    public static String getPhoneNumber() {
        return getPreferences(context).getString("phoneNumber", "7");
    }

    public static int getUserId(Context context) {
        return getPreferences(context).getInt("userId", 0);
    }

    /* SharedPreferences */

    /* getIds */

    public static TdApi.User getUserById(ArrayList<TdApi.User> items, int id) {
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).id == id) {
                return items.get(i);
            }
        }
        return null;
    }

    public static int getChatPosByUserId(ArrayList<TdApi.Chat> items, long userId) {
        for (int i = 0; i < items.size(); i++) {
            TdApi.Chat item = items.get(i);
            if (item.type instanceof TdApi.PrivateChatInfo) {
                TdApi.User user = ((TdApi.PrivateChatInfo) item.type).user;
                if (user.id == userId) {
                    return i;
                }
            }
        }
        return -1;
    }

    public static TdApi.User getUserByIdFromChats(int id, ArrayList<TdApi.Chat> items) {
        for (int i = 0; i < items.size(); i++) {
            TdApi.Chat item = items.get(i);
            if (item.type instanceof TdApi.PrivateChatInfo) {
                TdApi.User user = ((TdApi.PrivateChatInfo) item.type).user;
                if (user.id == id) return user;
            }
        }
        return new TdApi.User();
    }

    public static TdApi.Chat getChatById(ArrayList<TdApi.Chat> items, long id) {
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).id == id) {
                return items.get(i);
            }
        }
        return new TdApi.Chat();
    }

    public static int getUserPosById(ArrayList<TdApi.User> items, int id) {
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).id == id) {
                return i;
            }
        }
        return -1;
    }

    public static int getChatPositionById(ArrayList<TdApi.Chat> items, long id) {
        for (int i = 0; i < items.size(); i++) {
            Log.d("ItemId ", "" + items.get(i).id + " fileid " + id);
            if (items.get(i).id == id) {
                return i;
            }
        }
        return -1;
    }

    public static int getChatPhotoId(TdApi.Chat chat) {
        int id = 0;
        if (chat.type instanceof TdApi.PrivateChatInfo) {
            return ((TdApi.PrivateChatInfo) chat.type).user.profilePhoto.small.id;
        } else if (chat.type instanceof TdApi.GroupChatInfo) {
            return ((TdApi.GroupChatInfo) chat.type).groupChat.photo.small.id;
        }
        return id;
    }

    public static TdApi.Chat getChatByPhotoId(ArrayList<TdApi.Chat> items, int id) {
        for (int i = 0; i < items.size(); i++) {
            if (getPhotoIdByChat(items.get(i)) == id) {
                return items.get(i);
            }
        }
        return new TdApi.Chat();
    }


    public static int getChatPhotoId(ArrayList<TdApi.Chat> items, int id) {
        for (int i = 0; i < items.size(); i++) {
            if (getPhotoIdByChat(items.get(i)) == id) {
                return i;
            }
        }
        return -1;
    }

    /*public static int getUserPosByPhotoId(ArrayList<TdApi.User> users, int fileId) {
        for (int i = 0; i < users.size(); i++) {

        }
        return -1;
    }*/

    public static int getPhotoIdByChat(TdApi.Chat chat) {
        int id = 0;
        if (chat.type instanceof TdApi.PrivateChatInfo) {
            return ((TdApi.PrivateChatInfo) chat.type).user.profilePhoto.small.id;
        } else if (chat.type instanceof TdApi.GroupChatInfo) {
            return ((TdApi.GroupChatInfo) chat.type).groupChat.photo.small.id;
        }
        return id;
    }

    public static int getUserId(TdApi.TLObject object) {
        switch (object.getConstructor()) {
            case TdApi.UpdateUserStatus.CONSTRUCTOR:
                return ((TdApi.UpdateUserStatus) object).userId;
        }
        return -1;
    }

    public static boolean updateMessagesByFile(ArrayList<TdApi.Message> messages, TdApi.TLObject object) {
        boolean wasUpdated = false;
        int fileId = object instanceof TdApi.UpdateFile ? ((TdApi.UpdateFile) object).file.id : ((TdApi.UpdateFileProgress) object).fileId;
        for (int i = 0; i < messages.size(); i++) {
            if (messages.get(i).message != null) {
                TdApi.MessageContent message = messages.get(i).message;
                TdApi.File file;
                switch (message.getConstructor()) {
                    case TdApi.MessageSticker.CONSTRUCTOR:
                        if (fileWasUpdated(((TdApi.MessageSticker) message).sticker.sticker, object))
                            wasUpdated = true;
                        break;
                    case TdApi.MessagePhoto.CONSTRUCTOR:
                        TdApi.PhotoSize[] photos = ((TdApi.MessagePhoto) message).photo.photos;
                        if (fileWasUpdated(TGFunc.getSmallestPhoto(photos).photo, object)) wasUpdated = true;
                        if (fileWasUpdated(TGFunc.getBetterPhoto(photos).photo, object)) wasUpdated = true;
                        break;
                    case TdApi.MessageDocument.CONSTRUCTOR:
                        file = ((TdApi.MessageDocument) message).document.thumb.photo;
                        if (fileWasUpdated(file, object)) wasUpdated = true;
                        TdApi.File fileDoc = ((TdApi.MessageDocument) message).document.document;
                        if (fileWasUpdated(fileDoc, object)) wasUpdated = true;
                        break;
                    case TdApi.MessageVoice.CONSTRUCTOR:
                        file = ((TdApi.MessageVoice) message).voice.voice;
                        if (fileWasUpdated(file, object)) wasUpdated = true;
                        break;
                    case TdApi.MessageAudio.CONSTRUCTOR:
                        file = ((TdApi.MessageAudio) message).audio.audio;
                        if (fileWasUpdated(file, object)) wasUpdated = true;
                        break;
                }
            }
        }
        return wasUpdated;
    }


    public static int getMessagePosAndUpdateByFile(ArrayList<TdApi.Message> messages, TdApi.TLObject object) {

        Log.d("updFile0", "" + object.toString());
        int fileId = object instanceof TdApi.UpdateFile ? ((TdApi.UpdateFile) object).file.id : ((TdApi.UpdateFileProgress) object).fileId;
        for (int i = 0; i < messages.size(); i++) {
            if (messages.get(i).message != null) {
                TdApi.MessageContent message = messages.get(i).message;
                TdApi.File file;
                TdApi.PhotoSize[] photos;
                switch (message.getConstructor()) {
                    case TdApi.MessageSticker.CONSTRUCTOR:
                        if (fileWasUpdated(((TdApi.MessageSticker) message).sticker.sticker, object))
                            return i;
                        break;

                    case TdApi.MessagePhoto.CONSTRUCTOR:
                        //if (fileWasUpdated(((TdApi.MessageSticker) message).sticker.sticker, object)) return i;
                        photos = ((TdApi.MessagePhoto) message).photo.photos;
                        if (fileWasUpdated(TGFunc.getSmallestPhoto(photos).photo, object)) return i;
                        if (fileWasUpdated(TGFunc.getBetterPhoto(photos).photo, object)) return i;

                        break;
                    case TdApi.MessageDocument.CONSTRUCTOR:
                        Log.d("updFileDoc", "" + object.toString());
                        file = ((TdApi.MessageDocument) message).document.thumb.photo;
                        if (fileWasUpdated(file, object)) return i;
                        TdApi.File fileDoc = ((TdApi.MessageDocument) message).document.document;
                        if (fileWasUpdated(fileDoc, object)) return i;

                        break;
                    case TdApi.MessageVoice.CONSTRUCTOR:
                        file = ((TdApi.MessageVoice) message).voice.voice;
                        if (fileWasUpdated(file, object)) return i;
                        break;
                    case TdApi.MessageAudio.CONSTRUCTOR:
                        file = ((TdApi.MessageAudio) message).audio.audio;
                        if (fileWasUpdated(file, object)) return i;
                        break;

                    case TdApi.MessageWebPage.CONSTRUCTOR:
                        photos = ((TdApi.MessageWebPage) message).webPage.photo.photos;
                        if (photos.length == 0) return -1;
                        if (fileWasUpdated(TGFunc.getSmallestPhoto(photos).photo, object)) return i;
                        if (fileWasUpdated(TGFunc.getBetterPhoto(photos).photo, object)) return i;
                        break;
                    case TdApi.MessageVideo.CONSTRUCTOR:
                        file =  ((TdApi.MessageVideo) message).video.thumb.photo;
                        if (fileWasUpdated(file, object)) return i;
                        break;
                }
            }
        }

        return -1;
    }

    public static int getMessagePosByFileId(Context context, ArrayList<TdApi.Message> messages, TdApi.TLObject object) {
        Log.d("updFilePos", "" + object.toString());
        int fileId = object instanceof TdApi.UpdateFile ? ((TdApi.UpdateFile) object).file.id : ((TdApi.UpdateFileProgress) object).fileId;
        for (int i = 0; i < messages.size(); i++) {
            if (messages.get(i).message != null) {
                TdApi.MessageContent message = messages.get(i).message;
                switch (message.getConstructor()) {
                    case TdApi.MessageSticker.CONSTRUCTOR:
                        break;
                    case TdApi.MessageVoice.CONSTRUCTOR:
                        if (((TdApi.MessageVoice) message).voice.voice.id == fileId) {
                            return i;
                        }
                        break;
                    case TdApi.MessageAudio.CONSTRUCTOR:
                        if (((TdApi.MessageAudio) message).audio.audio.id == fileId) {
                            return i;
                        }
                        break;
                    case TdApi.MessageDocument.CONSTRUCTOR:
                        if (((TdApi.MessageDocument) message).document.document.id == fileId) {
                            return i;
                        }
                        break;
                }
            }
        }
        return -1;
    }

    private static boolean fileWasUpdated(TdApi.File file, TdApi.TLObject object) {
        if (object instanceof TdApi.UpdateFile) {
            TdApi.UpdateFile update = (TdApi.UpdateFile) object;
            if (file.id == update.file.id) {
                file.path = update.file.path;
                Log.d("fileWas", file.toString());
                return true;
            }
        }
        return false;
    }

    /* getIds */

    public static void setChatPhoto(TdApi.Chat chat, TdApi.UpdateFile file) {
        //TdApi.FileLocal fileLocal = new TdApi.FileLocal(file.fileId, file.size, file.path);
        if (chat.type instanceof TdApi.PrivateChatInfo) {
            ((TdApi.PrivateChatInfo) chat.type).user.profilePhoto.small = file.file;
        } else if (chat.type instanceof TdApi.GroupChatInfo) {
            ((TdApi.GroupChatInfo) chat.type).groupChat.photo.small = file.file;
        }
    }

    public static void setMessagePhoto(TdApi.Message message, TdApi.UpdateFile file) {
        //TdApi.FileLocal fileLocal = new TdApi.FileLocal(file.fileId, file.size, file.path);
        if (message.message instanceof TdApi.MessagePhoto) {
            ((TdApi.MessagePhoto) message.message).photo.photos[0].photo = file.file;
        }
    }

    public static String getChatTitle(TdApi.Chat chat) {
        String chatName = "";
        if (chat.type instanceof TdApi.PrivateChatInfo) {
            TdApi.User user = ((TdApi.PrivateChatInfo) chat.type).user;
            chatName = user.firstName + " " + user.lastName;
        } else if (chat.type instanceof TdApi.GroupChatInfo) {
            chatName = ((TdApi.GroupChatInfo) chat.type).groupChat.title;
        }
        return chatName;
    }

    private static ArrayList<TdApi.User> cachedUsers = new ArrayList<>();
    private static ArrayList<TdApi.Message> cachedMessages = new ArrayList<>();

    public static void setTopMessage(final Context context, final TextView textView, final TdApi.Chat chat) {
        textView.setTextColor(Color.parseColor(
                chat.topMessage.message instanceof TdApi.MessageText ? "#8a8a8a" : "#6b9cc2"));


        final TdApi.MessageContent message = chat.topMessage.message;
        if (chat.type instanceof TdApi.GroupChatInfo) {
            if (getUserId(context) != chat.topMessage.fromId) {
                for (int i = 0; i < cachedUsers.size(); i++) {
                    if (chat.topMessage.fromId == cachedUsers.get(i).id) {
                        textView.setText(getTopMessage(context, cachedUsers.get(i).firstName, message));
                        //Log.d("cachUs", cachedUsers.get(i).id)
                        return;
                    }
                }
                Client client = TG.getClientInstance();
                if (client == null) return;
                client.send(new TdApi.GetUser(chat.topMessage.fromId), new Client.ResultHandler() {
                    @Override
                    public void onResult(final TdApi.TLObject object) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                if (object instanceof TdApi.Error) return;
                                Log.i("text", getTopMessage(context, ((TdApi.User) object).firstName, message));
                                textView.setText(getTopMessage(context, ((TdApi.User) object).firstName, message));
                                cachedUsers.add((TdApi.User) object);
                            }
                        });

                    }
                });
            } else {
                textView.setText(getTopMessage(context, context.getString(R.string.you), message));
            }
        } else {
            textView.setText(getTopMessage(context, "", message));

            if (chat.topMessage.message instanceof TdApi.MessageText) {
                Emoji.applicationContext = context;
                textView.setText(Emoji.replaceEmoji(getTopMessage(context, "", message), textView.getPaint().getFontMetricsInt(), (20)));
            }

        }
    }

    public static String getTopMessage(Context context, String userName, TdApi.MessageContent message) {
        String name = userName.length() != 0 ? userName + ": " : "";
        TdApi.User user;
        switch (message.getConstructor()) {
            case TdApi.MessageText.CONSTRUCTOR:
                return name + ((TdApi.MessageText) message).text;
            case TdApi.MessageAudio.CONSTRUCTOR:
                return name + context.getString(R.string.audio);
            case TdApi.MessageVoice.CONSTRUCTOR:
                return name + context.getString(R.string.voice);
            case TdApi.MessageDocument.CONSTRUCTOR:
                return name + context.getString(R.string.document);
            case TdApi.MessageSticker.CONSTRUCTOR:
                return name + context.getString(R.string.sticker);
            case TdApi.MessagePhoto.CONSTRUCTOR:
                return name + context.getString(R.string.photo);
            case TdApi.MessageVideo.CONSTRUCTOR:
                return name + context.getString(R.string.video);
            case TdApi.MessageWebPage.CONSTRUCTOR:
                return name + context.getString(R.string.web_page);
            case TdApi.MessageContact.CONSTRUCTOR:
                return name + context.getString(R.string.contact);
            case TdApi.MessageLocation.CONSTRUCTOR:
                return name + context.getString(R.string.geo_point);

            case TdApi.MessageGroupChatCreate.CONSTRUCTOR:
                return userName + " " + context.getString(R.string.chat_create) + " " + ((TdApi.MessageGroupChatCreate) message).title;
            case TdApi.MessageChatAddParticipant.CONSTRUCTOR:
                user = ((TdApi.MessageChatAddParticipant) message).user;
                return userName + " " + context.getString(R.string.chat_add_participant) + " " + user.firstName + " " + user.lastName;
            case TdApi.MessageChatChangePhoto.CONSTRUCTOR:
                return userName + " " + context.getString(R.string.chat_change_photo);
            case TdApi.MessageChatChangeTitle.CONSTRUCTOR:
                return userName + " " + context.getString(R.string.chat_change_title) + " " + ((TdApi.MessageChatChangeTitle) message).title;
            case TdApi.MessageChatDeleteParticipant.CONSTRUCTOR:
                user = ((TdApi.MessageChatDeleteParticipant) message).user;
                return user.firstName + " " + user.lastName + " " + context.getString(R.string.chat_delete_participant);
            case TdApi.MessageChatDeletePhoto.CONSTRUCTOR:
                return userName + " " + context.getString(R.string.chat_delete_photo);

        }
        return "";
    }

    private static boolean isMessageContentInteractionChat(TdApi.MessageContent message) {
        switch (message.getConstructor()) {
            case TdApi.MessageGroupChatCreate.CONSTRUCTOR:
            case TdApi.MessageChatAddParticipant.CONSTRUCTOR:
            case TdApi.MessageChatChangePhoto.CONSTRUCTOR:
            case TdApi.MessageChatChangeTitle.CONSTRUCTOR:
            case TdApi.MessageChatDeleteParticipant.CONSTRUCTOR:
            case TdApi.MessageChatDeletePhoto.CONSTRUCTOR:
                return true;
        }
        return false;
    }

    public static String getFileLocalPath(TdApi.File file) {
        return file.path;
    }

    public static View getChatView(Context context, TdApi.Chat chat) {
        if (context == null) return null;
        LayoutInflater lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = lInflater.inflate(R.layout.main_item, null);
        if (chat == null) return view;
        boolean isOnline = isOnline(context);
        TdApi.File file;
        if (chat.type instanceof TdApi.GroupChatInfo) {
            view.findViewById(R.id.image_group).setVisibility(View.VISIBLE);
            file = ((TdApi.GroupChatInfo) chat.type).groupChat.photo.small;
            view.findViewById(R.id.status).setVisibility(View.GONE);
        } else {
            file = ((TdApi.PrivateChatInfo) chat.type).user.profilePhoto.small;
            TdApi.UserStatus status = ((TdApi.PrivateChatInfo) chat.type).user.status;
            if (status instanceof TdApi.UserStatusOnline || ((TdApi.PrivateChatInfo) chat.type).user.id == TGFunc.getUserId(context)) {
                view.findViewById(R.id.status).setVisibility(View.VISIBLE);
                view.findViewById(R.id.status).setBackgroundResource(0);
                view.findViewById(R.id.status).setBackgroundDrawable(context.getResources().getDrawable(isOnline ? R.drawable.ic_online : R.drawable.ic_clock));

            }
        }
        ((TextView) view.findViewById(R.id.chat_name)).setText(TGFunc.getChatTitle(chat));

        ((TextView) view.findViewById(R.id.date)).setText(TGFunc.getFormedDateForChat(chat.topMessage.date * 1000L));
        setTopMessage(context, ((TextView) view.findViewById(R.id.text)), chat);
        if (chat.unreadCount != 0) {
            view.findViewById(R.id.counter_layout).setVisibility(View.VISIBLE);
            if (isOnline) {
                view.findViewById(R.id.counter_layout).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.dialogs_badge));
                ((TextView) view.findViewById(R.id.counter_unread_messages)).setText(String.valueOf(chat.unreadCount));
            } else {
                view.findViewById(R.id.counter_layout).setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ic_error));
                ((TextView) view.findViewById(R.id.counter_unread_messages)).setText("");
            }
        }

        if (chat.notificationSettings.muteFor != 0) {
            view.findViewById(R.id.mute).setVisibility(View.VISIBLE);
        }

        ImageView imageView = ((ImageView) view.findViewById(R.id.imageView));
        Drawable drawable = new BitmapDrawable(context.getResources(),
                CircleTransform.makeCircleBitmap(getChatPlaceholder(context, chat, 50)));

        Log.d("path", getFileLocalPath(file));

        Picasso.with(context).load(new File(getFileLocalPath(file)))
                .placeholder(drawable)
                .transform(new CircleTransform())
                .into(imageView);
        return view;
    }

    public static View getMessageView(final Context context, final TdApi.User user, final TdApi.Message message, final Float progress, boolean other) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        LayoutInflater lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (message.message == null) {
            if (message.date == 0) {
                View view = lInflater.inflate(R.layout.chat_unread, null);
                ((TextView) view.findViewById(R.id.text)).setText(message.forwardDate +" "+ context.getString(message.forwardDate == 1 ? R.string.new_message : R.string.new_messages));
                return view;
            } else {
                View view = lInflater.inflate(R.layout.chat_date, null);
                ((TextView) view.findViewById(R.id.date_day)).
                        setText(TGFunc.getFormedDateForMessage(message.date * 1000L));
                return view;
            }
        }
        final View rootView = lInflater.inflate(R.layout.chat_item, null);
        if (message == null) return rootView;

        String title = user.firstName + " " + user.lastName;
        if (isMessageContentInteractionChat(message.message)) {
            View viewState = lInflater.inflate(R.layout.chat_state, null);
            final String text = getTopMessage(context, title, message.message);
            Spannable spannable = new SpannableString(text);
            spannable.setSpan(new ForegroundColorSpan(Color.parseColor("#5b95c2")), 0, title.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannable.setSpan(getUserSpan(user), 0, title.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            TdApi.User _user;
            int spanSize;
            switch (message.message.getConstructor()) {
                case TdApi.MessageChatAddParticipant.CONSTRUCTOR:
                    _user = ((TdApi.MessageChatAddParticipant) message.message).user;
                    spanSize = (_user.firstName + " " + _user.lastName).length();
                    spannable.setSpan(new ForegroundColorSpan(Color.parseColor("#5b95c2")), text.length() - spanSize, text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    spannable.setSpan(getUserSpan(_user), text.length() - spanSize, text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    break;
                case TdApi.MessageChatDeleteParticipant.CONSTRUCTOR:
                    _user = ((TdApi.MessageChatDeleteParticipant) message.message).user;
                    spanSize = (_user.firstName + " " + _user.lastName).length();
                    spannable.setSpan(new ForegroundColorSpan(Color.parseColor("#5b95c2")), 0, spanSize, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    spannable.setSpan(getUserSpan(_user), 0, spanSize, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    break;
            }
            ((TextView) viewState.findViewById(R.id.state)).setText(spannable);
            ((TextView) viewState.findViewById(R.id.state)).setMovementMethod(LinkMovementMethod.getInstance());
            return viewState;
        }

        final RelativeLayout messageLayout = (RelativeLayout) rootView.findViewById(R.id.messageLayout);
        TextView textView = new TextView(context);
        textView.setTextColor(Color.parseColor("#333333"));
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        textView.setPadding(0, 0, Util.dp(15), 0);

        final TdApi.File file;
        View view;

        if (message.forwardFromId == 0)
        switch (message.message.getConstructor()) {
            case TdApi.MessageText.CONSTRUCTOR:
                final String text = ((TdApi.MessageText) message.message).text;
                Spannable spannable = new SpannableString(text);
                boolean isUsername = false, isCommand = false;
                String word = "";

                for (int i = 1; i < text.length(); i++) {
                    if (i-2 == -1 || text.substring(i-2, i-1).equals(" ") || text.substring(i-2, i-1).equals("\n")) {
                        if (text.substring(i-1, i).equals("@") && !isCommand) isUsername = true;
                        if (text.substring(i-1, i).equals("/")) isCommand = true;
                    }

                    if ((Character.isLetterOrDigit(text.charAt(i)) || (text.substring(i, i+1).equals("_")) || (isCommand && text.substring(i, i+1).equals("@"))) && i != text.length()-1) {
                        if (isUsername || isCommand) word += text.charAt(i);
                    } else {
                        if ((isUsername || isCommand) && word.length() > 0) {
                            if (Character.isLetterOrDigit(text.charAt(i))  && i == text.length()-1) {
                                word += text.charAt(i);
                                i++;
                            }
                            if ((isUsername && word.length() >= 5) || isCommand) {
                                final String mWord = text.substring(i - word.length() - 1, i);
                                ClickableSpan userSpan = new ClickableSpan() {
                                    final String word;
                                    {
                                        word = mWord.substring(1, mWord.length());
                                    }

                                    @Override
                                    public void onClick(View widget) {
                                        Log.d("userSpan", "click");
                                        searchUser(word);
                                    }

                                    public void updateDrawState(TextPaint ds) {
                                        ds.setUnderlineText(false);
                                    }
                                };
                                ClickableSpan commandSpan = new ClickableSpan() {
                                    final String word;
                                    {
                                        word = mWord + ((message.chatId < 0 && user.type instanceof TdApi.UserTypeBot) ? ("@" + user.username) : "");
                                    }

                                    @Override
                                    public void onClick(View widget) {
                                        Log.d("commandSpan", "click");
                                        TdApi.InputMessageText message = new TdApi.InputMessageText(word);
                                        ((ChatFragment)((MainActivity)context).getSupportFragmentManager().findFragmentByTag("chat")).sendMessage(message);
                                    }

                                    public void updateDrawState(TextPaint ds) {
                                        ds.setUnderlineText(false);
                                    }
                                };

                                spannable.setSpan(new ForegroundColorSpan(Color.parseColor("#5b95c2")), i - word.length() - 1, i, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                                spannable.setSpan(isUsername ? userSpan : commandSpan, i - word.length() - 1, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                            }
                            isUsername = false;
                            isCommand = false;
                            word = "";
                        }
                    }
                }

                textView.setText(Emoji.replaceEmoji(spannable, textView.getPaint().getFontMetricsInt(), Util.dp(20)));
                textView.setMovementMethod(LinkMovementMethod.getInstance());

                messageLayout.addView(textView, messageLayout.getLayoutParams());
                break;
            case TdApi.MessageSticker.CONSTRUCTOR:
                view = lInflater.inflate(R.layout.chat_image, null);

                ImageView imageViewSticker = (ImageView) view.findViewById(R.id.image);
                TdApi.Sticker sticker = ((TdApi.MessageSticker) message.message).sticker;
                TdApi.PhotoSize photoSize = new TdApi.PhotoSize("", null, sticker.width, sticker.height);
                setSizeToSticker(context, imageViewSticker, photoSize, Color.WHITE);

                if (sticker.sticker.path.length() != 0) {
                    loadSticker(sticker.sticker.path, imageViewSticker);
                }
                messageLayout.addView(view, messageLayout.getLayoutParams());
                break;
            case TdApi.MessagePhoto.CONSTRUCTOR:
                view = lInflater.inflate(R.layout.chat_image, null);
                /*ImageView image = (ImageView) view.findViewById(R.id.image);
                final TdApi.PhotoSize[] photos = ((TdApi.MessagePhoto) message.message).photo.photos;
                setSizeToImageView(context, image, getLargestPhoto(photos), Color.LTGRAY);
                TdApi.PhotoSize betterPhotoSize = getBetterPhoto(photos);

                int rw = betterPhotoSize.width, rh = betterPhotoSize.height;
                if (rw == 0 || rh == 0) {
                    rw = Util.dp(100);
                    rh = Util.dp(100);
                }

                String photoPath = (betterPhotoSize.photo.path.length() != 0 ? betterPhotoSize : getSmallestPhoto(photos)).photo.path;
                Picasso.with(context).load(new File(photoPath))
                        .resize(rw, rh).centerCrop()
                        .into(image);*/
                //image
                setBetterImage(view, ((TdApi.MessagePhoto) message.message).photo.photos);
                view.findViewById(R.id.image).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ImageViewerFragment fragment = new ImageViewerFragment();
                        fragment.putPhoto(message.chatId, message.id, ((TdApi.MessagePhoto) message.message).photo);
                        if (Util.isTablet) {
                            ((FragmentActivity) context).findViewById(R.id.container_fullscreen).setVisibility(View.VISIBLE);
                            ((FragmentActivity) context).getSupportFragmentManager().
                                    beginTransaction().add(R.id.container_fullscreen, fragment, "imageViewer").addToBackStack(null).commit();
                        } else {
                            //fragment.setSharedElementEnterTransition( TransitionInflater.from(context).inflateTransition(android.R.transition.move));
                            ((FragmentActivity) context).getSupportFragmentManager().
                                    beginTransaction().setCustomAnimations(R.anim.enter_from_right, 0, 0, R.anim.exit_to_right).add(R.id.container, fragment, "imageViewer").addToBackStack(null).commit();
                        }
                            }
                });
                messageLayout.addView(view, messageLayout.getLayoutParams());
                break;

            case TdApi.MessageContact.CONSTRUCTOR:
                view = lInflater.inflate(R.layout.chat_contact, null);
                final TdApi.MessageContact contact = (TdApi.MessageContact) message.message;
                ((TextView) view.findViewById(R.id.name)).setText(contact.firstName + " " + contact.lastName);
                ((TextView) view.findViewById(R.id.phone)).setText(contact.phoneNumber);

                messageLayout.addView(view, messageLayout.getLayoutParams());
                break;

            case TdApi.MessageVoice.CONSTRUCTOR:
                view = lInflater.inflate(R.layout.chat_voice, null);
                TdApi.Voice voice = ((TdApi.MessageVoice) message.message).voice;
                ((TextView) view.findViewById(R.id.duration)).setText(getFormedDuration(voice.duration));
                file = voice.voice;

                if (file.path.length() > 0) {
                    final ImageView play = (ImageView) view.findViewById(R.id.play);
                    final ImageView pause = (ImageView) view.findViewById(R.id.pause);

                    view.findViewById(R.id.shadow_blue).setVisibility(View.VISIBLE);

                    if (MediaController.getInstance().isPlayingAudio() && message.id == MediaController.getInstance().getMessageId()) {
                        pause.setVisibility(View.VISIBLE);
                    } else {
                        play.setVisibility(View.VISIBLE);
                    }

                    play.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            pause.setVisibility(View.VISIBLE);
                            play.setVisibility(View.GONE);

                            MusicPlayer service = MusicPlayer.getInstance();
                            if (service.isPlaying()) service.pauseAudio();

                            MediaController.getInstance().playAudio(message);
                            invalidateListView(play);
                        }
                    });
                    pause.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            pause.setVisibility(View.GONE);
                            play.setVisibility(View.VISIBLE);
                            //MediaUtil.getInstance().stopVoice();
                            MediaController.getInstance().pauseAudio();
                            invalidateListView(play);
                        }
                    });
                    ((SeekBar)view.findViewById(R.id.seekBar)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                        @Override
                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                            if (message.id == MediaController.getInstance().getMessageId())
                            if (fromUser) {
                                MediaController.getInstance().seekToProgress(progress);
                            }
                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {}

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {}
                    });

                    if (message.id == MediaController.getInstance().getMessageId()) {// && !MediaController.getInstance().audioProgressChange) {
                        ((SeekBar)view.findViewById(R.id.seekBar)).setProgress((int)(MediaController.getInstance().audioProgress*100));
                    }

                } else {
                    setDownloadMode(view, file.id, progress);
                }
                messageLayout.addView(view, messageLayout.getLayoutParams());
                break;

            case TdApi.MessageAudio.CONSTRUCTOR:
                messageLayout.addView(getMessageAudioView(message, progress), messageLayout.getLayoutParams());
                break;

            case TdApi.MessageDocument.CONSTRUCTOR:
                view = lInflater.inflate(R.layout.chat_document, null);
                final TdApi.Document document = ((TdApi.MessageDocument) message.message).document;
                ((TextView) view.findViewById(R.id.fileName)).setText(document.fileName);
                ((TextView) view.findViewById(R.id.size)).setText(readableFileSize(document.document.size));
                file = document.document;

                if (file.path.length() > 0) {
                    View.OnClickListener onFileClick = new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.d("fileMime = ", file.path + " " + document.mimeType);
                            Intent intent = new Intent();
                            intent.setAction(android.content.Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.fromFile(new File(file.path)), document.mimeType);
                            try {
                                context.startActivity(intent);
                            } catch (Exception e) {}
                        }
                    };

                    final ImageView fileImageView = (ImageView) view.findViewById(R.id.file);
                    fileImageView.setVisibility(View.VISIBLE);
                    fileImageView.setOnClickListener(onFileClick);

                    view.findViewById(R.id.download).setVisibility(View.GONE);
                    final ImageView thumb = (ImageView) view.findViewById(R.id.thumb);
                    if (document.thumb.photo.path.length() != 0) {
                        thumb.setVisibility(View.VISIBLE);
                        thumb.setOnClickListener(onFileClick);
                        Picasso.with(context).load(new File(file.path))
                                .resize(40, 40)
                                .centerCrop()
                                .into(thumb);
                    } else {
                        downloadFile(document.thumb.photo.id);
                    }
                } else {
                    setDownloadMode(view, file.id, progress);
                }
                messageLayout.addView(view, messageLayout.getLayoutParams());
                break;

            case TdApi.MessageWebPage.CONSTRUCTOR:
                Log.d("mesweb", ""+message.message);
                view = lInflater.inflate(R.layout.chat_web_page, null);
                final TdApi.MessageWebPage webPage = (TdApi.MessageWebPage) message.message;
                ((TextView) view.findViewById(R.id.link)).setText(webPage.text);
                ((TextView) view.findViewById(R.id.siteName)).setText(webPage.webPage.siteName);
                ((TextView) view.findViewById(R.id.title)).setText(webPage.webPage.title);
                ((TextView) view.findViewById(R.id.description)).setText(webPage.webPage.description);
                if (webPage.webPage.duration != 0) {
                    ((TextView) view.findViewById(R.id.duration)).setText(getFormedTime(webPage.webPage.duration));
                }
                if (webPage.webPage.photo.photos.length != 0) {
                    setBetterImage(view, webPage.webPage.photo.photos);
                }
                view.findViewById(R.id.webPage).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(webPage.webPage.url));
                        try {
                            context.startActivity(intent);
                        } catch (Exception e) {}
                    }
                });
                messageLayout.addView(view, messageLayout.getLayoutParams());
                break;

            case TdApi.MessageVideo.CONSTRUCTOR:
                Log.d("mesweb", ""+message.message);
                view = lInflater.inflate(R.layout.chat_video, null);
                final TdApi.MessageVideo video = (TdApi.MessageVideo) message.message;
                setBetterImage(view, new TdApi.PhotoSize[]{video.video.thumb});
                file = video.video.video;

                ((TextView) view.findViewById(R.id.video_info)).setText(getFormedTime(video.video.duration)+", "+getSizeAsString(video.video.video.size));

                if (file.path.length() > 0) {
                    View.OnClickListener onFileClick = new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //Log.d("fileMime = ", file.path + " " + video.video. document.mimeType);
                            Intent intent = new Intent();
                            intent.setAction(android.content.Intent.ACTION_VIEW);
                            intent.setDataAndType(Uri.fromFile(new File(file.path)), "video/mp4");//document.mimeType);
                            try {
                                context.startActivity(intent);
                            } catch (Exception e) {}
                        }
                    };

                    final ImageView fileImageView = (ImageView) view.findViewById(R.id.file);
                    fileImageView.setVisibility(View.VISIBLE);
                    fileImageView.setOnClickListener(onFileClick);

                    view.findViewById(R.id.download).setVisibility(View.GONE);
                    /*final ImageView thumb = (ImageView) view.findViewById(R.id.thumb);
                    if (document.thumb.photo.path.length() != 0) {
                        thumb.setVisibility(View.VISIBLE);
                        thumb.setOnClickListener(onFileClick);
                        Picasso.with(context).load(new File(file.path))
                                .resize(40, 40)
                                .centerCrop()
                                .into(thumb);
                    } else {
                        downloadFile(document.thumb.photo.id);
                    }*/
                } else {
                    setDownloadMode(view, file.id, progress, "#ffffff");
                }
                messageLayout.addView(view, messageLayout.getLayoutParams());
                break;
        }

        ImageView imageView = ((ImageView) rootView.findViewById(R.id.avatar));
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProfile(user);
            }
        });
        rootView.findViewById(R.id.chat_name).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProfile(user);
            }
        });

        Drawable drawable = new BitmapDrawable(context.getResources(),
                CircleTransform.makeCircleBitmap(TGFunc.getUserPlaceholder(context, user, 50)));
        Picasso.with(context).load(new File(TGFunc.getFileLocalPath(user.profilePhoto.small)))
                .placeholder(drawable).transform(new CircleTransform()).into(imageView);
        ((TextView) rootView.findViewById(R.id.chat_name)).setText(title);
        ((TextView) rootView.findViewById(R.id.date)).setText(TGFunc.getFormedTime(message.date * 1000L));

        final RelativeLayout replyLayout = (RelativeLayout) rootView.findViewById(R.id.replyLayout);
        final View replyLine = rootView.findViewById(R.id.replyLine);

        if (message.replyToMessageId != 0) {
            for (TdApi.Message cachedMessage : cachedMessages) {
                if (message.replyToMessageId == cachedMessage.id) {
                    for (TdApi.User cachedUser : cachedUsers) {
                        if (cachedMessage.fromId == cachedUser.id) {
                            replyLayout.setVisibility(View.VISIBLE);
                            replyLine.setVisibility(View.VISIBLE);
                            replyLayout.addView(getMessageView(context, cachedUser, cachedMessage, progress, true));
                            return rootView;
                        }
                    }
                }
            }

            final Client client = TG.getClientInstance();
            if (client != null)
                client.send(new TdApi.GetMessage(message.chatId, message.replyToMessageId), new Client.ResultHandler() {
                    @Override
                    public void onResult(final TdApi.TLObject object) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                 if (object instanceof TdApi.Message) {
                                    final TdApi.Message newMessage = (TdApi.Message) object;
                                    cachedMessages.add(newMessage);
                                    boolean isHasUser = false;
                                    for (TdApi.User cachedUser : cachedUsers) {
                                        if (newMessage.fromId == cachedUser.id) {
                                            replyLayout.setVisibility(View.VISIBLE);
                                            replyLine.setVisibility(View.VISIBLE);
                                            replyLayout.addView(getMessageView(context, cachedUser, newMessage, progress, true));
                                            isHasUser = true;
                                            break;
                                        }
                                    }
                                    if (!isHasUser)
                                        client.send(new TdApi.GetUser(newMessage.fromId), new Client.ResultHandler() {
                                            @Override
                                            public void onResult(final TdApi.TLObject object) {
                                                new Handler(Looper.getMainLooper()).post(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        if (object instanceof TdApi.User) {
                                                            cachedUsers.add((TdApi.User) object);
                                                            replyLayout.setVisibility(View.VISIBLE);
                                                            replyLine.setVisibility(View.VISIBLE);
                                                            replyLayout.addView(getMessageView(context, (TdApi.User) object, newMessage, progress, true));
                                                        }
                                                    }
                                                });
                                            }
                                        });
                                }
                            }
                        });
                    }
                });
        }

        if (message.forwardFromId != 0) {
            for (TdApi.User cachedUser : cachedUsers) {
                if (message.forwardFromId == cachedUser.id) {
                    replyLine.setVisibility(View.VISIBLE);
                    replyLayout.setVisibility(View.VISIBLE);
                    replyLayout.addView(getForwardMessage(cachedUser, message, progress));
                    return rootView;
                }
            }
            Client client = TG.getClientInstance();
            if (client != null)
                client.send(new TdApi.GetUser(message.forwardFromId), new Client.ResultHandler() {
                    @Override
                    public void onResult(final TdApi.TLObject object) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                if (object instanceof TdApi.User) {
                                    cachedUsers.add((TdApi.User) object);
                                    replyLine.setVisibility(View.VISIBLE);
                                    replyLayout.setVisibility(View.VISIBLE);
                                    replyLayout.addView(getForwardMessage((TdApi.User) object, message, progress));
                                }
                            }
                        });
                    }
                });
        }
        return rootView;
    }

    public static View getForwardMessage(TdApi.User user, TdApi.Message message, Float progress) {
        TdApi.Message newMessage = new TdApi.Message(message.id, message.fromId, message.chatId,
                message.forwardDate, 0, 0, message.replyToMessageId, message.message, message.replyMarkup);
        return getMessageView(context, user, newMessage, progress, true);
    }

    public static View getMessageAudioView(final TdApi.Message message, Float progress) {
        LayoutInflater lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = lInflater.inflate(R.layout.chat_audio, null);
        TdApi.Audio audio = ((TdApi.MessageAudio) message.message).audio;
        ((TextView) view.findViewById(R.id.title)).setText(audio.title);
        ((TextView) view.findViewById(R.id.performer)).setText(audio.performer);
        final TdApi.File file = audio.audio;

        if (file.path.length() > 0) {
            final MusicPlayer service = MusicPlayer.getInstance();
            final ImageView play = (ImageView) view.findViewById(R.id.play);
            final ImageView pause = (ImageView) view.findViewById(R.id.pause);

            view.findViewById(R.id.shadow_blue).setVisibility(View.VISIBLE);

            if (service.isPlaying() && message.id == service.getMessageId()) {
                pause.setVisibility(View.VISIBLE);
            } else {
                play.setVisibility(View.VISIBLE);
            }
            play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pause.setVisibility(View.VISIBLE);
                    play.setVisibility(View.GONE);
                    if (MediaController.getInstance().isPlayingAudio()) MediaController.getInstance().pauseAudio();
                    service.playAudio(message);
                    invalidateListView(play);
                }
            });
            pause.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pause.setVisibility(View.GONE);
                    play.setVisibility(View.VISIBLE);
                    service.pauseAudio();
                    invalidateListView(play);
                }
            });
        } else {
            setDownloadMode(view, file.id, progress);
        }
        return view;
    }

    private static void invalidateListView(View view) {
        ViewParent parent = view.getParent();
        boolean isListView = false;
        int count = 0;
        while (!isListView && count < 10) {
            if (parent instanceof ListView) {
                isListView = true;
                ((ListView) parent).invalidateViews();
                Log.d("iLV", "" + count);
            } else {
                parent = parent.getParent();
            }
            count++;
        }
    }

    private static ClickableSpan getUserSpan(final TdApi.User user) {
        return new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Log.d("userSpanProfile", "click");
                showProfile(user);
            }

            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
            }
        };
    }

    private static void setDownloadMode(final View view, final int id, final Float progress) {
        setDownloadMode(view, id, progress, "#569ace");
    }

    private static void setDownloadMode(final View view, final int id, final Float progress, String color) {
        if (progress == 0) {
            view.findViewById(R.id.download).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    downloadFile(id);
                    setLoadingMode(view, id, progress);
                }
            });
        } else {
            setLoadingMode(view, id, progress);
        }
    }

    private static void setLoadingMode(final View view, final int id, Float progress) {
        setLoadingMode(view, id, progress, "#569ace");
    }

    private static void setLoadingMode(final View view, final int id, Float progress, String color) {
        if (progress == 0) progress = 1f;
        view.findViewById(R.id.download).setVisibility(View.INVISIBLE);
        view.findViewById(R.id.download_pause).setVisibility(View.VISIBLE);
        final CircleProgressBar progressBar = (CircleProgressBar) view.findViewById(R.id.progressBar);
        progressBar.setColor(Color.parseColor(color));//"#569ace"));
        progressBar.setProgress(progress);

        view.findViewById(R.id.download_pause).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelDownloadFile(id);
                view.findViewById(R.id.download).setVisibility(View.VISIBLE);
                view.findViewById(R.id.download_pause).setVisibility(View.INVISIBLE);
                progressBar.setColor(Color.TRANSPARENT);
                progressBar.setProgress(0);
            }
        });
    }

    private static void setSizeToImageView(Context context, ImageView imageView, TdApi.PhotoSize item, int color) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Log.d("imageView message nat ", "w = " + item.width + " h = " + item.height);

        float sizeSide = (float) getSmaller(size.x, size.y);

        float coef = item.width / sizeSide;
        float width = sizeSide, height = item.height / coef;
        Resources r = context.getResources();
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, r.getDisplayMetrics());
        coef = (width - px) / width;

        imageView.getLayoutParams().width = (int) (width * coef);
        imageView.getLayoutParams().height = (int) (height * coef);
        imageView.setBackgroundColor(color);
        imageView.requestLayout();

        Log.d("imageView message ", "w = " + imageView.getLayoutParams().width + " h = " + imageView.getLayoutParams().height);
    }

    private static void setSizeToSticker(Context context, ImageView imageView, TdApi.PhotoSize item, int color) {
        Log.d("imageView message natur", "w = " + item.width + " h = " + item.height);

        if (item.width == 0 || item.height == 0) return;

        imageView.getLayoutParams().width = item.width / (item.height / Util.dp(160));
        imageView.getLayoutParams().height = Util.dp(160);
        imageView.setBackgroundColor(color);
        imageView.requestLayout();

        Log.d("imageView message ", "w = " + imageView.getLayoutParams().width + " h = " + imageView.getLayoutParams().height);
    }


    public static TdApi.PhotoSize getSmallestPhoto(TdApi.PhotoSize[] photos) {
        //if (photos.length == 0) return -1;
        int pos0 = 0, pos1 = -1;
        for (int i = 0; i < photos.length; i++) {
            if (photos[i].height >= 200 && photos[i].height <= 500) {
                pos1 = i;
            }
            if (photos[i].height < 200) {
                pos0 = i;
            }
        }
        if (pos1 == -1) {
            return photos[pos0];
        } else {
            return photos[pos1];
        }
    }

    public static TdApi.PhotoSize getLargestPhoto(TdApi.PhotoSize[] photos) {
        //if (photos.length == 0) return -1;
        int height = 0, pos = 0;
        for (int i = 0; i < photos.length; i++) {
            if (photos[i].height > height) {
                height = photos[i].height;
                pos = i;
            }
        }
        return photos[pos];
    }


    public static TdApi.PhotoSize getBetterPhoto(TdApi.PhotoSize[] photos) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int bigSideSize = getBigger(size.x, size.y);

        for (int i = 0; i < photos.length; i++) {
            int photoSideSize = photos[i].height > photos[i].width ? photos[i].height : photos[i].width;
            if (photoSideSize > bigSideSize) {
                return photos[i];
            }
        }
        return getLargestPhoto(photos);
    }

    public static int getBigger(int a, int b) {
        return a > b ? a : b;
    }

    public static int getSmaller(int a, int b) {
        return a < b ? a : b;
    }

    public static Bitmap getChatPlaceholder(Context context, TdApi.Chat chat, int sideSize) {
        Bitmap bitmap = null;
        if (chat.type instanceof TdApi.PrivateChatInfo) {
            TdApi.User user = ((TdApi.PrivateChatInfo) chat.type).user;
            bitmap = getUserPlaceholder(context, user, sideSize);
        } else if (chat.type instanceof TdApi.GroupChatInfo) {
            TdApi.GroupChat groupChat = ((TdApi.GroupChatInfo) chat.type).groupChat;
            bitmap = getPlaceholder(context, getPlaceholderText(groupChat.title, ""), (int) chat.id, sideSize);
        }
        return bitmap;
    }

    public static Bitmap getUserPlaceholder(Context context, TdApi.User user, int sideSize) {
        return getPlaceholder(context, getPlaceholderText(user.firstName, user.lastName), user.id, sideSize);
    }

    private static String getPlaceholderText(String str1, String str2) {
        String str = "";
        if (str2.length() == 0) {
            if (str1.length() > 0) str = str1.substring(0, 1);
        } else {
            if (str1.length() > 0) str = str1.substring(0, 1);
            if (str2.length() > 0) str += str2.substring(0, 1);
        }
        return str;
    }

    private static Bitmap getPlaceholder(Context context, String shortTitle, int id, int sideSize) {
        //float scale = context.getResources().getDisplayMetrics().density;
        int width = sideSize;
        int height = sideSize;

        int[] colors = {0x7fcbd9, 0x70b2d8, 0x7bcb81, 0xe48283, 0xeea781, 0xef83ac, 0xe6c26c};

        id = Math.abs(id % 10);
        if (id > 6) {
            id = 9 - id;
        }

        int color = colors[id];
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        bitmap.eraseColor(Color.rgb((color >> 16) & 0xFF, (color >> 8) & 0xFF, (color >> 0) & 0xFF));

        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.WHITE);
        paint.setTextSize(18.0f);//Util.dp(18));//int) (18 * scale));
        paint.setShadowLayer(1f, 0f, 1f, Color.WHITE);

        Rect bounds = new Rect();
        paint.getTextBounds(shortTitle, 0, shortTitle.length(), bounds);
        int x = (bitmap.getWidth() - bounds.width()) / 2;
        int y = (bitmap.getHeight() + bounds.height()) / 2;

        canvas.drawText(shortTitle.toUpperCase(), x, y, paint);

        return bitmap;
    }

    public static String getCountry(Context context, String code) {
        String[] lines = getLines(context);
        for (int i = lines.length - 1; i >= 0; i--) {
            String line = lines[i];
            if (code.equals("+" + line.substring(0, line.indexOf(";")))) {
                return line.substring(line.lastIndexOf(";") + 1, line.length());
            }
        }
        return null;
    }

    public static String[] getCountries() {
        String[] lines = getLines(context);
        ArrayList<String> items = new ArrayList<String>();
        for (int i = 0; i < lines.length; i++) {
            String line = lines[i];
            items.add(line.substring(line.lastIndexOf(";") + 1, line.length()));
        }
        Collections.sort(items);
        return items.toArray(new String[items.size()]);
    }

    public static String[] getCodes() {
        String[] lines = getLines(context);
        ArrayList<String> items = new ArrayList<String>();
        for (int i = 0; i < lines.length; i++) {
            String line = lines[i];
            line = line.substring(line.lastIndexOf(";") + 1, line.length()) + ";" + line.substring(0, line.indexOf(";"));
            items.add(line);
        }
        Collections.sort(items);
        for (int i = 0; i < items.size(); i++) {
            String line = items.get(i);
            line = line.substring(line.indexOf(";") + 1, line.length());
            items.set(i, line);
        }
        return items.toArray(new String[items.size()]);
    }

    public static String[] getLines(Context context) {
        AssetManager assetManager = context.getAssets();

        String[] lines = null;
        InputStream input;
        try {
            input = assetManager.open("countries.txt");
            int size = input.available();
            byte[] buffer = new byte[size];
            input.read(buffer);
            input.close();

            String string = new String(buffer);
            lines = string.split(System.getProperty("line.separator"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }

    public static void setToolbar(FragmentActivity activity, View rootView, String title, boolean backButton) {
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        toolbar.setTitle(title);
        ((AppCompatActivity) activity).setSupportActionBar(toolbar);
        if (backButton) {
            ActionBar actionBar = ((AppCompatActivity) activity).getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(false);
        }
    }

    public static String getFormedDateForMessage(long unixDate) {
        Calendar c = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("yyyy");
        Log.d("Dates ", "" + c.get(Calendar.YEAR) + " " + dateFormat.format(new Date(unixDate)));
        if (c.get(Calendar.YEAR) == Integer.parseInt(dateFormat.format(new Date(unixDate)))) {
            dateFormat = new SimpleDateFormat("MMMM dd");
            return dateFormat.format(new Date(unixDate));
        } else {
            dateFormat = new SimpleDateFormat("MMMM dd, yyyy");
            return dateFormat.format(new Date(unixDate));
        }
    }

    public static String getFormedDateForChat(long unixDate) {
        if (DateUtils.isToday(unixDate)) {
            return getFormedTime(unixDate);
        } else if (unixDate + 6 * 24 * 60 * 60 * 1000 >= Calendar.getInstance().getTimeInMillis()) {
            return new SimpleDateFormat("EEE").format(new Date(unixDate));
        } else {
            return getFormedDate(unixDate);
        }
    }

    public static String getFormedDate(long unixDate) {
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");
        return dateFormat.format(new Date(unixDate));
    }

    public static String getFormedTime(long unixDate) {
        DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        return dateFormat.format(new Date(unixDate));
    }

    public static boolean isOnline(Context context) {
        if (context != null) {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnectedOrConnecting()) {
                return true;
            }
        }
        return false;
    }

    static int cacheSize = 10000 * 1024 * 8;
    static LruCache<String, Bitmap> mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
        @Override
        protected int sizeOf(String key, Bitmap bitmap) {
            return bitmap.getByteCount();
        }
    };

    public static void loadSticker(String path, ImageView imageView) {
        Bitmap bitmap = getBitmapFromMemCache(path);
        if (bitmap == null) {
            new LoadingWebP(context, path, imageView).execute();
        } else {
            imageView.setImageBitmap(bitmap);
        }
    }

    public static void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    public static Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }


    public static void updateUser(TdApi.User user, TdApi.User updateUser) {
        user.firstName = updateUser.firstName;
        user.lastName = updateUser.lastName;
        user.username = updateUser.username;
        user.status = updateUser.status;
        user.profilePhoto = updateUser.profilePhoto;
        user.myLink = updateUser.myLink;
        user.foreignLink = updateUser.foreignLink;
        user.type = updateUser.type;
    }

    public static String getFormedDuration(int duration) {
        return duration / 60 + ":" + (duration % 60 < 10 ? "0" : "") + duration % 60;
    }

    public static void downloadFile(final int id) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Client client = TG.getClientInstance();
                if (client == null) return;
                client.send(new TdApi.DownloadFile(id), new Client.ResultHandler() {
                    @Override
                    public void onResult(TdApi.TLObject object) {
                    }
                });
            }
        });
    }

    public static void cancelDownloadFile(int id) {
        Client client = TG.getClientInstance();
        if (client == null) return;
        client.send(new TdApi.CancelDownloadFile(id), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
            }
        });
    }

    public static void updatePlayer(final Context context, final View view) {
        if (context == null) return;
        final MusicPlayer service = MusicPlayer.getInstance();//((MainActivity) context).getService();;
        LinearLayout musicBar = null;

        if (service == null) {
            return;
        }
        if (view == null) return;

        if (musicBar == null) {
            musicBar = (LinearLayout) view.findViewById(R.id.music_bar);
            if (musicBar == null) return;
        }
        if (service.showPlayer && musicBar.getVisibility() == View.GONE) {
            musicBar.setVisibility(View.VISIBLE);
        } else if (!service.showPlayer && musicBar.getVisibility() == View.VISIBLE) {
            musicBar.setVisibility(View.GONE);
        }

        view.findViewById(R.id.music_bar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = ((AppCompatActivity) context).getSupportFragmentManager();
                if (Util.isTablet) {
                    ((AppCompatActivity) context).findViewById(R.id.container_layout_2).setVisibility(View.VISIBLE);
                    fragmentManager.beginTransaction().add(R.id.container, new MusicPlayerFragment(), "music_player").addToBackStack(null).commit();
                } else {
                    fragmentManager.beginTransaction().setCustomAnimations(R.anim.enter_from_right, 0, 0, R.anim.exit_to_right).add(R.id.container, new MusicPlayerFragment(), "music_player").addToBackStack(null).commit();
                }
            }
        });

        ((TextView) musicBar.findViewById(R.id.mb_title)).setText(service.audioTitle);

        Point size = new Point();
        ((MainActivity) context).getWindowManager().getDefaultDisplay().getSize(size);

        musicBar.findViewById(R.id.progressView).getLayoutParams().width =
                service.getPlayerPercentProgress() * (size.x / 100);
        musicBar.findViewById(R.id.progressView).requestLayout();

        if (service.isPlaying()) {
            musicBar.findViewById(R.id.mb_play).setVisibility(View.GONE);
            musicBar.findViewById(R.id.mb_pause).setVisibility(View.VISIBLE);
        } else {
            musicBar.findViewById(R.id.mb_play).setVisibility(View.VISIBLE);
            musicBar.findViewById(R.id.mb_pause).setVisibility(View.GONE);
        }

        musicBar.findViewById(R.id.mb_play).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MediaController.getInstance().isPlayingAudio()) MediaController.getInstance().pauseAudio();
                service.playAudio();
                ((ListView) view.findViewById(R.id.listView)).invalidateViews();
            }
        });

        musicBar.findViewById(R.id.mb_pause).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                service.pauseAudio();
                ((ListView) view.findViewById(R.id.listView)).invalidateViews();
            }
        });

        musicBar.findViewById(R.id.mb_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                service.showPlayer = false;
            }
        });
    }

    public static Context context;

    public static String getUserStatus(TdApi.UserStatus status) {
        switch (status.getConstructor()) {
            case TdApi.UserStatusOnline.CONSTRUCTOR:
                return context.getString(R.string.online);
            case TdApi.UserStatusRecently.CONSTRUCTOR:
                return context.getString(R.string.recently);
            case TdApi.UserStatusOffline.CONSTRUCTOR:
                long unixDate = ((TdApi.UserStatusOffline) status).wasOnline * 1000L;
                Log.d("unixDate", "" + unixDate);
                Calendar c = Calendar.getInstance();
                Date date = new Date(unixDate);
                Calendar cur = Calendar.getInstance();
                cur.setTime(date);
                if (DateUtils.isToday(unixDate)) {
                    return context.getString(R.string.last_seen_today) + " " +
                            new SimpleDateFormat("HH:mm").format(new Date(unixDate));
                } else if (DateUtils.isToday(unixDate + 24 * 60 * 60 * 1000)) {
                    return context.getString(R.string.last_seen_yesterday) + " " +
                            new SimpleDateFormat("HH:mm").format(new Date(unixDate));
                } else if (c.get(Calendar.YEAR) == cur.get(Calendar.YEAR)) {
                    return context.getString(R.string.last_seen) + " " +
                            new SimpleDateFormat("MMM d 'at' HH:mm").format(new Date(unixDate));
                } else {
                    return context.getString(R.string.last_seen) + " at " +
                            new SimpleDateFormat("dd.MM.yy").format(new Date(unixDate));
                }
        }
        return "";
    }

    public static String getChatStatus(ArrayList<TdApi.User> users) {
        Log.d("getChatStatus", "" + getPreferences(context).getInt("userId", 0));
        int online = 0;
        boolean isUserOnline = false;
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).status instanceof TdApi.UserStatusOnline) {
                if (users.get(i).id == getPreferences(context).getInt("userId", 0)) {
                    isUserOnline = true;
                } else {
                    online++;
                }
            }
        }
        String subTitle = "" + (users.size()) + " " + context.getString(users.size() == 1 ? R.string.member : R.string.members);
        if (online != 0) {
            //if (isUserOnline)
            online++;
            subTitle += ", " + online + " " + context.getString(R.string.online);
        }
        return subTitle;
    }

    public static void showLastImages(final View rootView, final ArrayList<TdApi.Message> messages, final View.OnClickListener listener) {
        /*new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {*/

        LinearLayout linearLayout = (LinearLayout) rootView.findViewById(R.id.lastImages);
        linearLayout.setOnClickListener(listener);

        int count = 0;
        while (messages.size() > count && count != 20) {
            if (messages.get(count).message instanceof TdApi.MessagePhoto) {
                LayoutInflater lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                RelativeLayout relativeLayout = (RelativeLayout) lInflater.inflate(R.layout.attach_item, null);
                relativeLayout.setTag(count);

                final ImageView imageView = (ImageView) relativeLayout.findViewById(R.id.imageView);
                imageView.setAdjustViewBounds(true);
                String imageLocation = TGFunc.getSmallestPhoto(((TdApi.MessagePhoto) messages.get(count).message).photo.photos).photo.path;//cursor.getString(1);

                Picasso.with(context).load(new File(imageLocation))
                        .resize(Util.dp(82), Util.dp(82))
                        .centerCrop()
                        .into(imageView);
                linearLayout.addView(relativeLayout);
            }
            count++;
        }

            //}}, 350);*/
    }

    public static void downloadPreviewPhoto(ArrayList<TdApi.Message> messages) {
        for (int i = 0; i < messages.size(); i++) {
            if (messages.get(i).message != null) {
                TdApi.MessageContent message = messages.get(i).message;
                switch (message.getConstructor()) {
                    case TdApi.MessagePhoto.CONSTRUCTOR:
                        TdApi.PhotoSize[] photos = ((TdApi.MessagePhoto) message).photo.photos;
                        downloadFile(TGFunc.getSmallestPhoto(photos).photo.id);
                        break;
                }
            }
        }
    }

    public static void showSoftKeyboard(Activity activity) {
        if (activity == null) return;
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public static void hideSoftKeyboard(Activity activity) {
        if (activity == null || activity.getCurrentFocus() == null) return;
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    private static void searchUser(String username) {
        Log.d("searchUser", username);
        Client client = TG.getClientInstance();
        if (client == null) return;
        client.send(new TdApi.SearchUser(username), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                Log.d("SearchUser", "" + object.toString());
                if (object instanceof TdApi.User) {
                    showProfile((TdApi.User) object);
                } else {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(context, R.string.no_such_user, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }

    private static void showProfile(final TdApi.User user) {
        if (user.type instanceof TdApi.UserTypeBot) {
            Client client = TG.getClientInstance();
            if (client == null) return;
            client.send(new TdApi.GetUserFull(user.id), new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {
                    Log.d("GetUserFull", "" + object.toString());
                    if (object instanceof TdApi.UserFull) {
                        showProfile(user, ((TdApi.UserFull) object).botInfo);
                    }
                }
            });
        } else {
            showProfile(user, null);
        }
    }

    private static void showProfile(TdApi.User user, TdApi.BotInfo botInfo) {
        ProfileFragment fragment = new ProfileFragment();
        fragment.user = user;
        fragment.botInfo = botInfo;
        if (Util.isTablet) {
            ((AppCompatActivity) context).findViewById(R.id.container_layout_2).setVisibility(View.VISIBLE);
        }
        ((AppCompatActivity) context).getSupportFragmentManager().
                beginTransaction().add(R.id.container, fragment, "profile").addToBackStack(null).commit();

    }

    public static class URLSpanNoUnderline extends URLSpan {
        public URLSpanNoUnderline(String url) {
            super(url);
        }
        @Override public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    }

    public static void showNotificationPopupMenu(final View menuItemView, final long chatId, final TdApi.NotificationSettings notificationSettings) {
        PopupMenu popup = new PopupMenu(context, menuItemView);
        popup.getMenuInflater().inflate(R.menu.popup_notification, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.enabled:
                        notificationSettings.muteFor = 0;
                        break;
                    case R.id.mute_for_1_hour:
                        notificationSettings.muteFor = 60*60;
                        break;
                    case R.id.mute_for_8_hours:
                        notificationSettings.muteFor = 8*60*60;
                        break;
                    case R.id.mute_for_2_days:
                        notificationSettings.muteFor = 2*24*60*60;
                        break;
                    case R.id.disable:
                        notificationSettings.muteFor = 365*24*60*60;
                        break;
                }
                Client client = TG.getClientInstance();
                if (client != null) {
                    client.send(new TdApi.SetNotificationSettings(new TdApi.NotificationSettingsForChat(chatId), notificationSettings), new Client.ResultHandler() {
                        @Override
                        public void onResult(TdApi.TLObject object) {
                            Log.d("SetNotificationSettings", object.toString());
                        }
                    });
                }
                ((Activity)context).invalidateOptionsMenu();
                MessagesFragment fragment = (MessagesFragment)((AppCompatActivity) context).getSupportFragmentManager().findFragmentByTag("messages");
                 if (fragment != null) {
                    fragment.updateListView();
                }
                return true;
            }
        });
        popup.show();
    }

    public static void showContainer(Fragment fragment) {
        if (Util.isTablet) {
            fragment.getActivity().findViewById(R.id.container_layout_2).setVisibility(View.VISIBLE);
        }
    }

    public static int getChatWidth() {
        Point size = new Point();
        //((Activity)context).getWindowManager().getDefaultDisplay().getSize(size);
        int width = context.getResources().getDisplayMetrics().widthPixels;// size.x;
        if (Util.isTablet) {
            int leftWidth = context.getResources().getDisplayMetrics().widthPixels / 100 * 35;
            if (leftWidth < Util.dp(320)) {
                leftWidth = Util.dp(320);
            }
            return width - leftWidth;
        }
        return width;
    }

    private static void setBetterImage(View view, TdApi.PhotoSize[] photos) {
        ImageView image = (ImageView) view.findViewById(R.id.image);
        //final TdApi.PhotoSize[] photos = ((TdApi.MessagePhoto) message.message).photo.photos;
        setSizeToImageView(context, image, getLargestPhoto(photos), Color.LTGRAY);
        TdApi.PhotoSize betterPhotoSize = getBetterPhoto(photos);

        int rw = betterPhotoSize.width, rh = betterPhotoSize.height;
        if (rw == 0 || rh == 0) {
            rw = Util.dp(100);
            rh = Util.dp(100);
        }

        String photoPath = (betterPhotoSize.photo.path.length() != 0 ? betterPhotoSize : getSmallestPhoto(photos)).photo.path;
        Picasso.with(context).load(new File(photoPath))
                .resize(rw, rh).centerCrop()
                .into(image);
    }

    private static String getFormedTime(int sec) {
        int hours = sec / 3600;
        int minutes = (sec % 3600) / 60;
        int seconds = sec % 60;
        if (hours > 0) {
            return String.format("%02d:%02d:%02d", hours, minutes, seconds);
        }
        if (minutes > 0) {
            return String.format("%02d:%02d", minutes, seconds);
        }
        return String.format("%01d:%02d", minutes, seconds);
    }

    private static String getSizeAsString(long bytes) {
        String[] Q = new String[] { "Bytes", "Kb", "Mb", "Gb", "T", "P", "E" };
        for (int i = 6; i >= 0; i--) {
            double step = Math.pow(1024, i);
            if (bytes > step)
                return String.format("%3.2f %s", bytes / step, Q[i]);
        }
        return "0.00 "+Q[0];
    }

}