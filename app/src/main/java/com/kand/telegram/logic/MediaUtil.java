package com.kand.telegram.logic;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.util.Log;

import java.nio.ByteBuffer;

public class MediaUtil {

    private static MediaUtil mediaUtil;
    private Thread threadRecorder;
    private AudioRecord audioRecorder;
    private int recordBufferSize, playerBufferSize;

    private int loudnessRecord;


    public MediaUtil() {
        recordBufferSize = AudioRecord.getMinBufferSize(16000, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
        if (recordBufferSize <= 0) {
            recordBufferSize = 1280;
        }
        playerBufferSize = AudioTrack.getMinBufferSize(48000, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT);
        if (playerBufferSize <= 0) {
            playerBufferSize = 3840;
        }
    }

    public static MediaUtil getInstance() {
        if (mediaUtil == null) {
            mediaUtil = new MediaUtil();
        }
        return mediaUtil;
    }

    public void startRecording() {
        Util.startRecord(TGFunc.getDir() + "temp_voice.ogg");
        audioRecorder = new AudioRecord(MediaRecorder.AudioSource.MIC, 16000, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, recordBufferSize * 10);
        audioRecorder.startRecording();
        threadRecorder = new Thread(new Runnable() {
            @Override
            public void run() {
                while (!threadRecorder.isInterrupted()) {
                    ByteBuffer buffer = ByteBuffer.allocateDirect(recordBufferSize);
                    int len = audioRecorder.read(buffer, buffer.capacity());
                    Log.d("len mic", "" + len);
                    Log.d("byte mic", buffer.toString());
                    loudnessRecord = 0;
                    for (int i = 0; i < len; i++) {
                        if (buffer.get(i) > loudnessRecord) {
                            loudnessRecord = buffer.get(i);
                        }
                    }
                    Util.writeFrame(buffer, len);
                }
            }
        });
        threadRecorder.start();
    }

    public void stopRecording() {
        try {
            threadRecorder.interrupt();
            threadRecorder.join();
            audioRecorder.stop();
            Util.stopRecord();
        } catch (Exception e) {}
    }

    public int getLoudnessRecord() {
        return loudnessRecord;
    }

}
