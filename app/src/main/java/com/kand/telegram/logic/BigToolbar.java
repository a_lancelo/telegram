package com.kand.telegram.logic;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kand.telegram.R;
import com.squareup.picasso.Picasso;

import org.drinkless.td.libcore.telegram.TdApi;

import java.io.File;

public class BigToolbar implements AbsListView.OnScrollListener {

    private int headerHeight = Util.dp(180);
    private Context context;
    private View container;
    private TextView title, subtitle;
    private ImageView image, floating;
    private ListView listView;

    public BigToolbar(Context context) {
        this.context = context;
    }

    public BigToolbar show(View rootView, String title, String subtitle, final TdApi.ProfilePhoto profilePhoto, Drawable placeholder, int resId) {
        TGFunc.setToolbar((FragmentActivity)context, rootView, "", true);
        listView = (ListView) rootView.findViewById(R.id.listView);
        container = rootView.findViewById(R.id.container);
        (this.title = (TextView) rootView.findViewById(R.id.title)).setText(title);
        (this.subtitle = (TextView) rootView.findViewById(R.id.subtitle)).setText(subtitle);
        (floating = (ImageView) rootView.findViewById(R.id.floating_button)).setImageResource(resId);
        Picasso.with(context).load(new File(profilePhoto.small.path)).placeholder(placeholder).
                transform(new CircleTransform()).into(image = (ImageView) rootView.findViewById(R.id.image));
        listView.setOnScrollListener(this);
        onScroll(listView, 0, 0, 0);
        return this;
    }

    public ImageView getFloatingButton() {
        return floating;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {}


    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (container.getHeight() == 0) return;
        int height = 0;
        View child = view.getChildAt(0);
        int top = child != null && firstVisibleItem == 0 ? child.getTop() : - Util.dp(120);
        height = headerHeight + top;

        if (height < Util.dp(60)) {
            ((FrameLayout.LayoutParams)container.getLayoutParams()).bottomMargin = 0;
        } else if (height > Util.dp(90)) {
            ((FrameLayout.LayoutParams)container.getLayoutParams()).bottomMargin = Util.dp(30);
        } else {
            ((FrameLayout.LayoutParams)container.getLayoutParams()).bottomMargin = height - Util.dp(60);
        }

        if (height > Util.dp(90)) {
            floating.setVisibility(View.VISIBLE);
        } else {
            floating.setVisibility(View.GONE);
        }

        height -= ((FrameLayout.LayoutParams)container.getLayoutParams()).bottomMargin;
        container.getLayoutParams().height = height;// - Util.dp(30);// + minHeaderHeight;
        changeMargins(height);

        container.requestLayout();
    }

    public void changeMargins(int height) {
        height -= Util.dp(60);
        float s = 120;

        float a = Util.dp(45) + 15/s * height;//40
        float imageX = Util.dp(40) - (40/s * height) + Util.dp(16);
        float imageY = Util.dp(8) + 82/s * height;//10 52

        image.setLayoutParams(new RelativeLayout.LayoutParams((int) a, (int) a));
        image.setX(imageX);
        image.setY(imageY);

        title.setX(imageX + a + Util.dp(15));
        title.setY(imageY + a / 2 - Util.dp(25));
        subtitle.setX(imageX + a + Util.dp(15));
        subtitle.setY(imageY + a / 2 + Util.dp(5));

        Log.d("bigT", "img "+imageX+" "+imageY+" "+ (imageX + a + Util.dp(15))+" title "+ (imageY + a / 2 - Util.dp(25))+ " subt "+(imageY + a / 2 + Util.dp(5)));
    }

}
