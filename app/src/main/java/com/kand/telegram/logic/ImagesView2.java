package com.kand.telegram.logic;

import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.kand.telegram.R;

import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;

public class ImagesView2 extends FrameLayout {

    private int pos;
    private float fx, fy, cx, cy, scale = 1.0f, pScale, fDiffZoom, smx, smy;
    private boolean viewMoving, slideToRight, isMoving, isZooming;
    private Bitmap bitmap;
    private ArrayList<TdApi.Message> messages;
    private ImageView imageView1, imageView2, imageView3;

    public ImagesView2(Context context, ArrayList<TdApi.Message> messages, int pos) {
        super(context);
        this.messages = messages;
        this.pos = pos;
        imageView1 = new ImageView(context);
        imageView2 = new ImageView(context);
        imageView3 = new ImageView(context);
        setBackgroundColor(Color.parseColor("#000000"));
        /*imageView1.setBackgroundColor(Color.parseColor("#000000"));
        imageView2.setBackgroundColor(Color.parseColor("#000000"));
        imageView3.setBackgroundColor(Color.parseColor("#000000"));*/
        LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParams.gravity = Gravity.CENTER;
        setLayoutParams(layoutParams);
        imageView1.setLayoutParams(layoutParams);
        imageView2.setLayoutParams(layoutParams);
        imageView3.setLayoutParams(layoutParams);
        addView(imageView3);
        addView(imageView2);
        addView(imageView1);
        //imageView1.setVisibility(GONE);

        Point size = new Point();
        ((Activity)context).getWindowManager().getDefaultDisplay().getSize(size);
        int w = size.x;//getRootView().getMeasuredWidth();
        //h = getRootView().getMeasuredHeight();
        //hm = Util.dp(20);
        imageView1.setX(-w);//-getMeasuredWidth());
        imageView1.setAlpha(255);
        imageView3.setAlpha(255);
        Log.d("ImagesViewW", "" + w + " " + imageView1.getX());//getMeasuredHeight());
        Log.d("SizeMes", "" + messages.size());// w + " " + imageView1.getX());//getMeasuredHeight());
        //messages.get(pos);
        loadImages();
        //if (path != null) loadImage(path);
        /*if (big.path.length() == 0) {
            TGFunc.downloadFile(big.id);
            rootView.findViewById(R.id.linearLayout).setVisibility(View.VISIBLE);
        } */
        //imageView2.
    }

    public ImagesView2(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int dx = (int)(event.getX() - fx);//Math.max(0, (int) (event.getX() - fx));
        Log.d("ImagesView", "" + event.getX() + " " + event.getY() + " " + event.getAction() + " " + viewMoving);
        Log.d("ImagesView 2", "" + event.getAction() + " " + viewMoving + " " + dx +" "+event.getPointerCount());
        checkDoubleTap(event);
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN:
                fx = event.getX();
                fy = event.getY();
                /*if (event.getPointerCount() == 2) {
                    isZooming = true;
                } else {
                    isZooming = false;
                }*/
                break;
            case MotionEvent.ACTION_MOVE:
                cx = event.getX();
                viewMoving = true;

                if (event.getPointerCount() == 2) {
                    if (!isZooming) {
                        isZooming = true;
                        isMoving = false;
                        pScale = scale;
                        fDiffZoom = event.getX(0) - event.getX(1);
                        if (fDiffZoom < 0) fDiffZoom = -fDiffZoom;
                    }
                }

                if (!isZooming) {
                    if (scale > 1) {
                        isMoving = true;
                    }
                    if (scale == 1) {
                        isMoving = false;
                    }
                }

                if (isMoving) {

                    boolean slideToRight = event.getX() < fx;
                    boolean slideToTop = event.getY() > fy;
                    //Bitmap bitmap = ((BitmapDrawable)imageView2.getDrawable()).getBitmap();
                    //Log.d("moc slide", "" + slideToRight + " " + imageView2.getWidth()+" "+imageView2.getHeight());
                    //Log.d("moc slide b", "" + slideToRight + " " + bitmap.getWidth()+" "+bitmap.getHeight());
                    Log.d("moc slide b", "" + slideToRight + " " + getBitmapWidth()+" "+getBitmapHeight());

                    if ((imageView2.getX() > 0 && slideToRight) || (imageView2.getX() + getMeasuredWidth() < getBitmapWidth() && !slideToRight)) {
                        imageView2.setX(smx + event.getX() - fx);
                    } else {
                        isMoving = false;
                    }

                    Log.d("moc slideT", "" + slideToTop);// + " " + imageView2.getWidth()+" "+imageView2.getHeight()+" "+);

                    if (bitmap.getHeight() > getMeasuredHeight()) {
                        if ((imageView2.getY() > 0 && slideToTop) || (imageView2.getY() + getMeasuredHeight() < getBitmapHeight() && !slideToTop)) {
                            //imageView2.setX(smx + event.getX() - fx);
                            imageView2.setY(smy + event.getY() - fy);
                        }
                    }/*else {
                        isMoving = false;
                    }

                    if (imageView2.getHeight() > getMeasuredHeight()) {

                    }*/

                    //if (smx + event.getX() - fx > 0 || getMeasuredWidth() - (smx + event.getX() - fx) < getMeasuredWidth()) {
                    //   if (smx + event.getX() - fx < 0 || getMeasuredWidth() - (smx + event.getX() - fx) < getMeasuredWidth()) {
                    //   isMoving = false;


                    //imageView2.setY(smy + event.getY() - fy);
                    Log.d("moc", "" +imageView2.getMeasuredWidth()+" "+imageView2.getMeasuredHeight() + " "+(event.getX() - fx) + " " + imageView2.getX() + " " + (event.getY() - fy) + " " + imageView2.getY());
                    //imageView2.setX(imageView2.getX() + getX() - fx);
                    //imageView2.setY(imageView2.getY() + getY() - fy);
                }

                if (isZooming && event.getPointerCount() == 2) {
                    float diffZoom = event.getX(0) - event.getX(1);
                    if (diffZoom < 0) diffZoom = -diffZoom;
                    scale = pScale + 3*(diffZoom - fDiffZoom) / getMeasuredWidth();
                    imageView2.setScaleX(scale);
                    imageView2.setScaleY(scale);//1 + 3*(event.getX(0) - event.getX(1) - fDiffZoom) / getMeasuredWidth());
                }

                if (!isMoving && !isZooming) {
                    slideToRight = event.getX() < fx;
                    //Log.d("ImagesView 3", "slideToRight " + slideToRight);
                /*if (!slideToRight) {// event.getAction()+" "+viewMoving+" "+dx);
                    //moveImage(imageView2, imageView3);
                    imageView3.setVisibility(GONE);
                    //moveImage(imageView1, imageView2);
                    moveImage(imageView1, imageView2);
                }*/
                    slideImage();
                }
                //moveImage(slideToRight ? imageView2 : imageView1, slideToRight ? imageView3 : imageView2);
                break;
            case MotionEvent.ACTION_UP:
                viewMoving = false;
                //findViewById(R.id.imageView_1).setX(0);
                fDiffZoom = 0;

                boolean wasApply = false;
                if (!isZooming && !isMoving) {
                    if (fx - event.getX() > getMeasuredWidth() / 3 && slideToRight) { //*2
                        Log.d("sVi", "r " + (fx - event.getX()) + " " + (getMeasuredWidth() / 3) * 2);
                        wasApply = true;

                        imageView2.animate().x(-getMeasuredWidth()). //getMeasuredWidth() - (fx - event.getX())).
                                setDuration(400).setListener(new Animator.AnimatorListener() {
                            @Override public void onAnimationStart(Animator animation) {}
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                pos--;
                                loadImages();
                                //imageView1.setX(-getMeasuredWidth());
                                imageView2.setX(0);
                                imageView3.setAlpha(255);//Math.max(127, 255 - (255 * (int) (cx - fx) / getMeasuredWidth())));
                                imageView3.setScaleX(1);// (cx - fx) / getMeasuredWidth());
                                imageView3.setScaleY(1);//(cx - fx) / getMeasuredWidth());
                                //imageView3.setX(0);//-getMeasuredWidth());

                            }
                            @Override public void onAnimationCancel(Animator animation) {}
                            @Override public void onAnimationRepeat(Animator animation) {}
                        });
                        imageView3.animate().scaleX(1).scaleY(1).alpha(255).setDuration(400);
                        /*
                        //imageView1.setX(-getMeasuredWidth());
                        imageView2.setX(0);
                        imageView3.setAlpha(255);//Math.max(127, 255 - (255 * (int) (cx - fx) / getMeasuredWidth())));
                        imageView3.setScaleX(1);// (cx - fx) / getMeasuredWidth());
                        imageView3.setScaleY(1);//(cx - fx) / getMeasuredWidth());
                        //imageView3.setX(0);//-getMeasuredWidth());
                        */
                    }

                    if (event.getX() - fx > getMeasuredWidth() / 3 && !slideToRight) {
                        Log.d("sVi", "l " + (event.getX() - fx) + " " + (getMeasuredWidth() / 3));
                        wasApply = true;

                        imageView1.animate().x(0). //getMeasuredWidth() - (event.getX() - fx)).
                                setDuration(400).setListener(new Animator.AnimatorListener() {
                            @Override public void onAnimationStart(Animator animation) {}
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                pos++;
                                loadImages();

                                imageView1.setX(-getMeasuredWidth());
                                imageView2.setX(0);
                                imageView2.setAlpha(255);//Math.max(127, 255 - (255 * (int) (cx - fx) / getMeasuredWidth())));
                                imageView2.setScaleX(1);// (cx - fx) / getMeasuredWidth());
                                imageView2.setScaleY(1);//(cx - fx) / getMeasuredWidth());
                            }
                            @Override public void onAnimationCancel(Animator animation) {}
                            @Override public void onAnimationRepeat(Animator animation) {}
                        });
                        //???
                        imageView2.animate().scaleX(0.5f).scaleY(0.5f).alpha(255).setDuration(400);
                    }
                }

                if (!wasApply) {
                    imageView1.setX(-getMeasuredWidth());
                    if (!isMoving && !isZooming) {
                        imageView2.setX(0);
                        imageView2.setY(0);
                        imageView2.setScaleX(1);
                        imageView2.setScaleY(1);
                    }
                    if (scale < 1) {
                        scale = 1;
                        imageView2.animate().scaleX(scale).scaleY(scale).setDuration(400);
                        //imageView2.setScaleX(scale);
                        //imageView2.setScaleY(scale);
                    }
                    if (scale > 3) {
                        scale = 3;
                        //imageView2.setScaleX(scale);
                        //imageView2.setScaleY(scale);
                        imageView2.animate().scaleX(scale).scaleY(scale).setDuration(400);
                        //imageView2.animate().scaleX(s)
                    }
                    imageView2.setAlpha(255);
                    imageView3.setAlpha(255);
                    imageView3.setScaleX(1);
                    imageView3.setScaleY(1);
                }

                if (isMoving) {
                    /*if (imageView2.getX() < 0) {
                        imageView2.setX(0);
                    }
                    if (imageView2.getMeasuredWidth() + imageView2.getX() < getMeasuredWidth()) {
                        imageView2.setX(-(imageView2.getMeasuredWidth() - getMeasuredWidth()));
                    }*/

                    smx = imageView2.getX();
                    smy = imageView2.getY();
                }

                isMoving = false;
                isZooming = false;
                break;
        }
        return true;// viewMoving;//super.onTouchEvent(event);
    }

    int clickCount = 0, MAX_DURATION = 500;
    long startTime, duration;

    private void checkDoubleTap(MotionEvent event) {
        switch(event.getAction()) { //& MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                startTime = System.currentTimeMillis();
                clickCount++;
                break;
            case MotionEvent.ACTION_UP:
                long time = System.currentTimeMillis() - startTime;
                duration=  duration + time;
                if (clickCount == 2) {
                    if (duration<= MAX_DURATION) {
                        Log.d("IView", "Double");
                        scale = scale <= 1.0 ? 3.0f : 1.0f;
                        imageView2.setScaleX(scale);
                        imageView2.setScaleY(scale);
                    }
                    clickCount = 0;
                    duration = 0;
                    break;
                }
        }
    }

    /*@Override
    protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld){
        super.onSizeChanged(xNew, yNew, xOld, yOld);
        w = xNew;
        h = yNew;
        invalidate();
    }*/

    private void slideImage() {//ImageView imageView, ImageView imageView2) {
        /*imageView.setX(cx - fx - getMeasuredWidth());// - cx - fx);
        imageView2.setAlpha(Math.max(127, 255 - (255 * (int) (cx - fx) / getMeasuredWidth())));
        imageView2.setScaleX((cx - fx) / getMeasuredWidth());
        imageView2.setScaleY((cx - fx) / getMeasuredWidth());*/
        if (!slideToRight && pos+1 != messages.size()) {//  && pos != 0) {
            imageView1.setVisibility(VISIBLE);
            imageView3.setVisibility(GONE);

            Log.d("mi", "l "+(cx-fx));
            if (pos+1 == messages.size() && cx - fx > Util.dp(20)) return;

            imageView1.setX(cx - fx - getMeasuredWidth());// - cx - fx);
            imageView2.setAlpha(Math.max(127, 255 - (255 * (int) (cx - fx) / getMeasuredWidth())));
            Log.d("mi", "al r " + imageView2.getAlpha());
            imageView2.setScaleX(scale - (cx - fx) / getMeasuredWidth()); //1 -
            imageView2.setScaleY(scale - (cx - fx) / getMeasuredWidth()); //1 -
        }

        if (slideToRight && pos != 0) {// && pos+1 != messages.size()) {
            imageView1.setVisibility(GONE);
            imageView3.setVisibility(VISIBLE);

            Log.d("mi", "r " + (cx - fx));
            if (pos == 0 && cx - fx > Util.dp(20)) return;
            //255 - neprozrachnaya
            imageView2.setX(cx - fx);// - getMeasuredWidth());// - cx - fx);


            imageView3.setAlpha(Math.max(127, 255 - (255 * (int) (cx - fx) / getMeasuredWidth())));
            Log.d("mi", "al r " + imageView3.getAlpha());
            imageView3.setScaleX(-(cx - fx) / getMeasuredWidth());
            imageView3.setScaleY(-(cx - fx) / getMeasuredWidth());
        }
    }

    private void loadImage(int pos, ImageView imageView) {
        if (messages.size() <= pos || pos < 0) return;
        if (!(messages.get(pos).message instanceof TdApi.MessagePhoto)) return;

        TdApi.PhotoSize photo = TGFunc.getBetterPhoto(((TdApi.MessagePhoto) messages.get(pos).message).photo.photos);
        String path = photo.photo.path;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        bitmap = BitmapFactory.decodeFile(path, options);

        imageView.setImageBitmap(bitmap);
    }

    private void loadImages() {
        if (pos == 0 || pos+1 == messages.size()) return;
        loadImage(pos+1, imageView1);
        loadImage(pos, imageView2);
        loadImage(pos - 1, imageView3);
    }

    private int getBitmapWidth() {
        Bitmap bitmap = ((BitmapDrawable)imageView2.getDrawable()).getBitmap();
        float scaleL = getWidth() > getHeight() ? bitmap.getWidth() / getWidth() : bitmap.getHeight() / getHeight();
        return (int)(bitmap.getWidth()/scaleL * scale);
    }

    private int getBitmapHeight() {
        Bitmap bitmap = ((BitmapDrawable)imageView2.getDrawable()).getBitmap();
        int scaleL = getWidth() > getHeight() ? bitmap.getWidth() / getWidth() : bitmap.getHeight() / getHeight();
        return (int)(bitmap.getHeight()/scaleL * scale);
    }

}

/*public class ImagesView extends ImageViewTouch {

    public ImagesView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.d("ImageViewTouchPage", "" + event.getX() + " " + event.getY());
        if (event.getAction() == MotionEvent.ACTION_MOVE) {
            setX(event.getX());

            //getX
        }
        return super.onTouchEvent(event);
    }
}*/
