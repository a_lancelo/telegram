package com.kand.telegram.logic;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.kand.telegram.R;
import com.kand.telegram.ui.MainActivity;
import com.kand.telegram.ui.PreviewAuthFragment;
import com.kand.telegram.ui.adapters.SidebarMenuAdapter;
import com.squareup.picasso.Picasso;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.io.File;


public class SidebarMenuFragment extends Fragment {

    public ListView sidebarMenulistView;

    protected DialogInterface.OnClickListener okLogOutDialog = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            logOut();
        }
    };

    public void addSlideMenu(View rootView) {

        DrawerLayout mDrawerLayout;
        ActionBarDrawerToggle mDrawerToggle;
        try {
            ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
        } catch (NullPointerException e) {}
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);

        mDrawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout); //rootView
        mDrawerToggle= new ActionBarDrawerToggle(getActivity(), mDrawerLayout, toolbar, R.string.app_name, R.string.app_name) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        sidebarMenulistView = ((ListView) getActivity().findViewById(R.id.menuListView)); //rootView
        sidebarMenulistView.setAdapter(new SidebarMenuAdapter(getActivity()));
    }

    public void updateSidebarHeader(TdApi.User user) {
        if (user == null || user.id == 0) return;
        ((TextView) getActivity().findViewById(R.id.name)).setText(user.firstName + " " + user.lastName); //getView
        ((TextView) getActivity().findViewById(R.id.phone)).setText("+" + user.phoneNumber); //getView

        ImageView imageView = ((ImageView) getActivity().findViewById(R.id.profilePhoto)); //getView
        Drawable drawable = new BitmapDrawable(getActivity().getResources(),
                CircleTransform.makeCircleBitmap(TGFunc.getUserPlaceholder(getActivity(), user, 65)));
        Picasso.with(getActivity()).load(new File(TGFunc.getFileLocalPath(user.profilePhoto.small)))
                .placeholder(drawable).transform(new CircleTransform()).into(imageView);
    }

    public void hideSidebarMenu() {
        DrawerLayout mDrawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout); //getView
        mDrawerLayout.closeDrawers();
    }

    public void logOut() {
        Client client = TG.getClientInstance();
        if (client == null) return;
        client.send(new TdApi.ResetAuth(), new Client.ResultHandler() {
            @Override
            public void onResult(final TdApi.TLObject object) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("LogOut", object.toString());
                        TGFunc.setFirstTimeState(getActivity(), true);
                        getPreferences().edit().clear().commit();
                        TGService service = ((MainActivity) getActivity()).getService();
                        if (service != null) {
                            Log.d("LogOut", "appService");
                            service.stopSelf();
                        } else {
                            Log.d("LogOut", "appService null");
                        }

                        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        fragmentManager.popBackStack();
                        Fragment fragment = new PreviewAuthFragment();
                        fragmentManager.beginTransaction().replace(R.id.container, fragment, "preview_auth").commit();
                    }
                });
            }
        });
    }

    public SharedPreferences getPreferences() {
        return getActivity().getSharedPreferences("telegram", Context.MODE_PRIVATE);
    }

}
