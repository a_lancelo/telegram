package com.kand.telegram.logic;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.ImageView;

import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import de.marcreichelt.webp_backport.WebPBackport;

public class LoadingWebP extends AsyncTask<Void, String, Bitmap> {

    String path;
    ImageView imageView;
    Context context;

    public LoadingWebP(Context context, String path, ImageView imageView) {
        this.context = context;
        this.path = path;
        this.imageView = imageView;
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        try {
            InputStream in = new FileInputStream(path);
            byte[] encoded = IOUtils.toByteArray(in);
            return WebPBackport.decode(encoded);
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (bitmap == null) return;
        TGFunc.addBitmapToMemoryCache(path, bitmap);
        imageView.setImageBitmap(bitmap);
    }
}
