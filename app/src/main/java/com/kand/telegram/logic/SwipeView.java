package com.kand.telegram.logic;

import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.FragmentActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.kand.telegram.R;

public class SwipeView extends LinearLayout {

    public SwipeView(Context context) {
        super(context);
    }

    public SwipeView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SwipeView(final View view) {
        super(view.getContext());
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        setLayoutParams(layoutParams);
        addView(view, layoutParams);
        animatorListener = new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                Log.d("animate", "onAnimationStart");
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                Log.d("animate", "onAnimationEnd");
                if (isClosing) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            setBackgroundColor(Color.TRANSPARENT);
                            ((FragmentActivity) view.getContext()).getSupportFragmentManager().popBackStack();
                            TGFunc.hideSoftKeyboard((Activity)view.getContext());
                        }
                    });
                }
                isClosing = false;
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        };

        //float toX = 320;
        //View view = getActivity().findViewById(R.id.container);
        //view.setTranslationY(ti);
        //view.setTranslationX(320);
        //view.animate().translationX(toX).setDuration(0).
        //view.animate().translationX(0).setDuration(800);
    }

    float fx, fy, px, py;
    boolean viewMoving, isClosing;
    Animator.AnimatorListener animatorListener;

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return onTouchEvent(ev);
    }

    public boolean onTouchEvent(MotionEvent ev) {
        int dx = Math.max(0, (int) (ev.getX() - fx));

        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                fx = ev.getX();
                fy = ev.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                if (dx > Util.dp(30)) {
                    viewMoving = true;
                }
                px = ev.getX();
                py = ev.getY();
                if (viewMoving) {
                    getChildAt(0).setX(dx);
                    setBackgroundColor(Color.argb(100 - (dx*100 / getMeasuredWidth()), 0, 0, 0));
                }
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                if (viewMoving) {
                    viewMoving = false;
                    int toX = 0;
                    if (ev.getX() > getMeasuredWidth() / 3) {
                        isClosing = true;
                        toX = getMeasuredWidth();
                    }
                    getChildAt(0).animate().translationX(toX).setDuration(200).setListener(animatorListener);
                }
                break;
        }
        return viewMoving;
    }
}
